//
//  ForgotPassword.m
//  Punch
//
//  Created by Bhushan Bawa on 03/11/16.
//  Copyright © 2016 Hyperlink. All rights reserved.
//

#import "ForgotPassword.h"

@interface ForgotPassword ()

@end

@implementation ForgotPassword

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]];
    self.navigationController.navigationBarHidden=false;
    
    
    
    otpTF = [[UITextField alloc] initWithFrame:CGRectMake((self.view.frame.size.width-300)/2, 90, 300, 45)];
    otpTF.borderStyle = UITextBorderStyleNone;
    otpTF.backgroundColor=[UIColor colorWithRed:231.0f/255.0f green:231.0f/255.0f blue:231.0f/255.0f alpha:206.0f/255.0f];
    otpTF.font = [UIFont boldSystemFontOfSize:15];
    otpTF.placeholder = @"Temporary Password";
    otpTF.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    otpTF.delegate = self;
    otpTF.textAlignment=NSTextAlignmentCenter;
    otpTF.autocapitalizationType = UITextAutocapitalizationTypeNone;
    
    [self.view addSubview:otpTF];
    
    
    
    NewPasswordTF = [[UITextField alloc] initWithFrame:CGRectMake((self.view.frame.size.width-300)/2, otpTF.frame.origin.y+otpTF.frame.size.height+40, 300, 45)];
    NewPasswordTF.borderStyle = UITextBorderStyleNone;
    NewPasswordTF.backgroundColor=[UIColor colorWithRed:231.0f/255.0f green:231.0f/255.0f blue:231.0f/255.0f alpha:206.0f/255.0f];
    NewPasswordTF.font = [UIFont boldSystemFontOfSize:15];
    NewPasswordTF.placeholder = @"New Password";
    NewPasswordTF.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    NewPasswordTF.delegate = self;
    NewPasswordTF.textAlignment=NSTextAlignmentCenter;
     NewPasswordTF.autocapitalizationType = UITextAutocapitalizationTypeNone;
    NewPasswordTF.secureTextEntry=TRUE;
    [self.view addSubview:NewPasswordTF];
    
    
    
    ConfirmpasswordTF = [[UITextField alloc] initWithFrame:CGRectMake((self.view.frame.size.width-300)/2,  NewPasswordTF.frame.origin.y+NewPasswordTF.frame.size.height+40, 300, 45)];
    ConfirmpasswordTF.borderStyle = UITextBorderStyleNone;
    ConfirmpasswordTF.backgroundColor=[UIColor colorWithRed:231.0f/255.0f green:231.0f/255.0f blue:231.0f/255.0f alpha:206.0f/255.0f];
    ConfirmpasswordTF.font = [UIFont boldSystemFontOfSize:15];
    ConfirmpasswordTF.placeholder = @"Confirm Password";
    ConfirmpasswordTF.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    ConfirmpasswordTF.delegate = self;
    ConfirmpasswordTF.textAlignment=NSTextAlignmentCenter;
    ConfirmpasswordTF.secureTextEntry=TRUE;
    ConfirmpasswordTF.autocapitalizationType = UITextAutocapitalizationTypeNone;
    [self.view addSubview:ConfirmpasswordTF];
    
    
    
    ChangePasswordBT = [UIButton buttonWithType:UIButtonTypeCustom];
    [ChangePasswordBT addTarget:self
               action:@selector(ChangePassword_Action:)
     forControlEvents:UIControlEventTouchUpInside];
    [ChangePasswordBT setTitle:@"Change Password" forState:UIControlStateNormal];
    ChangePasswordBT.frame = CGRectMake((self.view.frame.size.width-200)/2,  ConfirmpasswordTF.frame.origin.y+ConfirmpasswordTF.frame.size.height+40, 200, 45);
   [ChangePasswordBT setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    ChangePasswordBT.backgroundColor=[UIColor whiteColor];
    ChangePasswordBT.layer.cornerRadius=15;

    [self.view addSubview:ChangePasswordBT];
    



}


-(void)ChangePassword_Action:(UIButton *)sender
{
    if ([otpTF.text isEqualToString:@""] || [NewPasswordTF.text isEqualToString:@""] || [ConfirmpasswordTF.text isEqualToString:@""] ) {
        
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:@"All fields are mandatory" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        
    

    
    }
    else if(![otpTF.text isEqualToString:_otp_value])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:@"OTP is not match" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        

    }
    
     else if(NewPasswordTF.text.length<=5)
     {
         
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:@"Your password must be at least 6 characters" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
         [alert show];
         
     }
   
    
    
    else if(![NewPasswordTF.text isEqualToString:ConfirmpasswordTF.text])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:@"Password and Confirm Password are not Same" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        

    }
    else
    {
   
            NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
            [dic setObject:@"reset_password" forKey:@"method"];
    
            [dic setObject:NewPasswordTF.text forKey:@"password"];
            [dic setObject:_Email_id forKey:@"email"];
            [APPDELEGATE addLoader:nil];
            [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"reset_password"];
         }
            
    
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==501) {
        
        
        [self.navigationController popToRootViewControllerAnimated:YES];
        
        
        
        
    }
}


#pragma mark web service
-(void)webHandlerManager:(Webservice *)webHandlerManager didFinishLoadingWithData:(NSObject *)responseObject
{
    [APPDELEGATE removeLoader];
    
    
    if ([webHandlerManager.strMethod isEqualToString:@"reset_password"])
    {
        if([[responseObject valueForKey:@"sucess"]isEqualToString:@"1"])
        {
            
                       UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:[responseObject valueForKey:@"msg"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            alert.tag=501;
            
        }
        else
        {
           
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:[responseObject valueForKey:@"msg"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            
            
            
        }
    }
    //user_login
  }
-(void)webHandlerManager:(Webservice *)webHandlerManager didFailedLoadingWithError:(NSString *)responseError
{
    
    NSLog(@"%@",responseError);
    NSLog(@"%@",webHandlerManager.strMethod);
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    
    
    
    return YES;
    
}
@end
