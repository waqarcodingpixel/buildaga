//
//  Constants.h
//  Punch
//
//  Created by Jaimish on 01/07/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#ifndef Punch_Constants_h
#define Punch_Constants_h
#define iPhone UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone
#define iPad  UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad
#define _size  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
#define iOS8  ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
#define iOS7  ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
#define iOS78  ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0 && [[[UIDevice currentDevice] systemVersion] floatValue] < 8.0)

#define iOS6  ([[[UIDevice currentDevice] systemVersion] floatValue] >= 6.0)

/*
#define webURL              @"http://hyperlinkserver.com/punchlist/ws/service_new.php?"
#define kSingUpMethod       @"http://hyperlinkserver.com/punchlist/ws/signup.php?"
#define kEditProfile        @"http://hyperlinkserver.com/punchlist/ws/edit_profile.php?"
#define kFBGmailRegister    @"http://hyperlinkserver.com/punchlist/ws/fb_gamil_register.php?"
#define kCreateProject      @"http://hyperlinkserver.com/punchlist/ws/create_project2.php?"
#define kEditProject        @"http://hyperlinkserver.com/punchlist/ws/edit_project2.php?"
#define kEditProjectImage   @"http://hyperlinkserver.com/punchlist/ws/edit_project_image.php"

#define kSharingUrl         @"http://hyperlinkserver.com/punchlist/project/view_detail/"

#define UserProfile         @"http://hyperlinkserver.com/punchlist/user_uploads/"
#define UserProfileThumbs   @"http://hyperlinkserver.com/punchlist/user_uploads/thumb/"

#define ThumbImages         @"http://hyperlinkserver.com/punchlist/project_images/thumb/"
#define ProjectImages       @"http://hyperlinkserver.com/punchlist/project_images/"

#define AdImages            @"http://hyperlinkserver.com/punchlist/advertis_image/"
#define AdThumbsImages      @"http://hyperlinkserver.com/punchlist/advertis_image/thumb/"
*/






#define webURL              @"https://app.buildaga.com/ws/service_new.php?"
#define kSingUpMethod       @"http://app.buildaga.com/ws/signup.php?"
#define kEditProfile        @"https://app.buildaga.com/ws/edit_profile.php?"
#define kFBGmailRegister    @"https://app.buildaga.com/ws/fb_gamil_register.php?"
#define kCreateProject      @"https://app.buildaga.com/ws/create_project2.php?"
#define kEditProject        @"https://app.buildaga.com/ws/edit_project2.php?"
#define kEditProjectImage   @"https://app.buildaga.com/ws/edit_project_image.php"

#define kSharingUrl         @"https://app.buildaga.com/project/view_detail/"

#define kSharingUrl_New      @"https://app.buildaga.com/share/"

#define UserProfile         @"https://app.buildaga.com/user_uploads/"
#define UserProfileThumbs   @"https://app.buildaga.com/user_uploads/"

#define ThumbImages         @"https://app.buildaga.com/project_images/"
#define ProjectImages       @"https://app.buildaga.com/project_images/"

#define AdImages            @"https://app.buildaga.com/advertis_image/"
#define AdThumbsImages      @"https://app.buildaga.com/advertis_image/"

#define Count_project      @"https://app.buildaga.com/ws/check_user_project.php"



#define KLoginSuccess @"LoginSuccess"
#define login_type @"login_type"
#define LoginUserDetails @"LoginUserDetail"
#define LoginUser @"LoginUser"
#define KData @"data"
#define KSucess @"sucess"

#endif
