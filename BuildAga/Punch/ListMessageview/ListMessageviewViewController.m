//
//  ListMessageviewViewController.m
//  Punch
//
//  Created by Jaimish on 02/07/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import "ListMessageviewViewController.h"
#import "CommentsCell.h"
@interface ListMessageviewViewController ()
{
    int i;
    CGRect commenttableview;
    CGRect chatview;
    CGRect textview;

    NSMutableArray *commnets,*projectlist,*tempobject;
    UILabel *temp;
       CGFloat keyboard_height;
    CGRect framchatinput,chttext;
    
}
@end

@implementation ListMessageviewViewController

- (void)viewDidLoad {
    
    
    
    [[NSUserDefaults standardUserDefaults]setValue:@"0" forKey:@"CHECK"];
    [[NSUserDefaults standardUserDefaults]synchronize];

    
    projectlist=[[NSUserDefaults standardUserDefaults]valueForKey:@"MessageListing"];
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"MessageListing"];
    
    [[NSUserDefaults standardUserDefaults]synchronize];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardShow:) name:UIKeyboardWillChangeFrameNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardHide:) name:UIKeyboardDidHideNotification object:nil];
         self.tableview.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    commnets=[[NSMutableArray alloc]init];
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    [dic setObject:@"message_listing" forKey:@"method"];
    [dic setObject:[[[NSUserDefaults standardUserDefaults] valueForKey:LoginUserDetails] valueForKey:@"id"] forKey:@"user_id"];
    [APPDELEGATE addLoader:nil];
    [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"message_listing"];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==self.commentsview1.commenttable) {
         return  [commnets count];
    }
    return [projectlist count];
    
   
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==self.commentsview1.commenttable) {
        
        static NSString *cellIdentifier = @"CommentTableViewCell";
        
        CommentsCell *cell = (CommentsCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier ];
        if (cell == nil)
        {
            cell =[[CommentsCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier ];
            
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.comment.font=[UIFont fontWithName:@"OpenSans" size:12.0];
        //        cell.backgroundColor=[UIColor clearColor];
        //        cell.textLabel.text=[NSString stringWithFormat:@"I have to concede. There is now way using"];
        
        
        
        cell.commet.text=[[commnets valueForKey:@"comment"]objectAtIndex:indexPath.row];
        
        
        
        
        
        
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                             NSUserDomainMask, YES);
        //
        NSString *url = [NSString stringWithFormat:@"%@%@",UserProfile,[[commnets valueForKey:@"image"] objectAtIndex:indexPath.row]];
        NSString *filename =[NSString stringWithFormat:@"Profile_%@",[[commnets valueForKey:@"image"] objectAtIndex:indexPath.row] ];
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:filename];
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        if (image==nil) {
            
            NSString *url = [NSString stringWithFormat:@"%@%@",UserProfile,[[commnets valueForKey:@"image"] objectAtIndex:indexPath.row]];
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                
                NSString *str=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                NSURL *imagepath = [NSURL URLWithString:str];
                
                NSData *data = [NSData dataWithContentsOfURL:imagepath];
                UIImage *image = [UIImage imageWithData:data];
                [data writeToFile:path atomically:YES];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [cell.userimage setImage:image];
                    //                    [self.activityindicator stopAnimating];
                    //                    [self.activityindicator setHidden:YES];
                    
                });
            });
            
        }
        else
        {
            [cell.userimage setImage:image];
            //            [self.activityindicator stopAnimating];
            //            [self.activityindicator setHidden:YES];
            
        }
        
        
        
        
        
        
        
        
        
        //cell.userimage.image=[UIImage imageNamed:@"1.png"];
        cell.nameofuser.text=[[commnets valueForKey:@"name"] objectAtIndex:indexPath.row];
        //        cell.textLabel.textColor=[UIColor whiteColor];
        //       UIButton *button=[[UIButton alloc]initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width-70, 0,70 , 36)];
        //       [button setImage:[UIImage imageNamed:@"round_comment"] forState:UIControlStateNormal];
        
        //       cell.accessoryView=button;
        
        return cell;
    }
    else
    {
        static NSString *cellIdentifier = @"TableViewCell";
        
        UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier ];
        if (cell == nil)
        {
            cell =[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier ];
            
        }
        
        for (UIView *view in cell.contentView.subviews) {
            [view removeFromSuperview];
        }
        cell.backgroundColor=[UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
       
                cell.textLabel.text=[NSString stringWithFormat:@"%@ - %@",[[projectlist objectAtIndex:indexPath.row] valueForKey:@"project_name"],[[projectlist objectAtIndex:indexPath.row] valueForKey:@"task_title"]];
                cell.textLabel.font=[UIFont fontWithName:@"OpenSans" size:15.0];
                cell.textLabel.textColor=[UIColor whiteColor];
                UIButton *button=[[UIButton alloc]initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width-35, 12,25, 25)];
        
                [button setTitle:[NSString stringWithFormat:@"%d",[[[projectlist objectAtIndex:indexPath.row] valueForKey:@"count"]intValue]] forState:UIControlStateNormal];
                [button setBackgroundImage:[UIImage imageNamed:@"round_blue_message"] forState:UIControlStateNormal];
        if ([[[projectlist objectAtIndex:indexPath.row] valueForKey:@"count"]intValue]==0) {
            
            button.hidden=TRUE;
            
        }
        else
        {
            button.hidden=FALSE;

                [cell.contentView addSubview:button];
        }
                
                return cell;
    
   
    }
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    if (tableView==self.commentsview1.commenttable) {
        
       // NSIndexPath *indexPath = [NSIndexPath indexPathForRow:indexPath inSection:0];
        
//        CommentsCell *cell=(CommentsCell *)[self.commentsview1.commenttable cellForRowAtIndexPath:indexPath];
        temp=nil;
        temp=[[UILabel alloc]init];
        temp.font=[UIFont fontWithName:@"OpenSans" size:13.0];
        temp.text=[[commnets valueForKey:@"comment"] objectAtIndex:indexPath.row];
        
        if ([self resizeMessageLabel:temp].height<80) {
            return 90;
        }
        else
        {
            return [self resizeMessageLabel:temp].height;
        }
    }
    return  50;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"CHECK"]isEqualToString:@"0"]) {
        
        [[NSUserDefaults standardUserDefaults]setValue:@"1" forKey:@"CHECK"];
        [[NSUserDefaults standardUserDefaults]synchronize];

   
    i=0;
   
    self.commentsview1=[[CommentsView alloc]initWithFrame:self.view.frame];
    [self.view addSubview:self.commentsview1];
       [ self.commentsview1.closebtn addTarget:self action:@selector(close_btn_click:) forControlEvents:UIControlEventTouchUpInside];
    self.commentsview1.chattextview.delegate=self;
    [ self.commentsview1.sendbutton addTarget:self action:@selector(send:) forControlEvents:UIControlEventTouchUpInside];
    self.commentsview1.texview.frame=CGRectMake(0, 0, self.commentsview1.detailview.frame.size.width, 130+[self resizeMessageLabel:self.commentsview1.detailTask].height);
    self.commentsview1.commenttable.tableHeaderView=self.commentsview1.detailview;
    self.commentsview1.commenttable.delegate=self;
    self.commentsview1.commenttable.dataSource=self;
    [self performSelector:@selector(maindelay) withObject:self afterDelay:1];
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    tempobject=[[NSMutableArray alloc]init];
    [tempobject addObject:[projectlist objectAtIndex:indexPath.row]];
    [dic setObject:@"comment_list" forKey:@"method"];
    [dic setObject:[[[NSUserDefaults standardUserDefaults] valueForKey:LoginUserDetails] valueForKey:@"id"] forKey:@"user_id"];
    [dic setObject:[[projectlist objectAtIndex:indexPath.row] valueForKey:@"task_id"] forKey:@"task_id"];
    [APPDELEGATE addLoader:nil];
    [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"comment_list"];
        
        
         }
  
}
-(void)delay
{

    
    
}
-(void)send:(UIButton *)sendbutton
{
    i=0;
    
    self.commentsview1.chatview.frame=chatview;
    self.commentsview1.chattextview.frame=textview;
    self.commentsview1.commenttable.frame=commenttableview;
    
    //    self.commentsview1.chattextview.frame=CGRectMake(self.commentsview1.chatview.frame.origin.x,self.commentsview1.commenttable.frame.origin.y+self.commentsview1.commenttable.frame.size.height+5,self.commentsview1.chatview.frame.size.width,38);
    //    self.commentsview1.chatview.frame = CGRectMake(self.commentsview1.chatview.frame.origin.x,self.commentsview1.commenttable.frame.origin.y+self.commentsview1.commenttable.frame.size.height,self.commentsview1.chatview.frame.size.width,43);
    
    if ([self.commentsview1.chattextview.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0 || [self.commentsview1.chattextview.text isEqualToString:@"Write here..."] ) {
       

        
        self.commentsview1.chattextview.text=@"Write here...";
        [self.commentsview1.chattextview endEditing:TRUE];
        return;
    }
    else
    {
        NSString *comment=self.commentsview1.chattextview.text;
        // {"method":"give_comment","task_id":"1","comment":"please complete task fast","user_id":"5"}
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        
        [dic setObject:@"give_comment" forKey:@"method"];
        
      [dic setObject:[[tempobject objectAtIndex:0] valueForKey:@"task_id"] forKey:@"task_id"];
        [dic setObject:[[[NSUserDefaults standardUserDefaults] valueForKey:LoginUserDetails] valueForKey:@"id"] forKey:@"user_id"];
        [dic setObject:comment forKey:@"comment"];
        [APPDELEGATE addLoader:nil];
        [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"give_comment"];
        // comment = sfsdjfhksjafhaldksjfhlaskdjfhlaskjfhaslkdjfhsaldkfjhsalfjkhsaldkfjhsaldkfjhsalkfjhsaldkfjhsadklfjhasklfjhaskldjfhsakldfjhaskldfjhsakldfjhsakljdfhajksfhalksjfhalskdjfhaskljdfhsdjkfhskdjfhaskjfhklajsdhfjklsahfklsajdhfjklsdhfklasjfhalskjfhaklsjdfhaklsjfhaskldfhajklsfh;
        //        id = 102;
        //        image = "1438667571.png";
        //        insertdate = "2015-08-04 09:47:49";
        //        name = john;
        //        "task_id" = 9;
        //        "user_id" = 8;
        NSMutableDictionary *tempcomment=[[NSMutableDictionary alloc]init];
        [tempcomment  setValue:comment forKey:@"comment"];
        [tempcomment setValue:[[[NSUserDefaults standardUserDefaults] valueForKey:LoginUserDetails] valueForKey:@"image"] forKey:@"image"];
        [tempcomment setValue:[[[NSUserDefaults standardUserDefaults] valueForKey:LoginUserDetails] valueForKey:@"name"] forKey:@"name"];
        [commnets insertObject:tempcomment atIndex:[commnets count]];
        [self.commentsview1.commenttable beginUpdates];
        NSIndexPath *myIP = [NSIndexPath indexPathForRow:([commnets count]-1) inSection:0];
        NSArray *myArray = [[NSArray alloc] initWithObjects:myIP, nil];
        [self.commentsview1.commenttable insertRowsAtIndexPaths:myArray withRowAnimation:UITableViewRowAnimationBottom];
        
        //[self.mytableview deleteRowsAtIndexPaths:path withRowAnimation:UITableViewRowAnimationBottom];
        [self.commentsview1.commenttable endUpdates];
        
        

        
        self.commentsview1.chattextview.text=@"Write here...";
    }
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[commnets count]-1 inSection:0];
    [self.commentsview1.commenttable scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    [self.commentsview1.chattextview endEditing:TRUE];
    
    
   

    self.commentsview1.chattextview.text=@"Write here...";
    
    
    
}
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if ([commnets count]>0) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[commnets count]-1 inSection:0];
        [self.commentsview1.commenttable scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    }
    textView.text=@"";

    return YES;

}
- (void)textViewDidChange:(UITextView *)textView;
{
    
    [self adjustFrames];
    
    
}

- (void)textViewDidEndEditing:(UITextView *)textView;
{

    
    
}
#pragma mark labelSize
- (CGSize) resizeMessageLabel:(UILabel *)lblMessage
{
    CGSize maxLabelSize = CGSizeMake([[UIScreen mainScreen] bounds].size.width-20,9999);
    
    CGSize expectedLabelSize = [lblMessage.text sizeWithFont:lblMessage.font
                                           constrainedToSize:maxLabelSize
                                               lineBreakMode:lblMessage.lineBreakMode];
    expectedLabelSize.height=expectedLabelSize.height+20;
    
    //adjust the label the the new height.
    //    CGRect newFrame = lblMessage.frame;
    //    newFrame.size.height = expectedLabelSize.height;
    //    lblMessage.frame = newFrame;
    //    viewMessage.frame = CGRectMake(viewMessage.frame.origin.x, viewMessage.frame.origin.y, viewMessage.frame.size.width, lblMessage.frame.size.height);
    
    return expectedLabelSize;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
//{
//    if (tableView==self.commentsview1.commenttable) {
//        return 28.0;
//    }
//    return 0.0;
//}
//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
//{
//    if (tableView==self.commentsview1.commenttable) {
//       return  self.commentsview1.chatview;
//    }
//   
//    return  nil;
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark btn_click 
- (IBAction)back_btn_cllick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)keyboardShow:(NSNotification *)notification
{
    NSLog(@"%f", [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height);
    keyboard_height = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    
//  self.commentsview1.chatview.frame = CGRectMake(self.commentsview1.chatview.frame.origin.x, [[UIScreen mainScreen] bounds].size.height- self.commentsview1.chatview.frame.size.height,self.commentsview1.chatview.frame.size.width, self.commentsview1.chatview.frame.size.height);
}




- (void)keyboardHide:(NSNotification *)notification
{
 
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    
    [self adjustFrames];
    
    return YES;
}
-(void) adjustFrames
{
    //    CGRect textFrame = self.commentsview1.chattextview.frame;
    //
    //
    //    NSLog(@"%f",textFrame.size.height);
    //
    //
    //    if (i<10) {
    //        if (textFrame.size.height< self.commentsview1.chattextview.contentSize.height) {
    //
    //
    //            self.commentsview1.chatview.frame = CGRectMake(self.commentsview1.chatview.frame.origin.x,self.commentsview1.chatview.frame.origin.y+self.commentsview1.chatview.frame.size.height-self.commentsview1.chattextview.contentSize.height-5, self.commentsview1.chatview.frame.size.width,  self.commentsview1.chattextview.contentSize.height+5);
    //            textFrame.size.height =  self.commentsview1.chattextview.contentSize.height;
    //            self.commentsview1.chattextview.frame = textFrame;
    //            i++;
    //        }
    //
    //
    //    }
    //
    //
    //    //
    //    NSLog(@"%f",textFrame.size.height);
    [self performSelector:@selector(delay123) withObject:self afterDelay:0];
}
- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    if (action == @selector(paste:) ||action == @selector(copy:))
    {
        return [super canPerformAction:action withSender:sender];
    }
    
    
    return NO;
}
-(void)maindelay
{
    commenttableview=self.commentsview1.commenttable.frame;
    chatview=self.commentsview1.chatview.frame;
    textview=self.commentsview1.chattextview.frame;
    
}
-(void)delay123
{
    CGRect textFrame = self.commentsview1.chattextview.frame;
    
    
    NSLog(@"%f",self.commentsview1.chattextview.contentSize.height);
    
    self.commentsview1.commenttable.translatesAutoresizingMaskIntoConstraints=YES;
    
    
    if ((textFrame.size.height-self.commentsview1.chattextview.contentSize.height)>20) {
        self.commentsview1.commenttable.frame=CGRectMake(self.commentsview1.commenttable.frame.origin.x, self.commentsview1.commenttable.frame.origin.y+(textFrame.size.height-self.commentsview1.chattextview.contentSize.height),self.commentsview1.commenttable.frame.size.width ,self.commentsview1.commenttable.frame.size.height);
        
        
        if (self.commentsview1.chattextview.contentSize.height < textFrame.size.height && self.commentsview1.chattextview.contentSize.height<150) {
            
            
            self.commentsview1.chatview.translatesAutoresizingMaskIntoConstraints=YES;
            self.commentsview1.chattextview.translatesAutoresizingMaskIntoConstraints=YES;
            self.commentsview1.chatview.frame = CGRectMake(self.commentsview1.chatview.frame.origin.x,self.commentsview1.chatview.frame.origin.y+self.commentsview1.chatview.frame.size.height-self.commentsview1.chattextview.contentSize.height-5, self.commentsview1.chatview.frame.size.width,  self.commentsview1.chattextview.contentSize.height+5);
            textFrame.size.height =  self.commentsview1.chattextview.contentSize.height;
            self.commentsview1.chattextview.translatesAutoresizingMaskIntoConstraints=YES;
            
            self.commentsview1.chattextview.frame=CGRectMake(self.commentsview1.chattextview.frame.origin.x,(self.commentsview1.chattextview.frame.origin.y+self.commentsview1.chattextview.contentSize.height)-self.commentsview1.chattextview.contentSize.height, self.commentsview1.chattextview.frame.size.width,  self.commentsview1.chattextview.contentSize.height);
            
            
            // self.commentsview1.chattextview.frame = textFrame;
            // [self.commentsview1.chattextview sizeToFit];
            
        }
        
        
        
        
    }
    else
    {
        if ((self.commentsview1.chattextview.contentSize.height-textFrame.size.height)>20 && self.commentsview1.chattextview.contentSize.height<150) {
            //         self.commentsview1.commenttable.frame=CGRectMake(self.commentsview1.commenttable.frame.origin.x, self.commentsview1.commenttable.frame.origin.y, self.commentsview1.commenttable.frame.size.width,self.commentsview1.commenttable.frame.size.height-(self.commentsview1.chattextview.contentSize.height-textFrame.size.height)-6);
            self.commentsview1.commenttable.frame=CGRectMake(self.commentsview1.commenttable.frame.origin.x, self.commentsview1.commenttable.frame.origin.y-(self.commentsview1.chattextview.contentSize.height-textFrame.size.height),self.commentsview1.commenttable.frame.size.width ,self.commentsview1.commenttable.frame.size.height);
        }
        
        
        
        //    if (i<10) {
        if (textFrame.size.height< self.commentsview1.chattextview.contentSize.height && self.commentsview1.chattextview.contentSize.height<150) {
            
            
            self.commentsview1.chatview.translatesAutoresizingMaskIntoConstraints=YES;
            self.commentsview1.chattextview.translatesAutoresizingMaskIntoConstraints=YES;
            self.commentsview1.chatview.frame = CGRectMake(self.commentsview1.chatview.frame.origin.x,self.commentsview1.chatview.frame.origin.y+self.commentsview1.chatview.frame.size.height-self.commentsview1.chattextview.contentSize.height-5, self.commentsview1.chatview.frame.size.width,  self.commentsview1.chattextview.contentSize.height+5);
            textFrame.size.height =  self.commentsview1.chattextview.contentSize.height;
            self.commentsview1.chattextview.translatesAutoresizingMaskIntoConstraints=YES;
            
            self.commentsview1.chattextview.frame=CGRectMake(self.commentsview1.chattextview.frame.origin.x,(self.commentsview1.chattextview.frame.origin.y+self.commentsview1.chattextview.contentSize.height)-self.commentsview1.chattextview.contentSize.height, self.commentsview1.chattextview.frame.size.width,  self.commentsview1.chattextview.contentSize.height);
            
            
            // self.commentsview1.chattextview.frame = textFrame;
            // [self.commentsview1.chattextview sizeToFit];
            
        }
    }
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
  
    [[NSUserDefaults standardUserDefaults]setValue:@"0" forKey:@"CHECK"];
    [[NSUserDefaults standardUserDefaults]synchronize];

        [self.commentsview1 removeFromSuperview];
  

}
-(void)close_btn_click:(UIButton *)sender
{
    
    [[NSUserDefaults standardUserDefaults]setValue:@"0" forKey:@"CHECK"];
    [[NSUserDefaults standardUserDefaults]synchronize];

    
    
    [self.commentsview1 removeFromSuperview];
}

-(void)webHandlerManager:(Webservice *)webHandlerManager didFinishLoadingWithData:(NSObject *)responseObject
{
    [APPDELEGATE removeLoader];
    
    if ([webHandlerManager.strMethod isEqualToString:@"comment_list"])
    {
        if([[responseObject valueForKey:@"sucess"]isEqualToString:@"1"])
        {
            
            
            UILabel *templabel=[[UILabel alloc]init];
            templabel.text=[[responseObject valueForKey:KData] valueForKey:@"details"];
            templabel.font=[UIFont fontWithName:@"OpenSans" size:14.0];
            int hight=[self resizeMessageLabel:templabel].height;
            self.commentsview1.texview.text=[[responseObject valueForKey:KData] valueForKey:@"details"];
            self.commentsview1.detailTask.text=[[responseObject valueForKey:KData] valueForKey:@"details"];
              self.commentsview1.tasktitle.text=[[responseObject valueForKey:KData] valueForKey:@"task_title"];
            self.commentsview1.detailview.frame=CGRectMake(0, 0, self.commentsview1.detailview.frame.size.width, 130+hight);
           
            self.commentsview1.commenttable.tableHeaderView=self.commentsview1.detailview;
            [commnets removeAllObjects];
            commnets=[responseObject valueForKey:@"comment_data"];
            [self.commentsview1.commenttable reloadData];
            
            
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                    [dic setObject:@"message_listing" forKey:@"method"];
                    [dic setObject:[[[NSUserDefaults standardUserDefaults] valueForKey:LoginUserDetails] valueForKey:@"id"] forKey:@"user_id"];
                    
                    [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"message_listing"];
                    
                    //                    [self.activityindicator stopAnimating];
                    //                    [self.activityindicator setHidden:YES];
                    
                });
            });
            
        }
        else
        {
            
            //            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Punch List" message:[responseObject valueForKey:@"msg"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            //            [alert show];
            
            
        }
        
    }
    
    if ([webHandlerManager.strMethod isEqualToString:@"give_comment"])
    {
        if([[responseObject valueForKey:@"sucess"]isEqualToString:@"1"])
        {
            [self.commentsview1 endEditing:YES];
        }
        else
        {
            [self.commentsview1 endEditing:YES];
        }
    }
    if ([webHandlerManager.strMethod isEqualToString:@"message_listing"])
    {
        if([[responseObject valueForKey:@"sucess"]isEqualToString:@"1"])
        {
            
           
            
            projectlist=[responseObject valueForKey:KData];
            
            // projectlist
           
            NSSortDescriptor* sortOrder = [NSSortDescriptor sortDescriptorWithKey: @"count" ascending: NO];
            projectlist=[[projectlist sortedArrayUsingDescriptors: [NSArray arrayWithObject: sortOrder]] mutableCopy];

            
            [self.tableview reloadData];
            
            
        }
        else
        {
           
            projectlist=nil;
            projectlist=[[NSMutableArray alloc]init];
               [self.tableview reloadData];
            
        }
    }
}
-(void)webHandlerManager:(Webservice *)webHandlerManager didFailedLoadingWithError:(NSString *)responseError
{
    
    NSLog(@"%@",responseError);
    NSLog(@"%@",webHandlerManager.strMethod);
    
}
@end
