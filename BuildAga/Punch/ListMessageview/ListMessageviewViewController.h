//
//  ListMessageviewViewController.h
//  Punch
//
//  Created by Hyperlink on 02/07/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommentsView.h"



@interface ListMessageviewViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableview;
@property (strong, nonatomic) IBOutlet CommentsView  *commentsview1;

@end
