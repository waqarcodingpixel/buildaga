//
//  ViewController.h
//  Punch
//
//  Created by Jaimish on 30/06/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UIAlertViewDelegate>
{
    NSString *otp_value;
    
}

@property (strong, nonatomic) IBOutlet UITextField *txt_password;
@property (strong, nonatomic) IBOutlet UITextField *txt_email;
@property (strong, nonatomic) IBOutlet UIView *forgotpassview;
@property (strong, nonatomic) IBOutlet UITextField *txt_forgetemail;
@property (strong, nonatomic) IBOutlet UIButton *forgotpassword;
@property (strong, nonatomic) IBOutlet UIButton *signupbtn;

@end

