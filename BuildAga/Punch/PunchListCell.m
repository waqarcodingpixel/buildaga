//
//  PunchListCell.m
//  Punch
//
//  Created by Jaimish on 09/07/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import "PunchListCell.h"

@implementation PunchListCell
@synthesize commentbtn,tickbtn,taskdetails,taskname,edittask,counterbtn;
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PunchListCell" owner:self options:nil];
        self = [nib objectAtIndex:0];
        
    }
    return self;
}

- (IBAction)deleteTaskButtonTapped:(id)sender {
    
    [self.delegate deleteTaskButtonTapped:sender];
}

@end
