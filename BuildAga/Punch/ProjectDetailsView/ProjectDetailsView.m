//
//  ProjectDetailsView.m
//  Punch
//
//  Created by Jaimish on 06/07/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import "ProjectDetailsView.h"
#import "ProjectDetailcell.h"
#import "PunchListViewViewController.h"
#import "AlbumImageCell.h"
#import "CreateProject.h"
#import "CreatePunch.h"
#import <Social/Social.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MessageUI.h>

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareDialog.h>
#import <FBSDKShareKit/FBSDKShareLinkContent.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface ProjectDetailsView ()<MFMailComposeViewControllerDelegate>
{
    NSMutableArray *array,*array1;
    int temp,temp2,imageselect;
    NSMutableArray *albumImagedataarr,*arr_selected,*arrImages;
    NSMutableArray *emails,*punch,*imagedata;
    int indexofimage;
}
@end

@implementation ProjectDetailsView

- (void)viewDidLoad {
    
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleSignleTap:)];
    
    [self.sharingbg addGestureRecognizer:tap];
    UITapGestureRecognizer *tap1=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTap:)];
    self.imageScrollView.delegate=self;
    [self.imageshowbg addGestureRecognizer:tap1];
    
    UITapGestureRecognizer *tap2=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTap:)];
    [self.imageScrollView addGestureRecognizer:tap2];
    
    arrImages=[[NSMutableArray alloc]init];
    arr_selected=[[NSMutableArray alloc]init];
    imageselect=0;
    temp2=0;
/*    albumImagedataarr=[[NSMutableArray alloc]init];
    
    NSData *imagedataLocal =[self compressImage:[UIImage imageNamed:@"pic_album"]];
    [albumImagedataarr insertObject:imagedataLocal atIndex:0];
    [albumImagedataarr insertObject:imagedataLocal atIndex:1];
    [albumImagedataarr insertObject:imagedataLocal atIndex:2];
    [albumImagedataarr insertObject:imagedataLocal atIndex:3];
    [albumImagedataarr insertObject:imagedataLocal atIndex:4];
    [albumImagedataarr insertObject:imagedataLocal atIndex:5];
  */  
    self.detailbtn.selected=TRUE;
    
    self.projecttableview.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
//    if ([[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails]valueForKey:@"role"]isEqualToString:@"CU"]) {
//        
//        array=[[NSMutableArray alloc]initWithObjects:@"Contractor's Email Address",@"Punches:", nil];
//        
//    }
//    else
//    {
        array=[[NSMutableArray alloc]initWithObjects:@"Customer's Email Address",@"Punches:", nil];
        
  //  }
    
    self.nameofproject.adjustsFontSizeToFitWidth=TRUE;
    self.nameofproject1.adjustsFontSizeToFitWidth=TRUE;
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{

    if (scrollView==self.imageScrollView) {
        
        CGFloat width = scrollView.frame.size.width;
        NSInteger page = (scrollView.contentOffset.x + (0.5f * width)) / width;
        indexofimage=(int)page;
        self.pageControl.currentPage=page;
    }
    
    
}
-(void)handleSignleTap:(UITapGestureRecognizer *)sharetap
{

    [self.sharingview removeFromSuperview];
}
-(void)handleTap:(UITapGestureRecognizer *)showtap
{

    [self.imageslideview removeFromSuperview];
}
-(IBAction)btnImageViewRemoveClicked:(id)sender
{
        [self.imageslideview removeFromSuperview];
}
-(void)viewWillAppear:(BOOL)animated
{
    indexofimage=0;
    
    if ([[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails]valueForKey:@"role"]isEqualToString:@"CO"]) {
         [self.projecttableview setEditing:YES animated:YES];
    }
  
    else
    {
     [self.projecttableview setEditing:NO animated:NO];
    }
    
    //{"method":"project_detail","project_id":"1"}
    punch=[[NSMutableArray alloc]init];
    emails=[[NSMutableArray alloc]init];
    self.projectname.text=[[[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedProject"] valueForKey:@"title"];
    self.nameofproject.text=[[[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedProject"] valueForKey:@"title"];
    self.nameofproject1.text=[[[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedProject"] valueForKey:@"title"];
    
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"FromEditingMode"]isEqualToString:@"FromEditingMode"]) {
        
        [[NSUserDefaults standardUserDefaults]setValue:@"" forKey:@"FromEditingMode"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        punch=[[[NSUserDefaults standardUserDefaults] valueForKey:@"TempProject"] valueForKey:@"punch_data"];
        emails=[[[[NSUserDefaults standardUserDefaults] valueForKey:@"TempProject"] valueForKey:@"email_data"] valueForKey:@"email"];
        NSIndexSet *sections = [[NSIndexSet alloc] initWithIndex:0];
           [self.projecttableview reloadSections:sections withRowAnimation:UITableViewRowAnimationNone];
       
        
    
        sections=nil;
        sections=[[NSIndexSet alloc] initWithIndex:1];
        [self.projecttableview reloadSections:sections withRowAnimation:UITableViewRowAnimationNone];
        imagedata=[[[NSUserDefaults standardUserDefaults] valueForKey:@"TempProject"] valueForKey:@"image_data"];
        temp2=(int)[imagedata count];
        sections=nil;
        sections=[[NSIndexSet alloc] initWithIndex:0];
        [self.albumimagestable reloadSections:sections withRowAnimation:UITableViewRowAnimationNone];
        [arrImages removeAllObjects];
        for (int i=0; i<[imagedata count]; i++) {
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                 NSUserDomainMask, YES);
            //
            NSString *url = [NSString stringWithFormat:@"%@%@",ProjectImages,[[imagedata objectAtIndex:i] valueForKey:@"image"]];
            NSString *filename =[NSString stringWithFormat:@"Project_%@",[[imagedata objectAtIndex:i] valueForKey:@"image"]];
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString* path = [documentsDirectory stringByAppendingPathComponent:filename];
            UIImage* image = [UIImage imageWithContentsOfFile:path];
            if (image==nil) {
                
                NSString *url =  [NSString stringWithFormat:@"%@%@",ProjectImages,[[imagedata objectAtIndex:i] valueForKey:@"image"]];
                dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                    
                    NSString *str=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                    NSURL *imagepath = [NSURL URLWithString:str];
                    
                    NSData *data = [NSData dataWithContentsOfURL:imagepath];
                    // UIImage *image = [UIImage imageWithData:data];
                    [data writeToFile:path atomically:YES];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [arrImages addObject:data];
                        
                        
                    });
                });
                
            }
            else
            {
                
                [arrImages addObject:[NSData dataWithContentsOfFile:path]];
            }
            
        }
        
        
        
        
    }
    else
    {
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        [dic setObject:@"project_detail" forKey:@"method"];
        
        [dic setObject:[[[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedProject"] valueForKey:@"id"] forKey:@"project_id"];
        [APPDELEGATE addLoader:nil];
        [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"project_detail"];
        [arrImages removeAllObjects];
    }
    self.projecttableview.tableHeaderView = [[UIView alloc] initWithFrame:CGRectZero];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
 
    if (tableView==self.projecttableview) {
        
        if (indexPath.section==1) {
            return YES;
        }
        else
        {
            return NO;
        }
    }
    return NO;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    if (tableView== self.albumimagestable) {
        return  nil;
    }
    
    
    UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 40)];
    
    
    view.backgroundColor=[UIColor clearColor];
    
    UIImageView *image1=[[UIImageView alloc]initWithFrame:CGRectMake(15, 28, [[UIScreen mainScreen] bounds].size.width-30,1)];
    
    image1.backgroundColor=[UIColor whiteColor];
    
    //    image.image=[UIImage imageNamed:[sectionimage objectAtIndex:section]];
    UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(20, 0, 250, 30)];
    if ([[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails]valueForKey:@"role"]isEqualToString:@"CU"]) {
        
        array=[[NSMutableArray alloc]initWithObjects:@"Customer's Email Address",@"Punches:", nil];
        
    }
    else
    {
        array=[[NSMutableArray alloc]initWithObjects:@"Customer's Email Address",@"Punches:", nil];
        
    }
    label.text=[array objectAtIndex:section];
    label.textColor=[UIColor whiteColor];
    label.font=[UIFont fontWithName:@"OpenSans" size:18.0];
    if (section==1 && [[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails]valueForKey:@"role"]isEqualToString:@"CO"]) {
        UIButton *button=[[UIButton alloc]initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width-36, 0,24 ,24)];
      
        [button addTarget:self action:@selector(punchclick:) forControlEvents:UIControlEventTouchUpInside];
        [button setBackgroundImage:[UIImage imageNamed:@"add"] forState:UIControlStateNormal];
        
        [view addSubview:button];
        
    }
    
    [view addSubview:image1];
    [view addSubview:label];
    
    return  view;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView==self.albumimagestable) {
        return 120.0;
    }
    else if ([tableView isEqual:self.projecttableview])
    {
        if (indexPath.section==0) {
            return 0;
            
        }
        else
        {
            return 42.0;
        }
    }
    return 0;
    
}
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleNone;
}
-(void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
//    id thing = [things objectAtIndex:sourceIndexPath.row];
//    [things removeObjectAtIndex:sourceIndexPath.row];
//    [things insertObject:thing atIndex:destinationIndexPath.row];

    
//    for (int i=0; i<[punch count]; i++) {
//        NSLog(@"%@",[punch objectAtIndex:i]);
//    }
    
    
   // NSLog(@"Source %ld , Destination %ld",(long)sourceIndexPath.row,(long)destinationIndexPath.row);
    NSLog(@"%@",punch);

    NSMutableArray *tempobject=[[NSMutableArray alloc]init];
    [tempobject addObject:[punch objectAtIndex:sourceIndexPath.row]];
    [punch removeObjectAtIndex:sourceIndexPath.row];
    
    [punch insertObject:[tempobject objectAtIndex:0] atIndex:destinationIndexPath.row];
   // [punch replaceObjectAtIndex:destinationIndexPath.row withObject:];
 //   [punch replaceObjectAtIndex:sourceIndexPath.row withObject:[tempobject objectAtIndex:0]];
    
    NSLog(@"%@",punch);
    //{"method":"set_punch_priority","punch_priority":[{"punch_id": "1","priority": "2"},{"punch_id": "3","priority": "1"}]}
    
    NSMutableArray *prity=[[NSMutableArray alloc]init];
    int temppri=1;
    for (int i=0; i<punch.count; i++) {
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        [dic setValue:[[punch objectAtIndex:i] valueForKey:@"id"] forKey:@"punch_id"];
        [dic setValue:[NSString stringWithFormat:@"%d",temppri] forKey:@"priority"];
        
        [prity addObject:dic];
        temppri++;
        
    }
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    [dic setObject:@"set_punch_priority" forKey:@"method"];
    
    [dic setObject:prity forKey:@"punch_priority"];
    //[APPDELEGATE addLoader:nil];
    [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"set_punch_priority"];
    
    
    
}
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView==self.projecttableview) {
        if (indexPath.section==1) {
             return YES;
        }
        if (indexPath.section==0) {
            return NO;
        }
    }
    return NO;
}
- (NSIndexPath *)tableView:(UITableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath
{
    
    if (sourceIndexPath.section != proposedDestinationIndexPath.section) {
        NSInteger row = 0;
        if (sourceIndexPath.section < proposedDestinationIndexPath.section) {
            row = [tableView numberOfRowsInSection:sourceIndexPath.section] - 1;
        }
        return [NSIndexPath indexPathForRow:row inSection:sourceIndexPath.section];
    }
    
    return proposedDestinationIndexPath;

}
-(void)punchclick:(UIButton *)sender
{
    
    [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"FromProfileAddPunch"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    CreatePunch *Controller =[[CreatePunch alloc]init];
    [self.navigationController pushViewController:Controller animated:YES];
    

}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    return view;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==self.albumimagestable) {
        if(temp2%3==0)
        {
            return temp2 /3;
        }
        else
        {
            return (temp2/3)+1;
        }
        return temp2;
    }
    else
    {
        if (section==0) {
            
            return [emails count];
        }
        else
        {
            return [punch count];
            
        }
    }
    return  5;
   
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView== self.albumimagestable) {
        return  1;
    }
    return 2;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView==self.albumimagestable)
    {
        
        static NSString *cellIdentifier = @"AlbumTableCell";
        
        AlbumImageCell *cell = (AlbumImageCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier ];
        if (cell == nil)
        {
            cell =[[AlbumImageCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier ];
            
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if(temp2>indexPath.row*3)
        {
            if (imageselect==1) {
                cell.punchview1.albumbtn.hidden=FALSE;
                if([arr_selected containsObject:[NSString stringWithFormat:@"%ld", (long)indexPath.row*3]])
                {
                    cell.punchview1.albumbtn.selected=TRUE;
                }else
                {
                    
                    cell.punchview1.albumbtn.selected=FALSE;
                }
            }
            else
            {
                cell.punchview1.albumbtn.hidden=TRUE;
            }
            [cell.punchview1.activityindicator startAnimating];
            [cell.punchview1.activityindicator hidesWhenStopped];
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                 NSUserDomainMask, YES);
            //
            NSString *url = [NSString stringWithFormat:@"%@%@",ThumbImages,[[imagedata objectAtIndex:indexPath.row*3] valueForKey:@"image"]];
            NSString *filename =[[imagedata objectAtIndex:indexPath.row*3] valueForKey:@"image"] ;
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString* path = [documentsDirectory stringByAppendingPathComponent:filename];
            UIImage* image = [UIImage imageWithContentsOfFile:path];
            if (image==nil) {
                
                NSString *url =  [NSString stringWithFormat:@"%@%@",ThumbImages,[[imagedata objectAtIndex:indexPath.row*3] valueForKey:@"image"]];
                dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                    
                    NSString *str=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                    NSURL *imagepath = [NSURL URLWithString:str];
                    
                    NSData *data = [NSData dataWithContentsOfURL:imagepath];
                    UIImage *image = [UIImage imageWithData:data];
                    [data writeToFile:path atomically:YES];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [ cell.punchview1.addimags setImage:image];
                        [cell.punchview1.activityindicator stopAnimating];
                        
                        
                    });
                });
                
            }
            else
            {
                [ cell.punchview1.addimags setImage:image];
                  [cell.punchview1.activityindicator stopAnimating];
                
            }
            
            
            //            cell.punchview1.addimags.image=[UIImage imageWithData:[albumImagedataarr objectAtIndex:indexPath.row*3]];
            cell.punchview1.hidden=FALSE;
            
            UITapGestureRecognizer *tapgesture1=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(btn_selectimage:)];
            
            //            UILongPressGestureRecognizer *gesture1=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(btn_plus:)];
            
            //  [cell.punchview1.addimags addGestureRecognizer:gesture1];
            [cell.punchview1.addimags addGestureRecognizer:tapgesture1];
            
        }
        else
        {
            cell.punchview1.hidden=TRUE;
            
        }
        
        
        if(temp2>(indexPath.row*3)+1)
        {
            
            if (imageselect==1) {
                cell.punchview2.albumbtn.hidden=FALSE;
                if([arr_selected containsObject:[NSString stringWithFormat:@"%ld", (long)(indexPath.row*3)+1]])
                {
                    cell.punchview2.albumbtn.selected=TRUE;
                }
                else
                {
                    
                    cell.punchview2.albumbtn.selected=FALSE;
                }
            }
            else
            {
                cell.punchview2.albumbtn.hidden=TRUE;
            }
            
            [cell.punchview2.activityindicator startAnimating];
            [cell.punchview2.activityindicator hidesWhenStopped];
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                 NSUserDomainMask, YES);
            //
            NSString *url = [NSString stringWithFormat:@"%@%@",ThumbImages,[[imagedata objectAtIndex:(indexPath.row*3)+1] valueForKey:@"image"]];
            NSString *filename =[[imagedata objectAtIndex:(indexPath.row*3)+1] valueForKey:@"image"] ;
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString* path = [documentsDirectory stringByAppendingPathComponent:filename];
            UIImage* image = [UIImage imageWithContentsOfFile:path];
            if (image==nil) {
                
                NSString *url =  [NSString stringWithFormat:@"%@%@",ThumbImages,[[imagedata objectAtIndex:(indexPath.row*3)+1] valueForKey:@"image"]];
                dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                    
                    NSString *str=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                    NSURL *imagepath = [NSURL URLWithString:str];
                    
                    NSData *data = [NSData dataWithContentsOfURL:imagepath];
                    UIImage *image = [UIImage imageWithData:data];
                    [data writeToFile:path atomically:YES];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [ cell.punchview2.addimags setImage:image];
                          [cell.punchview2.activityindicator stopAnimating];
                        
                        
                    });
                });
                
            }
            else
            {
                [ cell.punchview2.addimags setImage:image];
                  [cell.punchview2.activityindicator stopAnimating];
                
            }
            //            cell.punchview2.addimags.image=[UIImage imageWithData:[albumImagedataarr objectAtIndex:(indexPath.row*3)+1]];
            UITapGestureRecognizer *tapgesture2=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(btn_selectimage:)];
            
            //            UILongPressGestureRecognizer *gesture2=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(btn_plus:)];
            //
            //            [cell.punchview2.addimags addGestureRecognizer:gesture2];
            [cell.punchview2.addimags addGestureRecognizer:tapgesture2];
            cell.punchview2.hidden=FALSE;
        }
        else
        {
            cell.punchview2.hidden=TRUE;
            
        }
        if(temp2>(indexPath.row*3)+2)
        {
            //            if (imageselect==1) {
            //                if([arr_selected containsObject:[NSString stringWithFormat:@"%ld", (long)(indexPath.row*3)+2]])
            //                {
            //                    cell.punchview3.albumbtn.selected=TRUE;
            //                }
            //                else
            //                {
            //
            //                    cell.punchview3.albumbtn.selected=FALSE;
            //                }
            //
            //                cell.punchview3.albumbtn.hidden=FALSE;
            //            }
            //            else
            //            {
            //                cell.punchview3.albumbtn.hidden=TRUE;
            //            }
            //
            //            UITapGestureRecognizer *tapgesture3=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(btn_selectimage:)];
            //
            //            UILongPressGestureRecognizer *gesture3=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(btn_plus:)];
            //
            //            [cell.punchview3.addimags addGestureRecognizer:gesture3];
            //            [cell.punchview3.addimags addGestureRecognizer:tapgesture3];
            //            cell.punchview3.hidden=FALSE;
            
            
            
            if (imageselect==1) {
                cell.punchview3.albumbtn.hidden=FALSE;
                if([arr_selected containsObject:[NSString stringWithFormat:@"%ld", (long)(indexPath.row*3)+2]])
                {
                    cell.punchview3.albumbtn.selected=TRUE;
                }
                else
                {
                    
                    cell.punchview3.albumbtn.selected=FALSE;
                }
            }
            else
            {
                cell.punchview3.albumbtn.hidden=TRUE;
            }
            
            [cell.punchview3.activityindicator startAnimating];
            [cell.punchview3.activityindicator hidesWhenStopped];
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                 NSUserDomainMask, YES);
            //
            NSString *url = [NSString stringWithFormat:@"%@%@",ThumbImages,[[imagedata objectAtIndex:(indexPath.row*3)+2] valueForKey:@"image"]];
            NSString *filename =[[imagedata objectAtIndex:(indexPath.row*3)+2] valueForKey:@"image"];
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString* path = [documentsDirectory stringByAppendingPathComponent:filename];
            UIImage* image = [UIImage imageWithContentsOfFile:path];
            if (image==nil) {
                
                NSString *url =  [NSString stringWithFormat:@"%@%@",ThumbImages,[[imagedata objectAtIndex:(indexPath.row*3)+2] valueForKey:@"image"]];
                dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                    
                    NSString *str=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                    NSURL *imagepath = [NSURL URLWithString:str];
                    
                    NSData *data = [NSData dataWithContentsOfURL:imagepath];
                    UIImage *image = [UIImage imageWithData:data];
                    [data writeToFile:path atomically:YES];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [ cell.punchview3.addimags setImage:image];
                          [cell.punchview3.activityindicator stopAnimating];
                        
                        
                    });
                });
                
            }
            else
            {
                [ cell.punchview3.addimags setImage:image];
                  [cell.punchview3.activityindicator stopAnimating];
                
            }
            //            cell.punchview2.addimags.image=[UIImage imageWithData:[albumImagedataarr objectAtIndex:(indexPath.row*3)+1]];
            UITapGestureRecognizer *tapgesture3=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(btn_selectimage:)];
            
            //            UILongPressGestureRecognizer *gesture3=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(btn_plus:)];
            //
            //            [cell.punchview3.addimags addGestureRecognizer:gesture3];
            [cell.punchview3.addimags addGestureRecognizer:tapgesture3];
            cell.punchview3.hidden=FALSE;
        }
        else
        {
            cell.punchview3.hidden=TRUE;
        }
        
        
        
        
        
        cell.punchview1.addimags.tag=indexPath.row*3;
        
        cell.punchview2.addimags.tag=(indexPath.row*3)+1;
        cell.punchview3.addimags.tag=(indexPath.row*3)+2;
        cell.punchview1.albumbtn.tag=indexPath.row*3;
        
        cell.punchview2.albumbtn.tag=(indexPath.row*3)+1;
        cell.punchview3.albumbtn.tag=(indexPath.row*3)+2;
        cell.backgroundColor=[UIColor clearColor];
        
        return cell;
        
        
    }
    else
    {
        static NSString *cellIdentifier = @"TableViewCell";
        
        UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier ];
        if (cell == nil)
        {
            cell =[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier ];
            
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor=[UIColor clearColor];
        
        switch (indexPath.section) {
            case 0:
            {
                for (UIView *view in cell.contentView.subviews) {
                    [view removeFromSuperview];
                }
                
                cell.textLabel.font=[UIFont fontWithName:@"OpenSans" size:15.0];
                UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(48, 0, [[UIScreen mainScreen] bounds].size.width-30, 30)];
                label.font=[UIFont fontWithName:@"OpenSans" size:15.0];
                
                label.text=[emails objectAtIndex:indexPath.row];
                label.textColor=[UIColor whiteColor];
                
                UITextView *textview=[[UITextView alloc]initWithFrame:CGRectMake(48, 0, [[UIScreen mainScreen] bounds].size.width-30, 30)];
                textview.font=[UIFont fontWithName:@"OpenSans" size:15.0];
                
                textview.text=[emails objectAtIndex:indexPath.row];
                textview.textColor=[UIColor whiteColor];
                textview.tintColor=[UIColor whiteColor];
                textview.backgroundColor=[UIColor clearColor];
               textview.dataDetectorTypes = UIDataDetectorTypeAll;
                textview.editable=FALSE;
                
               // [cell.contentView addSubview:textview];
                
                break;
            }
            case 1:
            {
                static NSString *cellIdentifier = @"ProjectDetailCell";
                
                ProjectDetailcell *cell = (ProjectDetailcell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier ];
                if (cell == nil)
                {
                    cell =[[ProjectDetailcell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier ];
                    
                }
                cell.labeltitle.text=[[punch objectAtIndex:indexPath.row] valueForKey:@"punch_title"];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.backgroundColor=[UIColor clearColor];
                
                cell.imageround.layer.cornerRadius = cell.imageround.frame.size.width / 2;
                cell.imageround.clipsToBounds = YES;
                if ([[[punch objectAtIndex:indexPath.row]valueForKey:@"is_complete"]isEqualToString:@"1"]) {
//                     cell.accessoryView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"checked"]];
//                     cell.editingAccessoryView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"checked"]];

                    cell.punchstatus.hidden=FALSE;
                    cell.punchstatus.selected=TRUE;
                }
                else
                {
                  cell.punchstatus.hidden=TRUE;
                }
               
                
                return cell;
                break;
            }
        }

        cell.backgroundColor=[UIColor clearColor];
        return cell;
    }
}

-(IBAction)emailContent:(id)sender
{
    [self.sharingview removeFromSuperview];

    MFMailComposeViewController *emailShareController = [[MFMailComposeViewController alloc] init];
    emailShareController.mailComposeDelegate = self;
    //    [emailShareController setSubject:@"Punch Sharing"];
    //    [emailShareController setMessageBody:message isHTML:NO];
    
    
    NSData *jpegData=  UIImageJPEGRepresentation([UIImage imageWithData:[arrImages objectAtIndex:indexofimage]], 1);
    [emailShareController addAttachmentData:jpegData mimeType:@"image/jpeg" fileName:[NSString stringWithFormat:@"image.jpg"]];
    //[controller addImage:[UIImage imageWithData:[shringimage objectAtIndex:i]]];


    NSString *strCompanyName;
    NSString *strWebsiteURL;
    if ([[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails]valueForKey:@"role"]isEqualToString:@"CU"])
    {
        NSDictionary *selectedClient = [[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedClient"];
        strCompanyName = [selectedClient valueForKey:@"company_name"];
        strWebsiteURL = [selectedClient valueForKey:@"website"];
    }
    else
    {
        NSDictionary *userDetail = [[NSUserDefaults standardUserDefaults]valueForKey:@"LoginUserDetail"];
        strCompanyName = [userDetail valueForKey:@"company_name"];
        strWebsiteURL = [userDetail valueForKey:@"website"];
    }
   
    if ([strWebsiteURL rangeOfString:@"http://"].location==NSNotFound)
        strWebsiteURL = [NSString stringWithFormat:@"http://%@",strWebsiteURL];
        
    NSMutableString *emailBody = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"<a href=%@>%@</a>",strWebsiteURL,strCompanyName]];
    [emailBody appendString:@"&nbsp;|&nbsp;powered by "];
    [emailBody appendString:@"<a href=\"http://www.buildaga.com\">Buildaga</a>"];
    
    [emailShareController setMessageBody:emailBody isHTML:YES];
    [emailShareController setToRecipients:emails];
    if (emailShareController) [self presentViewController:emailShareController animated:YES completion:nil];
    
}
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
//    [self.sharingview removeFromSuperview];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
//    if (tableView==self.albumimagestable)
//    {
//        return  0;
//    }else 
//    return 36.0;
    
    if (tableView==self.albumimagestable)
    {
        return  0;
    }
    else
    {
        if (section==0) {
            return 0;
        }
        return 36.0;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0;
}
- (IBAction)back_btn_click:(id)sender {
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"EditProject"];
    
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==1) {
       
        [[NSUserDefaults standardUserDefaults]setValue:[punch objectAtIndex:indexPath.row] forKey:@"punch_id"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        PunchListViewViewController *Projects=[[PunchListViewViewController alloc]init];
        [self.navigationController pushViewController:Projects animated:YES];
    }
    
    
}
- (IBAction)detail_album_click:(UIButton *)sender {
    
    
    
    if (sender==self.album || sender==self.album1) {
        if (self.detailbtn.selected || self.detailbtn1.selected) {
            [self.detailbtn setSelected:FALSE];
            [self.detailbtn1 setSelected:FALSE];
            
            
        }
        if (sender.isSelected) {
            [self.album setSelected:TRUE];
            [self.album1 setSelected:TRUE];
            
            
        }
        else
        {
            [sender setSelected:TRUE];
            [self.album1 setSelected:TRUE];
            self.albumview.frame=CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
            
            [self.view addSubview:self.albumview];
            [self.albumimagestable reloadData];
        }
        
        
    }
    else
    {
        if (self.album.selected || self.album1.selected) {
            [self.album setSelected:FALSE];
            [self.album1 setSelected:FALSE];
            
            
            
        }
        if (sender.isSelected) {
            
            [self.detailbtn setSelected:TRUE];
            [self.detailbtn1 setSelected:TRUE];
        }
        else
        {
            [sender setSelected:TRUE];
            [self.detailbtn1 setSelected:TRUE];
            [self.detailbtn setSelected:TRUE];
            [self.albumview removeFromSuperview];
            
        }
        
    }
}

- (NSData *)compressImage:(UIImage *)image
{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 1000.0;
    float maxWidth = 1000.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 1.0;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth) {
        if(imgRatio < maxRatio){
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio){
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }else{
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
    return imageData;
}
-(IBAction)btn_plus:(UIButton *)sender
{
    //    if (sender.state == UIGestureRecognizerStateEnded) {
    //               //Do Whatever You want on End of Gesture
    //    }
    //    else if (sender.state == UIGestureRecognizerStateBegan){
    //        NSLog(@"UIGestureRecognizerStateBegan.");
    //        if ([sender isKindOfClass:[UIButton class]])
    //        {
    //
    //
    //        }
    //        else
    //        {
    //            if (imageselect==0)
    //            {
    //                imageselect=1;
    //                [self.albumimagestable reloadData];
    //                self.deletebtn.hidden=FALSE;
    //                self.deletebtn.selected=TRUE;
    //                self.sharingbtn.hidden=FALSE;
    //
    //            }
    //            else
    //            {
    //
    //                self.deletebtn.hidden=TRUE;
    //                self.sharingbtn.hidden=TRUE;
    //                self.deletebtn.selected=FALSE;
    //                imageselect=0;
    //                [arr_selected removeAllObjects];
    //                [self.albumimagestable reloadData];
    //            }
    //  }
    
    if ([[NSUserDefaults standardUserDefaults]valueForKey:@"EditProject"]==nil) {
        [[NSUserDefaults standardUserDefaults]setObject:[[NSUserDefaults standardUserDefaults]valueForKey:@"TempProject"] forKey:@"EditProject"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    [[NSUserDefaults standardUserDefaults]setObject:@"ThroughAlbum" forKey:@"ThroughAlbum"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    CreateProject *project=[[CreateProject alloc]init];
    [self.navigationController pushViewController:project animated:NO];
    
    //Do Whatever You want on Began of Gesture
    //  }
    
}

- (IBAction)share_Cnacel:(id)sender {
     [self.sharingview removeFromSuperview];
}

-(void)btn_selectimage:(UITapGestureRecognizer *)sender
{
    UIView *view = sender.view;
    
    NSLog(@"%ld", (long)view.tag);
    if (imageselect==0) {
        self.imageslideview.frame=CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
        [self.view addSubview:self.imageslideview];
        [self scrollImages:view.tag];

    }
    else
    {
        if ([arr_selected containsObject:[NSString stringWithFormat:@"%ld", (long)view.tag] ])
        {
            [arr_selected removeObject:[NSString stringWithFormat:@"%ld", (long)view.tag]];
            
            NSLog(@"%ld",(view.tag /3));
            
            [self.albumimagestable reloadData];
            // [self.albumimagestable beginUpdates];
            
            
            //        NSIndexPath *myIP = [NSIndexPath indexPathForRow:view.tag/3 inSection:0];
            //        NSArray *myArray = [[NSArray alloc] initWithObjects:myIP, nil];
            //
            //        [self.albumimagestable reloadRowsAtIndexPaths:myArray withRowAnimation:UITableViewRowAnimationNone];
            //        [self.albumimagestable endUpdates];
            
            
        }
        else
        {
            [arr_selected addObject:[NSString stringWithFormat:@"%ld", (long)view.tag]];
            
            [self.albumimagestable reloadData];
            
            //[self.albumimagestable beginUpdates];
            
            
            //        NSIndexPath *myIP = [NSIndexPath indexPathForRow:view.tag/3 inSection:0];
            //        NSArray *myArray = [[NSArray alloc] initWithObjects:myIP, nil];
            //
            //        [self.albumimagestable reloadRowsAtIndexPaths:myArray withRowAnimation:UITableViewRowAnimationNone];
            //        [self.albumimagestable endUpdates];
        }
    }
    // NSLog(@"%ld",(long)[sender tag]);
    
}
-(void)scrollImages:(NSInteger )indexofImage
{
    
    indexofimage=(int)indexofImage;
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
//                                                         NSUserDomainMask, YES);
//    //
//    NSString *url = [NSString stringWithFormat:@"%@%@",ProjectImages,[[imagedata objectAtIndex:indexofImage] valueForKey:@"image"]];
//    NSString *filename =[NSString stringWithFormat:@"Project_%@",[[imagedata objectAtIndex:indexofImage] valueForKey:@"image"]];
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//    NSString* path = [documentsDirectory stringByAppendingPathComponent:filename];
//    UIImage* image = [UIImage imageWithContentsOfFile:path];
//    if (image==nil) {
//        
//        NSString *url =  [NSString stringWithFormat:@"%@%@",ProjectImages,[[imagedata objectAtIndex:indexofImage] valueForKey:@"image"]];
//        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
//            
//            NSString *str=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//            NSURL *imagepath = [NSURL URLWithString:str];
//            
//            NSData *data = [NSData dataWithContentsOfURL:imagepath];
//            // UIImage *image = [UIImage imageWithData:data];
//            [data writeToFile:path atomically:YES];
//            
//            dispatch_async(dispatch_get_main_queue(), ^{
//                
//                [arrImages replaceObjectAtIndex:indexofImage withObject:data];
//                
//                
//            });
//        });
//        
//    }
    
    for (int i = 0; i < [arrImages count]; i++)
    {
        //We'll create an imageView object in every 'page' of our scrollView.
        
        CGRect frame;
        frame.origin.x = ([[UIScreen mainScreen] bounds].size.width) * i;
        frame.origin.y = 0;
        frame.size.height = [[UIScreen mainScreen] bounds].size.height;
        frame.size.width=[[UIScreen mainScreen] bounds].size.width;
        
        // UIImageView *imgview = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, self.BigImageScroll.frame.size.height)];
        UIView *view =nil;
        NSLog(@"Scrollview Height : %f",self.imageScrollView.frame.size.height);
        view=[[UIView alloc] initWithFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width * i), 0, frame.size.width , frame.size.height - 130)];
        UIImageView *imgview = [[UIImageView alloc] initWithImage:[UIImage imageWithData:[arrImages objectAtIndex:i]]];
        
        imgview.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width , frame.size.height - 130);
        imgview.contentMode = UIViewContentModeScaleAspectFit;
        
        //UIImage *Newimage = [self.arrImages objectAtIndex:i];
        
        //UIImage *newsizeimage = [self aspectScaledImageSizeForImageView:imgview image:Newimage];
        
        //imgview.contentMode = UIViewContentModeScaleAspectFit;
        
        //imgview.clipsToBounds = YES;
        //imgview.image = newsizeimage;
        
        
        imgview.image=[UIImage imageWithData:[arrImages objectAtIndex:i]];
        //imgview.frame = CGRectMake(0, 0, imgview.image.size.width, imgview.image.size.height);


        [view addSubview:imgview];
        [self.imageScrollView addSubview:view];

    }

    
    self.imageScrollView.contentSize = CGSizeMake(([[UIScreen mainScreen] bounds].size.width) * [arrImages count],self.imageScrollView.frame.size.height);
    
    [self.imageScrollView setContentOffset:CGPointMake([[UIScreen mainScreen] bounds].size.width * indexofImage, 0.0f) animated:NO];
    self.pageControl.numberOfPages = [arrImages count];
//    self.pageControl.transform = CGAffineTransformMakeScale(0.5, 0.5);
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    


//    [self.imageslideview removeFromSuperview];
//    [self.sharingview removeFromSuperview];
    
    
}
- (IBAction)Share_btn_click:(id)sender {
    
    self.sharingview.frame=CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
    [self.view addSubview:self.sharingview];
}

- (IBAction)fbsharing_btn_click:(id)sender {
  
    [self.sharingview removeFromSuperview];
  
    NSString *strCompanyName;
    NSString *strWebsiteURL;
    
    if ([[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails]valueForKey:@"role"]isEqualToString:@"CU"])
    {
        NSDictionary *selectedClient = [[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedClient"];
        strCompanyName = [selectedClient valueForKey:@"company_name"];
//        strWebsiteURL = [selectedClient valueForKey:@"website"];
    }
    else
    {
        NSDictionary *userDetail = [[NSUserDefaults standardUserDefaults]valueForKey:@"LoginUserDetail"];
        strCompanyName = [userDetail valueForKey:@"company_name"];
//        strWebsiteURL = [userDetail valueForKey:@"website"];
    }
//    if ([strWebsiteURL rangeOfString:@"http://"].location==NSNotFound)
//        strWebsiteURL = [NSString stringWithFormat:@"http://%@",strWebsiteURL];

    strWebsiteURL = [NSString stringWithFormat:@"%@%@",kSharingUrl_New,[[[NSUserDefaults standardUserDefaults]valueForKey:@"SelectedProject"] valueForKey:@"hash_code"]];
    
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentDescription = [NSString stringWithFormat:@"Check out the work that is being done on this project by %@ | powered by Buildaga",strCompanyName];
    content.contentTitle = self.nameofproject1.text;

    @try {
        content.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ProjectImages,[[imagedata objectAtIndex:indexofimage] valueForKey:@"image"]]];
    }
    @catch (NSException *exception) {
         content.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ProjectImages,[[imagedata objectAtIndex:indexofimage-1] valueForKey:@"image"]]];
    }
  
//    if([[imagedata objectAtIndex:indexofimage] valueForKey:@"image"] != nil)
//        content.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ProjectImages,[[imagedata objectAtIndex:indexofimage] valueForKey:@"image"]]];

    content.contentURL = [NSURL URLWithString:strWebsiteURL];
    
    FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
    dialog.fromViewController = self;
    dialog.shareContent = content;
    dialog.mode = FBSDKShareDialogModeNative; // if you don't set this before canShow call, canShow would always return YES
    if (![dialog canShow]) {
        // fallback presentation when there is no FB app
        dialog.mode = FBSDKShareDialogModeFeedWeb;
    }
    [dialog show];

 
    
//    [FBSDKShareDialog showFromViewController:self
//                                 withContent:content
//                                    delegate:nil];
    
 /*   SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];

//    [controller addURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ProjectImages,[[imagedata objectAtIndex:indexofimage] valueForKey:@"image"]]]];

//    [controller setInitialText:@"|www.Company.com powered by Buildaga"];
    [controller addImage:[UIImage imageWithData:[arrImages objectAtIndex:indexofimage]]];
    [controller addURL:[NSURL URLWithString:@"www.buildaga.com"]];    
   
    [self presentViewController:controller animated:YES completion:Nil];
    [controller setCompletionHandler:^(SLComposeViewControllerResult result) {
        NSLog(@"start completion block");
        NSString *output;
        switch (result) {
            case SLComposeViewControllerResultCancelled:
                output = @"Action Cancelled";
                break;
            case SLComposeViewControllerResultDone:
            {
                [self.sharingview removeFromSuperview];
                output = @"Post Successfull";
                break;
            }
            default:
                break;
        }
//        if (result != SLComposeViewControllerResultCancelled)
//        {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Punch List" message:output delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//            [alert show];
//        }
        
    }];*/
}
- (IBAction)edit_btn_click:(id)sender {
    
    if ([[NSUserDefaults standardUserDefaults]valueForKey:@"EditProject"]==nil) {
        [[NSUserDefaults standardUserDefaults]setObject:[[NSUserDefaults standardUserDefaults]valueForKey:@"TempProject"] forKey:@"EditProject"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }

    CreateProject *project=[[CreateProject alloc]init];
    [self.navigationController pushViewController:project animated:NO];
}

#pragma mark web service
-(void)webHandlerManager:(Webservice *)webHandlerManager didFinishLoadingWithData:(NSObject *)responseObject
{
    [APPDELEGATE removeLoader];
    if ([webHandlerManager.strMethod isEqualToString:@"project_detail"])
    {
        
        //http://hyperlinkserver.com/punchlist/project_images/thumb/55aa2aa18524f1437215393.jpg
        if([[responseObject valueForKey:@"sucess"]isEqualToString:@"1"])
        {
            [[NSUserDefaults standardUserDefaults]setObject:responseObject forKey:@"EditProject"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [[NSUserDefaults standardUserDefaults]setObject:responseObject forKey:@"TempProject"];
            
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            emails=nil;
            emails=[[responseObject valueForKey:@"email_data"] valueForKey:@"email"];
            NSIndexSet *sections = [[NSIndexSet alloc] initWithIndex:0];
//            
//           
//            [self.projecttableview reloadSections:sections withRowAnimation:UITableViewRowAnimationFade];
            punch=nil;
            punch=[responseObject valueForKey:@"punch_data"];
            [self.projecttableview reloadData];
//            sections=nil;
//            sections=[[NSIndexSet alloc] initWithIndex:1];
//            [self.projecttableview reloadSections:sections withRowAnimation:UITableViewRowAnimationFade];
            imagedata=[responseObject valueForKey:@"image_data"];
            temp2=(int)[imagedata count];
            sections=nil;
            sections=[[NSIndexSet alloc] initWithIndex:0];
            [self.albumimagestable reloadSections:sections withRowAnimation:UITableViewRowAnimationFade];
        
            for (int i=0; i<[imagedata count]; i++) {
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                     NSUserDomainMask, YES);
                //
                NSString *url = [NSString stringWithFormat:@"%@%@",ProjectImages,[[imagedata objectAtIndex:i] valueForKey:@"image"]];
                NSString *filename =[NSString stringWithFormat:@"Project_%@",[[imagedata objectAtIndex:i] valueForKey:@"image"]];
                NSString *documentsDirectory = [paths objectAtIndex:0];
                NSString* path = [documentsDirectory stringByAppendingPathComponent:filename];
                UIImage* image = [UIImage imageWithContentsOfFile:path];
                if (image==nil) {
                    
                    NSString *url =  [NSString stringWithFormat:@"%@%@",ProjectImages,[[imagedata objectAtIndex:i] valueForKey:@"image"]];
                    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                        
                        NSString *str=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                        NSURL *imagepath = [NSURL URLWithString:str];
                        
                        NSData *data = [NSData dataWithContentsOfURL:imagepath];
                        // UIImage *image = [UIImage imageWithData:data];
                        [data writeToFile:path atomically:YES];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [arrImages addObject:data];
                            
                            
                        });
                    });
                    
                }
                else
                {
                    
                    [arrImages addObject:[NSData dataWithContentsOfFile:path]];
                }
                
            }
        }
        else
        {
            
            
        }
    }
    if ([webHandlerManager.strMethod isEqualToString:@"set_punch_priority"])
    {
        
        //http://hyperlinkserver.com/punchlist/project_images/thumb/55aa2aa18524f1437215393.jpg
        if([[responseObject valueForKey:@"sucess"]isEqualToString:@"1"])
        {
            
        }
        else
        {
            
            
        }
    }
}
-(void)webHandlerManager:(Webservice *)webHandlerManager didFailedLoadingWithError:(NSString *)responseError
{
    
    NSLog(@"%@",responseError);
    NSLog(@"%@",webHandlerManager.strMethod);
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
