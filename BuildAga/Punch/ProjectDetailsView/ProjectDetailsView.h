//
//  ProjectDetailsView.h
//  Punch
//
//  Created by Jaimish on 06/07/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "SDWebImage/UIImageView+WebCache.h"
#import "UIImageView+WebCache.h"


@interface ProjectDetailsView : UIViewController<UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *projecttableview;
@property (strong, nonatomic) IBOutlet UIView *albumview;
@property (strong, nonatomic) IBOutlet UIButton *detailbtn;
@property (strong, nonatomic) IBOutlet UIButton *album;
@property (strong, nonatomic) IBOutlet UIButton *detailbtn1;
@property (strong, nonatomic) IBOutlet UIButton *album1;
@property (strong, nonatomic) IBOutlet UIButton *deletebtn;
@property (strong, nonatomic) IBOutlet UITableView *albumimagestable;
@property (strong, nonatomic) IBOutlet UIView *imageslideview;

@property (strong, nonatomic) IBOutlet UIScrollView *imageScrollView;
@property (strong, nonatomic) IBOutlet UIView *sharingview;
@property (strong, nonatomic) IBOutlet UILabel *projectname;
@property (strong, nonatomic) IBOutlet UILabel *nameofproject;
@property (strong, nonatomic) IBOutlet UILabel *nameofproject1;
@property (strong, nonatomic) IBOutlet UIButton *sharingbtn;
@property (strong, nonatomic) IBOutlet UIImageView *imageshowbg;

@property (strong, nonatomic) IBOutlet UIImageView *sharingbg;
@property(strong,nonatomic) IBOutlet UIPageControl  *pageControl;

-(IBAction)btnImageViewRemoveClicked:(id)sender;
@end
