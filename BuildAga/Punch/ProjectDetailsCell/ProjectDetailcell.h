//
//  ProjectDetailcell.h
//  Punch
//
//  Created by Jaimish on 09/07/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProjectDetailcell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *labeltitle;
@property (strong, nonatomic) IBOutlet UIImageView *imageround;
@property (strong, nonatomic) IBOutlet UIButton *punchstatus;

@end
