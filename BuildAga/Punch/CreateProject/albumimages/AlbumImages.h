//
//  AlbumImages.h
//  Punch
//
//  Created by Jaimish on 04/07/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlbumImages : UIView
@property(nonatomic,retain) IBOutlet UIView *viewMain;
@property(nonatomic,retain) IBOutlet UIImageView *addimags;
@property (strong, nonatomic) IBOutlet UIButton *albumbtn;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityindicator;
@end
