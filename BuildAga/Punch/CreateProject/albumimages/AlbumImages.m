//
//  AlbumImages.m
//  Punch
//
//  Created by Jaimish on 04/07/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import "AlbumImages.h"

@implementation AlbumImages
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        
        self.viewMain = [[[NSBundle mainBundle] loadNibNamed:@"AlbumImages" owner:self options:nil] objectAtIndex:0];
        
        [self addSubview:self.viewMain];
        self.viewMain.backgroundColor=[UIColor clearColor];
        
        // Initialization code
    }
    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
