//
//  ContactDetailCell.h
//  Punch
//
//  Created by Jaimish on 29/07/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactDetailCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UITextField *firstname;
@property (strong, nonatomic) IBOutlet UITextField *phonenumber;
@property (strong, nonatomic) IBOutlet UITextField *email;
@property (strong, nonatomic) IBOutlet UITextField *address;

@property (strong, nonatomic) IBOutlet UITextField *city;
@property (strong, nonatomic) IBOutlet UITextField *state;
@property (strong, nonatomic) IBOutlet UITextField *zip;

@property (strong, nonatomic) IBOutlet UITextField *lastname;

@end
