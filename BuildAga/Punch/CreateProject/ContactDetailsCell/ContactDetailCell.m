//
//  ContactDetailCell.m
//  Punch
//
//  Created by Jaimish on 29/07/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import "ContactDetailCell.h"

@implementation ContactDetailCell
@synthesize firstname,phonenumber,email,address,city,state,zip,lastname;
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ContactDetailCell" owner:self options:nil];
        self = [nib objectAtIndex:0];
        
    }
    return self;
}

@end
