//
//  AlbumImageCell.h
//  Punch
//
//  Created by Jaimish on 04/07/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlbumImages.h"
@interface AlbumImageCell : UITableViewCell
@property (strong, nonatomic) IBOutlet AlbumImages *punchview1;
@property (strong, nonatomic) IBOutlet AlbumImages *punchview2;
@property (strong, nonatomic) IBOutlet AlbumImages *punchview3;
@end
