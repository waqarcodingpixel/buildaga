//
//  AlbumImageCell.m
//  Punch
//
//  Created by Jaimish on 04/07/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import "AlbumImageCell.h"

@implementation AlbumImageCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        /*  NSInteger temp = [[UIScreen mainScreen]bounds].size.width - 300;
        
        self.obj1=[[ProfileRideView alloc]initWithFrame:CGRectMake(temp / 4, 10, 100, 120)];
        [self.contentView addSubviewelf.obj1];
        
        self.obj2=[[ProfileRideView alloc]initWithFrame:CGRectMake(obj1.frame.size.width + obj1.frame.origin.x + (temp / 4), 10, 100, 120)];
        [self.contentView addSubviewelf.obj2];
        
        
        self.obj3=[[ProfileRideView alloc]initWithFrame:CGRectMake(obj2.frame.size.width + obj2.frame.origin.x + (temp / 4), 10, 100, 120)];
        [self.contentView addSubviewelf.obj3];
        // Initialization code
         */
        NSInteger temp = [[UIScreen mainScreen]bounds].size.width - 300;
        self.punchview1=[[AlbumImages alloc]initWithFrame:CGRectMake(temp / 4, 10, 100, 120)];
        [self.contentView addSubview:self.punchview1];
        self.punchview2=[[AlbumImages alloc]initWithFrame:CGRectMake(self.punchview1.frame.size.width + self.punchview1.frame.origin.x + (temp / 4), 10, 100, 120)];
        [self.contentView addSubview:self.punchview2];
        self.punchview3=[[AlbumImages alloc]initWithFrame:CGRectMake(self.punchview2.frame.size.width + self.punchview2.frame.origin.x + (temp / 4), 10, 100, 120)];
        [self.contentView addSubview:self.punchview3];
        
        
    }
    return self;
}

@end
