//
//  PunchcellTableViewCell.h
//  Punch
//
//  Created by Jaimish on 02/07/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Punchview.h"
@interface PunchcellTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet Punchview *punchview1;
@property (strong, nonatomic) IBOutlet Punchview *punchview2;
@property (strong, nonatomic) IBOutlet Punchview *punchview3;
@end
