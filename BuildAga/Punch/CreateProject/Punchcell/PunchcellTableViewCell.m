//
//  PunchcellTableViewCell.m
//  Punch
//
//  Created by Jaimish on 02/07/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import "PunchcellTableViewCell.h"

@implementation PunchcellTableViewCell
@synthesize punchview1,punchview2,punchview3;
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.punchview1=[[Punchview alloc]initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width/3*0+30, 0, [[UIScreen mainScreen] bounds].size.width/3-20, 80)];
        [self.contentView addSubview:self.punchview1];
           self.punchview2=[[Punchview alloc]initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width/3*1+15, 0, [[UIScreen mainScreen] bounds].size.width/3-20, 80)];
         [self.contentView addSubview:self.punchview2];
        self.punchview3=[[Punchview alloc]initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width/3*2, 0, [[UIScreen mainScreen] bounds].size.width/3-20, 80)];
         [self.contentView addSubview:self.punchview3];
        
    }
    return self;
}
@end
