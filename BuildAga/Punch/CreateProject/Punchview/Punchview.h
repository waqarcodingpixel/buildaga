//
//  Punchview.h
//  Punch
//
//  Created by Jaimish on 02/07/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Punchview : UIView
@property(nonatomic,retain) IBOutlet UIView *viewMain;
@property(nonatomic,retain) IBOutlet UIButton *addPunch;
@end
