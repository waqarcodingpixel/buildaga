//
//  Punchview.m
//  Punch
//
//  Created by Jaimish on 02/07/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import "Punchview.h"

@interface Punchview ()

@end

@implementation Punchview
@synthesize viewMain,addPunch;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        
        self.viewMain = [[[NSBundle mainBundle] loadNibNamed:@"Punchview" owner:self options:nil] objectAtIndex:0];
        self.addPunch.titleLabel.numberOfLines=0;
        [self addSubview:self.viewMain];
        self.viewMain.backgroundColor=[UIColor clearColor];
        
        // Initialization code
    }
    return self;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
