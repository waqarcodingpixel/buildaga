//
//  CreateProject.h
//  Punch
//
//  Created by Jaimish on 02/07/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateProject : UIViewController<UITextFieldDelegate,UITableViewDelegate,UICollectionViewDataSource,UITableViewDataSource,UICollectionViewDelegate,UIPickerViewDelegate>
{
    UIPickerView *State_List;
    NSMutableArray *State_List_Array;
    NSMutableArray *State_List_iso;
    NSString *string_value;
    NSString *string_IOS;
    NSInteger string_value_int;
    UIImageView *imgview;
}
@property (strong, nonatomic) IBOutlet UIView *headerview;
@property (strong, nonatomic) IBOutlet UIView *albumview;

@property (strong, nonatomic) IBOutlet UIButton *detailbtn;
@property (strong, nonatomic) IBOutlet UIButton *album;
@property (strong, nonatomic) IBOutlet UIView *footerview;
@property (strong, nonatomic) IBOutlet UITableView *tableview;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionview;
@property (strong, nonatomic) IBOutlet UITableView *footertableview;
@property (strong, nonatomic) IBOutlet UIButton *detailbtn1;
@property (strong, nonatomic) IBOutlet UIButton *album1;
@property (strong, nonatomic) IBOutlet UITableView *albumimagestable;
@property (strong, nonatomic) IBOutlet UITextField *txt_title;
@property (strong, nonatomic) IBOutlet UIView *imageslideview;
@property (strong, nonatomic) IBOutlet UILabel *punchlabel;
@property (strong, nonatomic) IBOutlet UIButton *punchbtn;
@property (strong, nonatomic) IBOutlet UIImageView *punchline;
@property (strong, nonatomic) IBOutlet UIScrollView *imageScrollView;
@property (strong, nonatomic) IBOutlet UILabel *createprojectlable;
@property (strong, nonatomic) IBOutlet UILabel *createprojectlabel;
@property (strong, nonatomic) IBOutlet UIButton *deletebtn;
@property (strong, nonatomic) IBOutlet UIButton *sharingbtn;
@property (strong, nonatomic) IBOutlet UIButton *editalbumbtnback;

@property (strong, nonatomic) IBOutlet UIView *newfooterview;
@property (strong, nonatomic) IBOutlet UILabel *newpunchlabel;
@property (strong, nonatomic) IBOutlet UIButton *newpunchbtn;
@property (strong, nonatomic) IBOutlet UIImageView *newfooter;
@property (strong, nonatomic) IBOutlet UITableView *newfootertable;
@property (strong, nonatomic) IBOutlet UIButton *addemailbtn;

@property (strong, nonatomic) IBOutlet UIView *shringview;
@property (strong, nonatomic) IBOutlet UIButton *facebookshringbtn;
@property (strong, nonatomic) IBOutlet UIImageView *sharingbg;



@end
