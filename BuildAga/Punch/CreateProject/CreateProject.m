//
//  CreateProject.m
//  Punch
//
//  Created by Jaimish on 02/07/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
//sortedArray=[anArray sortedArrayUsingDescriptors:@[sort]];

#import "CreateProject.h"
#import "PunchcellTableViewCell.h"
#import "CreatePunch.h"
#import "UzysAssetsPickerController.h"
#import "AlbumImageCell.h"
#import "ASIFormDataRequest.h"
#import "ASIHTTPRequest.h"
#import "SBJsonParser1.h"
#import "ContactDetailCell.h"
#import "SBJsonWriter1.h"
#import "ProjectDetailcell.h"
#import <MessageUI/MFMailComposeViewController.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <MessageUI/MessageUI.h>

#import <Social/Social.h>

@interface CreateProject ()<UzysAssetsPickerControllerDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,UIAlertViewDelegate>
{
    NSMutableArray *emails,*punch;
    NSInteger countemails,countpunch;
    int temp,temp2,imageselect;
    NSMutableArray *albumImagedataarr,*orignalimages,*arr_selected,*deletedimages,*tempimages;
    int projectCreated;
    
    UIButton *buttoncreate;
    NSMutableDictionary *dicdetails;
    NSMutableArray *shringimage;
    
    NSMutableArray *editarray,*punchlist,*detailsobject;
}
@end

@implementation CreateProject
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([alertView tag]==1000) {
        
        if (buttonIndex==0) {
            
            if ([arr_selected count]>0)
            {
                NSMutableArray *deleteimages=[[NSMutableArray alloc]init];
                for(int i=0;i<[arr_selected count];i++)
                {
                    
                    [deleteimages addObject:[[[editarray valueForKey:@"image_data"] valueForKey:@"id"] objectAtIndex:[[NSString stringWithFormat:@"%@",[arr_selected objectAtIndex:i]]integerValue]-1]];
                    
                    
                    [deletedimages addObject:[albumImagedataarr  objectAtIndex:[[NSString stringWithFormat:@"%@",[arr_selected objectAtIndex:i]]integerValue]]];
                }
                //             NSMutableArray *arraytemp=[[NSMutableArray alloc]init];
                //             arraytemp = [[editarray valueForKey:@"image_data"] mutableCopy];
                //             for(int i=0;i<[arr_selected count];i++)
                //             {
                //
                //                 [arraytemp removeObjectAtIndex:[[NSString stringWithFormat:@"%@",[arr_selected objectAtIndex:i]]integerValue]-1];
                //             }
                //             NSMutableArray *arraytemp1=[editarray mutableCopy];
                //             [arraytemp1 setValue:arraytemp forKey:@"image_data"];
                //             editarray=nil;
                //             editarray=[[NSMutableArray alloc]init];
                //
                //             editarray=arraytemp1;
                NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                [dic setObject:@"delete_image" forKey:@"method"];
                NSString *joinedComponents = [deleteimages componentsJoinedByString:@","];
                [dic setObject:[[editarray valueForKey:KData] valueForKey:@"id"] forKey:@"project_id"];
                [dic setObject:joinedComponents forKey:@"image_id"];
                [APPDELEGATE addLoader:nil];
                [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"delete_image"];
            }
        }
    }
    
    
    
    
    
}
- (void)viewDidLoad {
    
    
    
    string_value=@"";
    string_IOS=@"";
    
    
    
    State_List_iso=[[NSMutableArray alloc]initWithObjects:@"AL",
                    @"AK",
                    @"AZ",
                    @"AR",
                    @"CA",
                    @"CO",
                    @"CT",
                    @"DE",
                    @"DC",
                    @"FL",
                    @"GA",
                    @"HI",
                    @"ID",
                    @"IL",
                    @"IN",
                    @"IA",
                    @"KS",
                    @"KY",
                    @"LA",
                    @"ME",
                    @"MD",
                    @"MA",
                    @"MI",
                    @"MN",
                    @"MS",
                    @"MO",
                    @"MT",
                    @"NE",
                    @"NV",
                    @"NH",
                    @"NJ",
                    @"NM",
                    @"NY",
                    @"NC",
                    @"ND",
                    @"OH",
                    @"OK",
                    @"OR",
                    @"PA",
                    @"RI",
                    @"SC",
                    @"SD",
                    @"TN",
                    @"TX",
                    @"UT",
                    @"VT",
                    @"VA",
                    @"WA",
                    @"WV",
                    @"WI",
                    @"WY", nil];
    
    
    
    State_List_Array=[[NSMutableArray alloc]initWithObjects:@"Alabama",
                      @"Alaska",
                      @"Arizona",
                      @"Arkansas",
                      @"California",
                      @"Colorado",
                      @"Connecticut",
                      @"Delaware",
                      @"District of Columbia",
                      @"Florida",
                      @"Georgia",
                      @"Hawaii",
                      @"Idaho",
                      @"Illinois",
                      @"Indiana",
                      @"Iowa",
                      @"Kansas",
                      @"Kentucky",
                      @"Louisiana",
                      @"Maine",
                      @"Maryland",
                      @"Massachusetts",
                      @"Michigan",
                      @"Minnesota",
                      @"Mississippi",
                      @"Missouri",
                      @"Montana",
                      @"Nebraska",
                      @"Nevada",
                      @"New Hampshire",
                      @"New Jersey",
                      @"New Mexico",
                      @"New York",
                      @"North Carolina",
                      @"North Dakota",
                      @"Ohio",
                      @"Oklahoma",
                      @"Oregon",
                      @"Pennsylvania",
                      @"Rhode Island",
                      @"South Carolina",
                      @"South Dakota",
                      @"Tennessee",
                      @"Texas",
                      @"Utah",
                      @"Vermont",
                      @"Virginia",
                      @"Washington",
                      @"West Virginia",
                      @"Wisconsin",
                      @"Wyoming", nil];

    
    
    detailsobject=[[NSMutableArray alloc]init];
    dicdetails=[[NSMutableDictionary alloc]init];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    self.txt_title.leftView = paddingView;
    self.txt_title.leftViewMode = UITextFieldViewModeAlways;
    self.detailbtn.selected=TRUE;
    [super viewDidLoad];
    [self.collectionview registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"Cell"];
    emails=[[NSMutableArray alloc]init];
    tempimages=[[NSMutableArray alloc]init];
    [self addemail_btn_click:self];
    
    albumImagedataarr=[[NSMutableArray alloc]init];
    orignalimages=[[NSMutableArray alloc]init];
    NSData *imagedata=[self compressImage:[UIImage imageNamed:@"pic_album"]];
    [albumImagedataarr insertObject:imagedata atIndex:0];
    self.tableview.tableHeaderView=self.headerview;
    punch=[[NSMutableArray alloc]init];
    temp=1;
    temp2=1;
    projectCreated=0;
    // [self addpunch_btn_click:self];
    
    // Do any additional setup after loading the view from its nib.
}

-(void)viewDidDisappear:(BOOL)animated
{
    //
    //
    
}
-(void)viewWillAppear:(BOOL)animated
{
    
    
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"FromGallery"] isEqualToString:@"1"]) {
        [[NSUserDefaults standardUserDefaults]setValue:@"0" forKey:@"FromGallery"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    else
    {
        if ([[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails]valueForKey:@"role"]isEqualToString:@"CU"]) {
            self.addemailbtn.hidden=TRUE;
            self.txt_title.enabled=FALSE;
        }
        else
        {
            self.addemailbtn.hidden=FALSE;
            self.txt_title.enabled=TRUE;
        }
        self.tableview.tableFooterView=self.footerview;
        
        
        
//        if ([[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails]valueForKey:@"role"]isEqualToString:@"CO"]) {
//            
//            self.addemailbtn.hidden=FALSE;
//
//        }
//        else
//        {
//             self.addemailbtn.hidden=TRUE;
//        }

        

        //Date 19/12 Time:3:05
        temp=0;
        self.punchbtn.hidden=TRUE;
        self.punchlabel.hidden=TRUE;
        self.punchline.hidden=TRUE;
        
        
        deletedimages=[[NSMutableArray alloc]init];
        editarray=nil;
        
        if ([[NSUserDefaults standardUserDefaults]valueForKey:@"EditArray"]!=nil)
        {
            editarray=nil;
            editarray=[[NSMutableArray alloc]init];
            editarray=[[NSUserDefaults standardUserDefaults]valueForKey:@"EditArray"];
            
            
        }
        //  [[NSUserDefaults standardUserDefaults]setObject:@"ThroughAlbum" forKey:@"ThroughAlbum"];
        if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"ThroughAlbum"]isEqualToString:@"ThroughAlbum"]) {
            self.albumview.frame=CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
            
            [self.view addSubview:self.albumview];
            [[NSUserDefaults standardUserDefaults]setValue:@"" forKey:@"ThroughAlbum"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
        
        if ([[NSUserDefaults standardUserDefaults]valueForKey:@"EditProject"]!=nil) {
            arr_selected=[[NSMutableArray alloc]init];
            self.deletebtn.hidden=FALSE;
            self.deletebtn.selected=TRUE;
            self.sharingbtn.hidden=FALSE;
            self.album1.selected=TRUE;
            //self.detailbtn.selected=FALSE;
            imageselect=1;
            //  self.editalbumbtnback.hidden=FALSE;
            
            editarray=[[NSMutableArray alloc]init];
            editarray=[[NSUserDefaults standardUserDefaults]valueForKey:@"EditProject"];
            [buttoncreate setTitle:@"UPDATE" forState:UIControlStateNormal];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"EditProject"];
            
            [[NSUserDefaults standardUserDefaults]synchronize];
            [[NSUserDefaults standardUserDefaults]setObject:editarray forKey:@"EditArray"];
            
            [[NSUserDefaults standardUserDefaults]synchronize];
            self.createprojectlabel.text=[[editarray valueForKey:KData] valueForKey:@"title"];
            self.createprojectlable.text=[[editarray valueForKey:KData] valueForKey:@"title"];
            buttoncreate.hidden=FALSE;
            self.txt_title.text=[[editarray valueForKey:KData] valueForKey:@"title"];
            //        NSIndexSet *sections = [[NSIndexSet alloc] initWithIndex:0];
            //        [self.projectlist reloadSections:sections withRowAnimation:UITableViewRowAnimationFade];
            temp=0;
            countemails=[[editarray valueForKey:@"email_data"] count];
            NSLog(@"%ld",(long)countemails);
            
            detailsobject=[[editarray valueForKey:@"email_data"] mutableCopy];
            NSLog(@"%@",detailsobject);
            [self.tableview reloadData];
            self.punchbtn.hidden=TRUE;
            self.punchlabel.hidden=TRUE;
            self.punchline.hidden=TRUE;
            
            
            
            
            
            
            for (int i=0; i<[[editarray valueForKey:@"image_data"] count]; i++) {
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                     NSUserDomainMask, YES);
                //
                // NSString *url = [NSString stringWithFormat:@"%@%@",ThumbImages,[[[editarray valueForKey:@"image_data"] objectAtIndex:i] valueForKey:@"image"]];
                NSString *filename =[NSString stringWithFormat:@"Project_%@",[[[editarray valueForKey:@"image_data"] objectAtIndex:i] valueForKey:@"image"]];
                NSString *documentsDirectory = [paths objectAtIndex:0];
                NSString* path = [documentsDirectory stringByAppendingPathComponent:filename];
                UIImage* image = [UIImage imageWithContentsOfFile:path];
                if (image==nil) {
                    
                    NSString *url =  [NSString stringWithFormat:@"%@%@",ThumbImages,[[[editarray valueForKey:@"image_data"] objectAtIndex:i] valueForKey:@"image"]];
                    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                        
                        NSString *str=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                        NSURL *imagepath = [NSURL URLWithString:str];
                        
                        NSData *data = [NSData dataWithContentsOfURL:imagepath];
                        // UIImage *image = [UIImage imageWithData:data];
                        [data writeToFile:path atomically:YES];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [albumImagedataarr addObject:data];
                            [orignalimages addObject:data];
                            
                        });
                    });
                    
                }
                else
                {
                    
                    [albumImagedataarr addObject:[NSData dataWithContentsOfFile:path]];
                    [orignalimages addObject:[NSData dataWithContentsOfFile:path]];
                }
                
            }
            
            
            
            
            temp2=(int)[albumImagedataarr count];
            [self.albumimagestable reloadData];
            
            
            
            
        }
        else
        {
            
            if ([[NSUserDefaults standardUserDefaults]valueForKey:@"FromProjectList"]!=nil)
            {
             
                
                editarray=nil;
                editarray=[[NSMutableArray alloc]init];
                NSLog(@"%@",[[[NSUserDefaults standardUserDefaults]valueForKey:@"FromProjectList"] valueForKey:@"email_data"]);
                NSMutableDictionary *custom=[[NSMutableDictionary alloc]init];
                [custom setObject:[[[NSUserDefaults standardUserDefaults]valueForKey:@"FromProjectList"] valueForKey:@"email_data"] forKey:@"email_data"];
                [editarray addObject:custom];
                countemails=[[editarray valueForKey:@"email_data"] count];
                detailsobject=[[editarray valueForKey:@"email_data"] mutableCopy];
               

                [self.tableview reloadData];
            
            }
            
            
            projectCreated=0;
            buttoncreate.hidden=FALSE;
            
        }
        

        if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"FromPunch"]isEqualToString:@"1"]) {
            punchlist=[[NSMutableArray alloc]init];
            punchlist=[[NSUserDefaults standardUserDefaults]valueForKey:@"PuchNameList"];
            [[NSUserDefaults standardUserDefaults]setValue:@"0" forKey:@"FromPunch"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            projectCreated=1;
            buttoncreate.hidden=TRUE;
            self.tableview.tableFooterView=nil;
            
            self.tableview.tableFooterView=self.newfooterview;
            [self.newfootertable reloadData];
        }
        if([[[NSUserDefaults standardUserDefaults]valueForKey:@"FirstTime"]isEqualToString:@"2"])
        {
            
            buttoncreate.hidden=TRUE;
        }
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)sharingbtn_click:(id)sender {
 
    [self.shringview removeFromSuperview];
    
    //    UIImage *image=[UIImage imageNamed:@"pic_signup"];
    //    FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];
    //    photo.image = image;
    //    photo.userGenerated = YES;
    //    FBSDKSharePhotoContent *content = [[FBSDKSharePhotoContent alloc] init];
    //    content.photos = @[photo];
    
    //    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
    
    SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    
    [controller setInitialText:@"www.buildaga.com | powered by Buildaga"];
//    [controller addURL:[NSURL URLWithString:@"www.buildaga.com"]];

    
    for (int i=0; i<[shringimage count]; i++) {
        [controller addImage:[UIImage imageWithData:[shringimage objectAtIndex:i]]];
    }
    [self presentViewController:controller animated:YES completion:Nil];
    
    
    [controller setCompletionHandler:^(SLComposeViewControllerResult result) {
        NSLog(@"start completion block");
        NSString *output;
        switch (result) {
            case SLComposeViewControllerResultCancelled:
                output = @"Action Cancelled";
                break;
            case SLComposeViewControllerResultDone:
            {
                [self.shringview removeFromSuperview];
                output = @"Post Successfull";
                break;
            }
            default:
                break;
        }
//        if (result != SLComposeViewControllerResultCancelled)
//        {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Punch List" message:output delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//            [alert show];
//        }
    }];
    // }
    
}

- (IBAction)detail_album_click:(UIButton *)sender {
    
    
    
    if (sender==self.album || sender==self.album1) {
        if (self.detailbtn.selected || self.detailbtn1.selected) {
            [self.detailbtn setSelected:FALSE];
            [self.detailbtn1 setSelected:FALSE];
            
            
        }
        if (sender.isSelected) {
            [self.album setSelected:TRUE];
            [self.album1 setSelected:TRUE];
            
            
        }
        else
        {
            [sender setSelected:TRUE];
            [self.album1 setSelected:TRUE];
            self.albumview.frame=CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
            
            [self.view addSubview:self.albumview];
            [self.view endEditing:YES];
        }
        
        
    }
    else
    {
        if (self.album.selected || self.album1.selected) {
            [self.album setSelected:FALSE];
            [self.album1 setSelected:FALSE];
            
            
            
        }
        if (sender.isSelected) {
            
            [self.detailbtn setSelected:TRUE];
            [self.detailbtn1 setSelected:TRUE];
        }
        else
        {
            [sender setSelected:TRUE];
            [self.detailbtn1 setSelected:TRUE];
            [self.detailbtn setSelected:TRUE];
            [self.albumview removeFromSuperview];
            
        }
       }
    
  }
- (IBAction)back_btn_cllick:(id)sender {
    
    if ([buttoncreate.titleLabel.text isEqualToString:@"CREATE"] &&  [[[NSUserDefaults standardUserDefaults]valueForKey:@"Remove"] intValue]==0) {
        
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"TempProject"];
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"EditProject"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
    }
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"FromGallery"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"FirstTime"];
    [[NSUserDefaults standardUserDefaults]setValue:@"FromEditingMode" forKey:@"FromEditingMode"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [self.navigationController popViewControllerAnimated:NO];
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==self.footertableview) {
        if(temp%3==0)
        {
            return temp /3;
        }
        else
        {
            return (temp/3)+1;
        }
        return temp;
    }
    if (tableView==self.albumimagestable) {
        if(temp2%3==0)
        {
            return temp2 /3;
        }
        else
        {
            return (temp2/3)+1;
        }
        return temp2;
    }
    if (tableView==self.newfootertable) {
        return   [punchlist count];
    }
    return countemails;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==self.footertableview) {
        
        static NSString *cellIdentifier = @"TableCell";
        
        PunchcellTableViewCell *cell = (PunchcellTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier ];
        if (cell == nil)
        {
            cell =[[PunchcellTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier ];
            
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if(temp>indexPath.row*3)
        {
            cell.punchview1.addPunch.titleLabel.numberOfLines  =0;
            cell.punchview1.addPunch.titleLabel.textAlignment  =NSTextAlignmentCenter;
            cell.punchview1.hidden=FALSE;
        }
        else
        {
            cell.punchview1.hidden=TRUE;
        }
        
        
        if(temp>(indexPath.row*3)+1)
        {
            
            cell.punchview2.addPunch.titleLabel.numberOfLines  =0;
            cell.punchview2.addPunch.titleLabel.textAlignment  =NSTextAlignmentCenter;
            
            cell.punchview2.hidden=FALSE;
        }
        else
        {
            cell.punchview2.hidden=TRUE;
        }
        
        
        if(temp>(indexPath.row*3)+2)
        {
            
            cell.punchview3.addPunch.titleLabel.numberOfLines  =0;
            cell.punchview3.addPunch.titleLabel.textAlignment  =NSTextAlignmentCenter;
            
            cell.punchview3.hidden=FALSE;
        }
        else
        {
            cell.punchview3.hidden=TRUE;
        }
        
        [ cell.punchview1.addPunch addTarget:self action:@selector(add_Punch:) forControlEvents:UIControlEventTouchUpInside];
        [ cell.punchview2.addPunch addTarget:self action:@selector(add_Punch:) forControlEvents:UIControlEventTouchUpInside];
        [ cell.punchview3.addPunch addTarget:self action:@selector(add_Punch:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.punchview1.addPunch.tag=indexPath.row*3;
        cell.punchview2.addPunch.tag=(indexPath.row*3)+1;
        cell.punchview3.addPunch.tag=(indexPath.row*3)+2;
        cell.backgroundColor=[UIColor clearColor];
        
        return cell;
        
        
    }
    else if (tableView==self.albumimagestable)
    {
        
        static NSString *cellIdentifier = @"AlbumTableCell";
        
        AlbumImageCell *cell = (AlbumImageCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier ];
        if (cell == nil)
        {
            cell =[[AlbumImageCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier ];
            
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if(temp2>indexPath.row*3)
        {
            if (imageselect==1) {
                cell.punchview1.albumbtn.hidden=FALSE;
                if([arr_selected containsObject:[NSString stringWithFormat:@"%ld", (long)indexPath.row*3]])
                {
                    cell.punchview1.albumbtn.selected=TRUE;
                    [shringimage addObject:[albumImagedataarr objectAtIndex:indexPath.row*3]];
                    
                }else
                {
                    
                    cell.punchview1.albumbtn.selected=FALSE;
                }
            }
            else
            {
                cell.punchview1.albumbtn.hidden=TRUE;
            }
            if (indexPath.row==0) {
                cell.punchview1.albumbtn.hidden=TRUE;
            }
            cell.punchview1.addimags.image=[UIImage imageWithData:[albumImagedataarr objectAtIndex:indexPath.row*3]];
            cell.punchview1.hidden=FALSE;
            
            UITapGestureRecognizer *gesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(btn_plus:)];
            
            [cell.punchview1.addimags addGestureRecognizer:gesture];
            
        }
        else
        {
            cell.punchview1.hidden=TRUE;
            
        }
        
        
        if(temp2>(indexPath.row*3)+1)
        {
            if (imageselect==1) {
                cell.punchview2.albumbtn.hidden=FALSE;
                if([arr_selected containsObject:[NSString stringWithFormat:@"%ld", (long)(indexPath.row*3)+1]])
                    
                {
                    cell.punchview2.albumbtn.selected=TRUE;
                    [shringimage addObject:[albumImagedataarr objectAtIndex:(indexPath.row*3)+1]];
                }else
                {
                    
                    cell.punchview2.albumbtn.selected=FALSE;
                }
            }
            else
            {
                cell.punchview2.albumbtn.hidden=TRUE;
            }
            
            UITapGestureRecognizer *gesture2=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(btn_plus:)];
            
            [cell.punchview2.addimags addGestureRecognizer:gesture2];
            cell.punchview2.addimags.image=[UIImage imageWithData:[albumImagedataarr objectAtIndex:(indexPath.row*3)+1]];
            
            cell.punchview2.hidden=FALSE;
        }
        else
        {
            cell.punchview2.hidden=TRUE;
            
        }
        if(temp2>(indexPath.row*3)+2)
        {
            
            if (imageselect==1) {
                cell.punchview3.albumbtn.hidden=FALSE;
                if([arr_selected containsObject:[NSString stringWithFormat:@"%ld", (long)(indexPath.row*3)+2]])
                {
                    [shringimage addObject:[albumImagedataarr objectAtIndex:(indexPath.row*3)+2]];
                    cell.punchview3.albumbtn.selected=TRUE;
                }else
                {
                    
                    cell.punchview3.albumbtn.selected=FALSE;
                }
            }
            else
            {
                cell.punchview3.albumbtn.hidden=TRUE;
            }
            UITapGestureRecognizer *gesture3=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(btn_plus:)];
            
            [cell.punchview3.addimags addGestureRecognizer:gesture3];
            
            cell.punchview3.addimags.image=[UIImage imageWithData:[albumImagedataarr objectAtIndex:(indexPath.row*3)+2]];
            cell.punchview3.hidden=FALSE;
        }
        else
        {
            cell.punchview3.hidden=TRUE;
        }
        
        cell.punchview1.addimags.tag=indexPath.row*3;
        
        cell.punchview2.addimags.tag=(indexPath.row*3)+1;
        cell.punchview3.addimags.tag=(indexPath.row*3)+2;
        
        cell.backgroundColor=[UIColor clearColor];
        
        return cell;
        
        
    }
    else if (tableView==self.newfootertable)
    {
        static NSString *cellIdentifier = @"ProjectDetailCell";
        
        ProjectDetailcell *cell = (ProjectDetailcell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier ];
        if (cell == nil)
        {
            cell =[[ProjectDetailcell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier ];
            
        }
        cell.labeltitle.text=[punchlist objectAtIndex:indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor=[UIColor clearColor];
        cell.punchstatus.hidden=TRUE;
        cell.imageround.layer.cornerRadius = cell.imageround.frame.size.width / 2;
        cell.imageround.clipsToBounds = YES;
        
        return cell;
        
        
    }
    else
    {
        static NSString *cellIdentifier = @"ContactDetailCell";
        
        ContactDetailCell  *cell = (ContactDetailCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier ];
        if (cell == nil)
        {
            cell =[[ContactDetailCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier ];
            
        }
        
        
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor=[UIColor clearColor];
        
        cell.firstname.tag=1000+indexPath.row;
        cell.phonenumber.tag=2000+indexPath.row;
        cell.email.tag=3000+indexPath.row;
        cell.address.tag=4000+indexPath.row;
        cell.lastname.tag=5000+indexPath.row;
        
         cell.city.tag=8000+indexPath.row;
         cell.state.tag=9000+indexPath.row;
         cell.zip.tag=10000+indexPath.row;
        
        
        cell.firstname.frame=CGRectMake(20, 5, self.view.frame.size.width-40, 30);
        
        cell.lastname.frame=CGRectMake(20,  cell.firstname.frame.origin.y+37, self.view.frame.size.width-40, 30);
        cell.phonenumber.frame=CGRectMake(20, cell.lastname.frame.origin.y+37, self.view.frame.size.width-40, 30);
        cell.email.frame=CGRectMake(20, cell.phonenumber.frame.origin.y+37, self.view.frame.size.width-40, 30);
        cell.address.frame=CGRectMake(20, cell.email.frame.origin.y+37, self.view.frame.size.width-40, 30);
        cell.city.frame=CGRectMake(20, cell.address.frame.origin.y+37, self.view.frame.size.width-40, 30);
        cell.state.frame=CGRectMake(20, cell.city.frame.origin.y+37, self.view.frame.size.width-40, 30);
        cell.zip.frame=CGRectMake(20, cell.state.frame.origin.y+37, self.view.frame.size.width-40, 30);
        
        
        UIView *paddingView4 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
        cell.lastname.leftView = paddingView4;
        cell.lastname.leftViewMode = UITextFieldViewModeAlways;
        
        
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
        cell.firstname.leftView = paddingView;
        cell.firstname.leftViewMode = UITextFieldViewModeAlways;
        
        
        UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
        cell.email.leftView = paddingView1;
        cell.email.leftViewMode = UITextFieldViewModeAlways;
        
        
        UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
        cell.phonenumber.leftView = paddingView2;
        cell.phonenumber.leftViewMode = UITextFieldViewModeAlways;
        
        
        UIView *paddingView3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
        cell.address.leftView = paddingView3;
        cell.address.leftViewMode = UITextFieldViewModeAlways;
        
        
        
        UIView *paddingView7 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
        cell.city.leftView = paddingView7;
        cell.city.leftViewMode = UITextFieldViewModeAlways;

        
        
        UIView *paddingView8 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
        cell.state.leftView = paddingView8;
        cell.state.leftViewMode = UITextFieldViewModeAlways;

        
        
        UIView *paddingView9 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
        cell.zip.leftView = paddingView9;
        cell.zip.leftViewMode = UITextFieldViewModeAlways;

        
        cell.firstname.delegate=self;
        cell.lastname.delegate=self;
        cell.phonenumber.delegate=self;
        cell.email.delegate=self;
        cell.address.delegate=self;
        cell.city.delegate=self;
        cell.state.delegate=self;
        cell.zip.delegate=self;
        
        
        
        
        NSLog(@"Cell tag value%ld",(long)cell.tag);
        
        NSLog(@"Cell tag new value%ld",(long)cell.state.tag);
        
        
        if (cell.state) {
            State_List = [[UIPickerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-200, self.view.frame.size.width, 200)];
            State_List.showsSelectionIndicator = YES;
            State_List.delegate = self;
            State_List.backgroundColor = [UIColor colorWithRed:206.0f/255.0f green:210.0f/255.0f blue:215.0f/255.0f alpha:1.0];
            State_List.tintColor = [UIColor blackColor];
            [cell.state setInputView:State_List];
            
            UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
            [toolBar setTintColor:[UIColor grayColor]];
            
            UIBarButtonItem *Cancel=[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(ShowSelectedCancel)];
            
            UIBarButtonItem *doneBtn=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(ShowSelectedDate:)];
            doneBtn.tag=indexPath.row;
            
            UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
            [toolBar setItems:[NSArray arrayWithObjects:Cancel,space,doneBtn, nil]];
            [cell.state setInputAccessoryView:toolBar];
            
        }
        else
        {
            [State_List removeFromSuperview];
            
        }

        
        
        
        // user_id=1&title=Bathroom Project new&user_details=[{"name": "abc","email": "abc@gmail.com","address":"shahibaug ahmedabad","phoneno":"1234567891"},{"name": "efg","email": "efg@gmail.com","address":"maninagar ahmedabad","phoneno":"1234567891"},{"name": "xyz","email": "xyz@gmail.com","address":"shahibaug ahmedabad","phoneno":"1234567891"}]
        
        
        //        UITextField *emailstext=[[UITextField alloc]initWithFrame:CGRectMake(15, 5, [[UIScreen mainScreen] bounds].size.width-30, 30)];
        //        emailstext.tag=1000+indexPath.row;
        //        emailstext.autocapitalizationType=UITextAutocapitalizationTypeNone;
        //        emailstext.backgroundColor=[UIColor colorWithRed:205 green:205 blue:205 alpha:1.0];
        //
        //        emailstext.backgroundColor=[UIColor colorWithRed:205.0/255.0 green:205.0/255.0 blue:205.0/255.0 alpha:1.0];
        //
        //        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
        //        emailstext.leftView = paddingView;
        //        emailstext.leftViewMode = UITextFieldViewModeAlways;
        //
        //
        //
        //
        //
        //        [cell.contentView addSubview:emailstext];
        
        //dicdetails=nil;
        dicdetails=[[NSMutableDictionary alloc]init];
        if([[editarray valueForKey:@"email_data"] count]>indexPath.row)
        {
            cell.firstname.text=[[[editarray valueForKey:@"email_data"] valueForKey:@"name"] objectAtIndex:indexPath.row];
            cell.phonenumber.text=[[[editarray valueForKey:@"email_data"] valueForKey:@"phoneno"] objectAtIndex:indexPath.row];
            cell.lastname.text=[[[editarray valueForKey:@"email_data"] valueForKey:@"lastname"] objectAtIndex:indexPath.row];
            cell.email.text=[[[editarray valueForKey:@"email_data"] valueForKey:@"email"] objectAtIndex:indexPath.row];
            cell.address.text=[[[editarray valueForKey:@"email_data"] valueForKey:@"address"] objectAtIndex:indexPath.row];
            
              cell.city.text=[[[editarray valueForKey:@"email_data"] valueForKey:@"city"] objectAtIndex:indexPath.row];
            
              cell.state.text=[[[editarray valueForKey:@"email_data"] valueForKey:@"state"] objectAtIndex:indexPath.row];
            
              cell.zip.text=[[[editarray valueForKey:@"email_data"] valueForKey:@"zip"] objectAtIndex:indexPath.row];
            
            cell.firstname.enabled=FALSE;
            cell.lastname.enabled=FALSE;
            cell.phonenumber.enabled=FALSE;
            cell.email.enabled=FALSE;
            cell.address.enabled=FALSE;
             cell.city.enabled=FALSE;
             cell.state.enabled=FALSE;
             cell.zip.enabled=FALSE;
            
        }
        else
        {
            
            cell.firstname.text=[[detailsobject valueForKey:@"name"] objectAtIndex:indexPath.row];
            cell.phonenumber.text=[[detailsobject valueForKey:@"phoneno"] objectAtIndex:indexPath.row];
            cell.email.text=[[detailsobject valueForKey:@"email"] objectAtIndex:indexPath.row];
            cell.address.text=[[detailsobject valueForKey:@"address"] objectAtIndex:indexPath.row];
            
              cell.city.text=[[detailsobject valueForKey:@"city"] objectAtIndex:indexPath.row];
              cell.state.text=[[detailsobject valueForKey:@"state"] objectAtIndex:indexPath.row];
              cell.zip.text=[[detailsobject valueForKey:@"zip"] objectAtIndex:indexPath.row];
            
            
            cell.firstname.enabled=TRUE;
            cell.phonenumber.enabled=TRUE;
            cell.email.enabled=TRUE;
            cell.address.enabled=TRUE;
            cell.city.enabled=TRUE;
            cell.state.enabled=TRUE;
            cell.zip.enabled=TRUE;
            
        }
        
        
        //        if (indexPath.row==countemails) {
        //            for (UIView *view in cell.contentView.subviews) {
        //                [view removeFromSuperview];
        //            }
        //            if (temp%3==0) {
        //                self.footerview.frame=CGRectMake(0,0,[[UIScreen mainScreen]bounds].size.width,(154+((temp/3)*100)));
        //            }
        //            else
        //            {
        //                self.footerview.frame=CGRectMake(0,0,[[UIScreen mainScreen]bounds].size.width,(154+(((temp/3)+1)*100)));
        //
        //            }
        //
        //            [cell.contentView addSubview:self.footerview];
        //
        //
        //        }
        
        return cell;
    }
}



- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField.tag==7000)
    {
         NSLog(@"Tag Delegate textFieldShouldBeginEditing :---> %ld",(long)textField.tag);
    }
    
    
    
     return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    
    
    
   //  NSLog(@"Tag Delegate textFieldShouldEndEditing :---> %ld",(long)textField.tag);
    
    
    if(textField.tag==7000)
    {
        if (textField.text.length==1) {
            textField.text = [[[textField.text substringToIndex:1] uppercaseString] stringByAppendingString:[textField.text substringFromIndex:1]];
        }
        
        
    }
    if (textField.tag>=1000 && textField.tag<=1999) {
        
        if (textField.text.length==1) {
            textField.text = [[[textField.text substringToIndex:1] uppercaseString] stringByAppendingString:[textField.text substringFromIndex:1]];
        }
        [[detailsobject objectAtIndex:textField.tag%1000] setValue:textField.text forKey:@"name"];
    }
    else if (textField.tag>=2000 && textField.tag<=2999)
    {
        
        [[detailsobject objectAtIndex:textField.tag%2000] setValue:textField.text forKey:@"phoneno"];
        
    }
    else if (textField.tag>=3000 && textField.tag<=3999)
    {
        [[detailsobject objectAtIndex:textField.tag%3000] setValue:textField.text forKey:@"email"];
        
        
    }
    else if(textField.tag>=4000 && textField.tag<=4999)
    {
        
        if (textField.text.length==1) {
            textField.text = [[[textField.text substringToIndex:1] uppercaseString] stringByAppendingString:[textField.text substringFromIndex:1]];
        }
        [[detailsobject objectAtIndex:textField.tag%4000] setValue:textField.text  forKey:@"address"];
        
    }
    else if(textField.tag>=5000 && textField.tag<=5999)
    {
        if (textField.text.length==1) {
            textField.text = [[[textField.text substringToIndex:1] uppercaseString] stringByAppendingString:[textField.text substringFromIndex:1]];
        }
        [[detailsobject objectAtIndex:textField.tag%5000] setValue:textField.text  forKey:@"lastname"];
        
    }
    
    
    NSLog(@"%ld",(long)textField.tag);
    
    return YES;
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if(textField.tag==7000)
    {
        if (textField.text.length==1) {
            textField.text = [[[textField.text substringToIndex:1] uppercaseString] stringByAppendingString:[textField.text substringFromIndex:1]];
        }
        
    }
    if (textField.tag>=1000 && textField.tag<=1999) {
        
        if (textField.text.length==1) {
            textField.text = [[[textField.text substringToIndex:1] uppercaseString] stringByAppendingString:[textField.text substringFromIndex:1]];
        }
        [[detailsobject objectAtIndex:textField.tag%1000] setValue:textField.text forKey:@"name"];
    }
    else if (textField.tag>=2000 && textField.tag<=2999)
    {
        
        [[detailsobject objectAtIndex:textField.tag%2000] setValue:textField.text forKey:@"phoneno"];
        
    }
    else if (textField.tag>=3000 && textField.tag<=3999)
    {
        [[detailsobject objectAtIndex:textField.tag%3000] setValue:textField.text forKey:@"email"];
        
        
    }
    else if(textField.tag>=4000 && textField.tag<=4999)
    {
        
        if (textField.text.length==1) {
            textField.text = [[[textField.text substringToIndex:1] uppercaseString] stringByAppendingString:[textField.text substringFromIndex:1]];
        }
        [[detailsobject objectAtIndex:textField.tag%4000] setValue:textField.text  forKey:@"address"];
        
    }
    
    else if(textField.tag>=8000 && textField.tag<=8999)
    {
        
        if (textField.text.length==1) {
            textField.text = [[[textField.text substringToIndex:1] uppercaseString] stringByAppendingString:[textField.text substringFromIndex:1]];
        }
        [[detailsobject objectAtIndex:textField.tag%8000] setValue:textField.text  forKey:@"city"];
        
    }

    
    
    else if(textField.tag>=9000 && textField.tag<=9999)
    {
        
        if (textField.text.length==1) {
            textField.text = [[[textField.text substringToIndex:1] uppercaseString] stringByAppendingString:[textField.text substringFromIndex:1]];
        }
        [[detailsobject objectAtIndex:textField.tag%9000] setValue:textField.text  forKey:@"state"];
        
    }

    
    
    else if(textField.tag>=10000 && textField.tag<=10999)
    {
        
        if (textField.text.length==1) {
            textField.text = [[[textField.text substringToIndex:1] uppercaseString] stringByAppendingString:[textField.text substringFromIndex:1]];
        }
        [[detailsobject objectAtIndex:textField.tag%10000] setValue:textField.text  forKey:@"zip"];
        
    }

    
    
    else if(textField.tag>=5000 && textField.tag<=5999)
    {
        if (textField.text.length==1) {
            textField.text = [[[textField.text substringToIndex:1] uppercaseString] stringByAppendingString:[textField.text substringFromIndex:1]];
        }
        [[detailsobject objectAtIndex:textField.tag%5000] setValue:textField.text  forKey:@"lastname"];
        
    }
    
    
    return YES;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if (tableView==self.footertableview) {
        
        return 100.0;
    }
    if (tableView==self.albumimagestable) {
        return 120.0;
    }
    else if (tableView==self.newfootertable)
    {
        return 36.0;
        
        
    }
    else
    {
        
        //        if (indexPath.row==countemails) {
        //            if (temp%3==0) {
        //                NSLog(@"%d",(154+((temp/3)*100)));
        //                return (154+((temp/3)*100));
        //
        //            }
        //            else
        //            {
        //                NSLog(@"%d",(154+(((temp/3)+1)*100)));
        //
        //                return (154+(((temp/3)+1)*100));
        //
        //
        //            }
        //        }
        
    }
    return 315;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return  countpunch;
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    
    
    cell.backgroundColor=[UIColor clearColor];
    UIButton *button=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 100, 100)];
    button.titleLabel.textAlignment=NSTextAlignmentCenter;
    
    [button setTitle:@"Add       Punch" forState:UIControlStateNormal];
    [button setFont:[UIFont fontWithName:@"OpenSans" size:17.0]];
    
    [button setImage:[UIImage imageNamed:@"add_punch"] forState:UIControlStateNormal];
    button.tag=1000+indexPath.row;
    [cell.contentView addSubview:button];
    cell.backgroundColor=[UIColor clearColor];
    return cell;
    
}
-(void)add_Punch:(UIButton *)puchbtn
{
    
    if (projectCreated==1) {
        CreatePunch *punchview=[[CreatePunch alloc]init];
        [self.navigationController pushViewController:punchview animated:YES];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:@"Please create project" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        
    }
    
    
}
- (IBAction)addemail_btn_click:(id)sender {
    
    // countemails=[emails count]+1;
    if ([emails count]==0) {
        countemails++;
    }
    //    NSIndexPath *myIP = [NSIndexPath indexPathForRow:countemails inSection:0];
    //    NSArray *myArray = [[NSArray alloc] initWithObjects:myIP, nil];
    //
    //    [self.tableview reloadRowsAtIndexPaths:myArray withRowAnimation:UITableViewRowAnimationNone];
    //    [self.tableview endUpdates];
    
    [dicdetails setValue:@"" forKey:@"name"];
    [dicdetails setValue:@"" forKey:@"lastname"];
    [dicdetails setValue:@"" forKey:@"phoneno"];
    
    [dicdetails setValue:@"" forKey:@"email"];
    [dicdetails setValue:@"" forKey:@"address"];
    
     [dicdetails setValue:@"" forKey:@"city"];
     [dicdetails setValue:@"" forKey:@"state"];
     [dicdetails setValue:@"" forKey:@"zip"];
    
    
    
    [detailsobject addObject:dicdetails];
    [self.tableview beginUpdates];
    NSIndexPath *myIP = [NSIndexPath indexPathForRow:countemails-1 inSection:0];
    NSArray *myArray = [[NSArray alloc] initWithObjects:myIP, nil];
    [self.tableview insertRowsAtIndexPaths:myArray withRowAnimation:UITableViewRowAnimationBottom];
    
    //[self.mytableview deleteRowsAtIndexPaths:path withRowAnimation:UITableViewRowAnimationBottom];
    [self.tableview endUpdates];
    //[self.tableview reloadData];
}
- (IBAction)addpunch_btn_click:(id)sender {
    
    if (projectCreated==1) {
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:@"Please create project" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
        
    }
    
    CreatePunch *punch=[[CreatePunch alloc]init];
    [self.navigationController pushViewController:punch animated:YES];
    
    //    temp++;
    //    [self.footertableview reloadData];
    //
    //    [self.tableview beginUpdates];
    //
    //
    //    NSIndexPath *myIP = [NSIndexPath indexPathForRow:countemails inSection:0];
    //    NSArray *myArray = [[NSArray alloc] initWithObjects:myIP, nil];
    //
    //    [self.tableview reloadRowsAtIndexPaths:myArray withRowAnimation:UITableViewRowAnimationNone];
    //    [self.tableview endUpdates];
    buttoncreate.hidden=TRUE;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (tableView==self.footertableview) {
        UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 50, [[UIScreen mainScreen] bounds].size.width, 36)];
        view.backgroundColor=[UIColor clearColor];
        
        buttoncreate=[UIButton buttonWithType:UIButtonTypeCustom];
        buttoncreate.frame=CGRectMake([[UIScreen mainScreen] bounds].size.width/2-64, 0,128 , 36);
        //        UIButton *button=[[UIButton alloc]initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width/2-64, 0,128 , 36)];
        //
        if (editarray==nil || [[NSUserDefaults standardUserDefaults]valueForKey:@"FromProjectList"]!=nil) {
            if ([[NSUserDefaults standardUserDefaults]valueForKey:@"FromProjectList"]!=nil) {
                [[NSUserDefaults standardUserDefaults]setValue:@"1" forKey:@"Remove"];
                [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"FromProjectList"];
                [[NSUserDefaults standardUserDefaults]synchronize];
            }
            else
            {
             [[NSUserDefaults standardUserDefaults]setValue:@"0" forKey:@"Remove"];
                [[NSUserDefaults standardUserDefaults]synchronize];

            }
            
            [buttoncreate setTitle:@"CREATE" forState:UIControlStateNormal];
        }
        else
        {
            
            [buttoncreate setTitle:@"UPDATE" forState:UIControlStateNormal];
        }
        
        //button.titleLabel.textColor=[UIColor blackColor];
        [buttoncreate setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [buttoncreate setBackgroundImage:[UIImage imageNamed:@"button_create"] forState:UIControlStateNormal];
        [buttoncreate addTarget:self action:@selector(createProject:) forControlEvents:UIControlEventTouchUpInside];
        buttoncreate.tag=10011;
        
        // [button addTarget:self action:@selector(viewAll:) forControlEvents:UIControlEventTouchUpInside];
        [buttoncreate setFont:[UIFont fontWithName:@"OpenSans-Bold" size:15.0]];
        
        
       //  if ([[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails]valueForKey:@"role"]isEqualToString:@"CO"]) {
        
        [view addSubview:buttoncreate];
       //  }
        
        return view;
        
    }
    return nil;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    
    if (tableView==self.footertableview) {
        return 100;
    }
    return 0.0;
    
}

- (IBAction)back_ablum:(id)sender {
    
}

-(void)createProject:(UIButton *)btn
{
    
    
    if ([self.txt_title.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0 ){
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:@"Please enter title" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
        
        
    }
    int totalblank;
    totalblank=0;
    
    int allblank,mail,fnm,addres,phn,lnm;
    
    NSMutableArray *emailsaddress=[[NSMutableArray alloc]init];
    
    
    for (int i=0; i<countemails; i++) {
        allblank=0;
        mail=fnm=lnm=addres=phn=0;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        ContactDetailCell *cell=(ContactDetailCell *)[self.tableview cellForRowAtIndexPath:indexPath];
        
        if ([cell.firstname.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0)
        {
            allblank++;
            fnm=1;
            
        }
        if ([cell.lastname.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0)
        {
            allblank++;
            lnm=1;
            
        }
        if ([cell.phonenumber.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0)
        {
            
            allblank++;
            phn=1;
        }
        if ([cell.email.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0)
        {
            allblank++;
            mail=1;
            
        }
        else if (![self validateEmail:cell.email.text])
        {
            
            
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:@"Please enter a valid email address" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            return;
            
            
            
            
        }
        
        if ([cell.address.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0)
        {
            allblank++;
            addres=1;
            
            
        }
        
        
        if (allblank==5)
        {
            totalblank++;
        }
        else if(fnm==1)
        {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:@"Please enter first name" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            return;
            
        }
        else if(lnm==1)
        {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:@"Please enter last name" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            return;
            
        }
        else if(phn==1)
        {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:@"Please enter phone number" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            return;
            
        }
        else if(mail==1)
        {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:@"Please enter email" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            return;
            
        }
        else if(addres==1)
        {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:@"Please enter address" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            return;
            
        }
        else
        {
            
            NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
            [dic setValue:cell.firstname.text forKey:@"name"];
            [dic setValue:cell.lastname.text forKey:@"lastname"];
            
            [dic setValue:cell.phonenumber.text forKey:@"phoneno"];
            [dic setValue:cell.email.text forKey:@"email"];
            [dic setValue:cell.address.text forKey:@"address"];
            
             [dic setValue:cell.city.text forKey:@"city"];
             [dic setValue:string_IOS forKey:@"state"];
             [dic setValue:cell.zip.text forKey:@"zip"];
            
            
            
            
            
//            if ([emailsaddress containsObject:dic] || [[editarray valueForKey:@"email_data"]containsObject:dic]) {
//                
//            }
//            else
//            {
                [emailsaddress addObject:dic];
//            }
        }
        
        
        
        
        
        
    }
    if (totalblank==countemails) {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:@"Please enter customer details" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    
    //    for (int i=0; i<countemails; i++)
    //    {
    //
    //
    //        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
    //        UITableViewCell *cell=(UITableViewCell *)[self.tableview cellForRowAtIndexPath:indexPath];
    //
    //
    //        for (UIView *view in cell.contentView.subviews)
    //        {
    //
    //            if([view isKindOfClass:[UITextField class]])
    //            {
    //                UITextField *textfield=(UITextField *)view;
    //                if ([textfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0)
    //                {
    //                    totalblank++;
    //
    //                }
    //                else
    //                {
    //                    if (![self validateEmail:textfield.text]){
    //
    //                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Punch List" message:@"Please enter valid email" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    //                        [alert show];
    //                        return;
    //                    }
    //                    else
    //                    {
    //
    //                        if (![[editarray valueForKey:@"email_data"]containsObject:textfield.text])
    //                        {
    //                            [emailsaddress addObject:textfield.text];
    //                        }
    //
    //                    }
    //
    //                }
    //            }
    //
    //        }
    //
    //
    //
    //
    //
    //    }
    
    ///http://hyperlinkserver.com/punchlist/ws/create_project.php?
    
    
    if ([buttoncreate.titleLabel.text isEqualToString:@"UPDATE"]) {
        
        NSURL *url = [NSURL URLWithString:kEditProject];
        //project_id=1&title=Kitchen project&email=ruchi@gmail.com
        [APPDELEGATE addLoader:nil];
        ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
        //  user_id=1&title=Bathroom Project&email=sonali1@hyperlinkinfosystem.com,ruchi@gmail.com
        //image
        [request addPostValue:[[editarray valueForKey:KData] valueForKey:@"id"]  forKey:@"project_id"];
        
        SBJsonWriter1 *writer=[[SBJsonWriter1 alloc]init];
        NSString *responseArray = [writer stringWithObject:emailsaddress];
        
        [request addPostValue:[self.txt_title.text capitalizedString]  forKey:@"title"];
        [request addPostValue:responseArray  forKey:@"user_details"];
        //        [request setData:[orignalimages objectAtIndex:0] withFileName:@"png" andContentType:@"image/png" forKey:@"image"];
        
        
        //        for (int i =(int)[[editarray valueForKey:@"image_data"] count]; i < [orignalimages count]; i++) {
        //
        //            [request setData:[orignalimages objectAtIndex:i]  withFileName:[NSString stringWithFormat:@"image_%d.png",i] andContentType:@"image/png" forKey:[NSString stringWithFormat:@"image[%d]",i]];
        //
        //        }
        [request setDelegate:self];
        [request startAsynchronous];
    }
    else
    {
        
        NSURL *url = [NSURL URLWithString:kCreateProject];
        //fb_gmail_id=1234&name=riya&email=riya@gmail.com&login_type=F&device_token=Afdrfdrvrf&device_type=A
        [APPDELEGATE addLoader:nil];
        ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
        //  user_id=1&title=Bathroom Project&email=sonali1@hyperlinkinfosystem.com,ruchi@gmail.com
        //image
        [request addPostValue:[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails] valueForKey:@"id"]  forKey:@"user_id"];
        //  NSString *joinedComponents = [emailsaddress componentsJoinedByString:@","];
        [request addPostValue:[self.txt_title.text capitalizedString]  forKey:@"title"];
        
        SBJsonWriter1 *writer=[[SBJsonWriter1 alloc]init];
        NSString *responseArray = [writer stringWithObject:emailsaddress];
        [request addPostValue:responseArray  forKey:@"user_details"];
        //        [request setData:[orignalimages objectAtIndex:0] withFileName:@"png" andContentType:@"image/png" forKey:@"image"];
        for (int i = 0; i < [orignalimages count]; i++) {
            
            [request setData:[orignalimages objectAtIndex:i]  withFileName:[NSString stringWithFormat:@"image_%d.png",i] andContentType:@"image/png" forKey:[NSString stringWithFormat:@"image[%d]",i]];
            
        }
        [[NSUserDefaults standardUserDefaults]setValue:@"1" forKey:@"FirstTime"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [request setDelegate:self];
        [request startAsynchronous];
        
    }
    
    
    
    
}
- (void)requestFinished:(ASIHTTPRequest *)request
{
    
    
    [APPDELEGATE removeLoader];
    NSString *responseStr = [request responseString];
    SBJsonParser1 *parseData = [[SBJsonParser1 alloc] init];
    NSArray *responseArray = [parseData objectWithString:responseStr];
    
    
    if ([[responseArray valueForKey:KSucess] isEqualToString:@"1"]) {

        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:[responseArray valueForKey:@"msg"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        
        
        buttoncreate.hidden=TRUE;
        projectCreated=1;
        if ( [[[NSUserDefaults standardUserDefaults]valueForKey:@"FirstTime"] isEqualToString:@"1"]) {
            [[NSUserDefaults standardUserDefaults]setValue:@"2" forKey:@"FirstTime"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
        
        
        [tempimages removeAllObjects];
        [[NSUserDefaults standardUserDefaults]setObject:[responseArray valueForKey:KData] forKey:@"SelectedProject"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [[NSUserDefaults standardUserDefaults]setObject:responseArray forKey:@"TempProject"];
//        [[NSUserDefaults standardUserDefaults]setObject:responseArray forKey:@"EditProject"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        if (editarray!=nil) {
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"EditArray"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [self back_btn_cllick:nil];
            [self.navigationController popViewControllerAnimated:YES];
        }
        [self back_btn_cllick:nil];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:[responseArray valueForKey:@"msg"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];

    }
    
    
}
- (void)requestFailed:(ASIHTTPRequest *)request
{
    [APPDELEGATE removeLoader];
    NSLog(@"%@",request);
    
    
    
    //    [appDelegate.loading removeFromSuperview];
    //    [appDelegate.alertPopUp.popUpDescription setText:@"ERROR"];
    //    [self.view addSubview:appDelegate.alertPopUp];
}


- (BOOL)validateEmail:(NSString *)emailStr
{
    NSString *emailRegex = @"^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,4})$";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailStr];
}

- (IBAction)shring_btn_click:(id)sender {
    
    if ([shringimage count]==0) {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Buildaga" message:@"Please select image" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    self.shringview.frame=CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
    
    [self.view addSubview:self.shringview];
    
}

-(void)btn_plus:(UITapGestureRecognizer *)sender
{
    //  [albumImagedataarr removeAllObjects];
    
    NSLog(@"%ld",(long)sender.view.tag);
    
    if (sender.view.tag==0) {
        UzysAssetsPickerController *imgPicker = [[UzysAssetsPickerController alloc] init];
        [imgPicker setEditing:YES animated:YES];
        imgPicker.delegate = self;
        
        imgPicker.maximumNumberOfSelectionVideo = 0;
        imgPicker.maximumNumberOfSelectionPhoto = 15;
        [self presentViewController:imgPicker animated:YES completion:^{  }];
        
        
        //      [self.albumimagestable reloadData];
        //        self.sharingbtn.hidden=TRUE;
        //        self.deletebtn.hidden=TRUE;
    }
    else
    {
        if ([[editarray valueForKey:@"image_data"] count]<(long)sender.view.tag) {
            return;
        }
        if ([arr_selected containsObject:[NSString stringWithFormat:@"%ld", (long)sender.view.tag] ])
        {
            [arr_selected removeObject:[NSString stringWithFormat:@"%ld", (long)sender.view.tag]];
            
            NSLog(@"%ld",(sender.view.tag /3));
            shringimage=nil;
            shringimage=[[NSMutableArray alloc]init];
            [self.albumimagestable reloadData];
            // [self.albumimagestable beginUpdates];
            
            
            //        NSIndexPath *myIP = [NSIndexPath indexPathForRow:view.tag/3 inSection:0];
            //        NSArray *myArray = [[NSArray alloc] initWithObjects:myIP, nil];
            //
            //        [self.albumimagestable reloadRowsAtIndexPaths:myArray withRowAnimation:UITableViewRowAnimationNone];
            //        [self.albumimagestable endUpdates];
            
            
        }
        else
        {
            
            [arr_selected addObject:[NSString stringWithFormat:@"%ld", (long)sender.view.tag]];
            shringimage=nil;
            shringimage=[[NSMutableArray alloc]init];
            
            [self.albumimagestable reloadData];
            
            //[self.albumimagestable beginUpdates];
            
            
            //        NSIndexPath *myIP = [NSIndexPath indexPathForRow:view.tag/3 inSection:0];
            //        NSArray *myArray = [[NSArray alloc] initWithObjects:myIP, nil];
            //
            //        [self.albumimagestable reloadRowsAtIndexPaths:myArray withRowAnimation:UITableViewRowAnimationNone];
            //        [self.albumimagestable endUpdates];
        }
        
        
    }
    
    
}
-(void)scrollImages:(NSInteger )indexofImage
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    //
    //  NSString *url = [NSString stringWithFormat:@"%@%@",ProjectImages,[[orignalimages objectAtIndex:indexofImage] valueForKey:@"image"]];
    NSString *filename =[NSString stringWithFormat:@"Project_%@",[[orignalimages objectAtIndex:indexofImage] valueForKey:@"image"]];
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:filename];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    if (image==nil) {
        
        NSString *url =  [NSString stringWithFormat:@"%@%@",ProjectImages,[[orignalimages objectAtIndex:indexofImage] valueForKey:@"image"]];
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            
            NSString *str=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *imagepath = [NSURL URLWithString:str];
            
            NSData *data = [NSData dataWithContentsOfURL:imagepath];
            // UIImage *image = [UIImage imageWithData:data];
            [data writeToFile:path atomically:YES];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [orignalimages replaceObjectAtIndex:indexofImage withObject:data];
                
                
            });
        });
        
    }
    
    
    
    
    for (int i = 0; i < [orignalimages count]; i++)
    {
        //We'll create an imageView object in every 'page' of our scrollView.
        
        CGRect frame;
        frame.origin.x = ([[UIScreen mainScreen] bounds].size.width) * i;
        frame.origin.y = 0;
        frame.size = [[UIScreen mainScreen] bounds].size;
        frame.size.width=[[UIScreen mainScreen] bounds].size.width;
        UIView *view = [[UIView alloc] initWithFrame:frame];
        // UIImageView *imgview = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, self.BigImageScroll.frame.size.height)];
        
        [imgview removeFromSuperview];
        
        
    imgview = [[UIImageView alloc] initWithImage:[UIImage imageWithData:[orignalimages objectAtIndex:i]]];
        imgview.backgroundColor=[UIColor blackColor];
        imgview.frame = CGRectMake(0, 0, frame.size.width , self.imageScrollView.frame.size.height);
        
        
        NSLog(@"%f",imgview.image.size.width);
        NSLog(@"%f",imgview.image.size.height);
        //UIImage *Newimage = [self.arrImages objectAtIndex:i];
        
        //UIImage *newsizeimage = [self aspectScaledImageSizeForImageView:imgview image:Newimage];
        
        imgview.contentMode = UIViewContentModeScaleToFill;
        
        //imgview.clipsToBounds = YES;
        //imgview.image = newsizeimage;
        
        //imgview.frame = CGRectMake(0, 0, imgview.image.size.width, imgview.image.size.height);
        [view addSubview:imgview];
        
        [self.imageScrollView addSubview:view];
        
        self.imageScrollView.contentSize = CGSizeMake(([[UIScreen mainScreen] bounds].size.width) * [orignalimages count],self.imageScrollView.frame.size.height);
    }
    
    
    
    [self.imageScrollView setContentOffset:CGPointMake([[UIScreen mainScreen] bounds].size.width * indexofImage, 0.0f) animated:NO];
    
    
}
-(void)btn_selectimage:(UITapGestureRecognizer *)sender
{
    UIView *view = sender.view;
    
    NSLog(@"%ld", (long)view.tag);
    if (imageselect==0) {
        self.imageslideview.frame=CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
        [self.view addSubview:self.imageslideview];
        [self scrollImages:view.tag];
    }
    else
    {
        if ([arr_selected containsObject:[NSString stringWithFormat:@"%ld", (long)view.tag] ])
        {
            [arr_selected removeObject:[NSString stringWithFormat:@"%ld", (long)view.tag]];
            
            NSLog(@"%ld",(view.tag /3));
            
            [self.albumimagestable reloadData];
            // [self.albumimagestable beginUpdates];
            
            
            //        NSIndexPath *myIP = [NSIndexPath indexPathForRow:view.tag/3 inSection:0];
            //        NSArray *myArray = [[NSArray alloc] initWithObjects:myIP, nil];
            //
            //        [self.albumimagestable reloadRowsAtIndexPaths:myArray withRowAnimation:UITableViewRowAnimationNone];
            //        [self.albumimagestable endUpdates];
            
            
        }
        else
        {
            [arr_selected addObject:[NSString stringWithFormat:@"%ld", (long)view.tag]];
            
            [self.albumimagestable reloadData];
            
            //[self.albumimagestable beginUpdates];
            
            
            //        NSIndexPath *myIP = [NSIndexPath indexPathForRow:view.tag/3 inSection:0];
            //        NSArray *myArray = [[NSArray alloc] initWithObjects:myIP, nil];
            //
            //        [self.albumimagestable reloadRowsAtIndexPaths:myArray withRowAnimation:UITableViewRowAnimationNone];
            //        [self.albumimagestable endUpdates];
        }
    }
    // NSLog(@"%ld",(long)[sender tag]);
    
}
-(IBAction)emailContent:(id)sender
{
    
    MFMailComposeViewController *emailShareController = [[MFMailComposeViewController alloc] init];
    emailShareController.mailComposeDelegate = self;
//    [emailShareController setSubject:@"Punch Sharing"];
    
    NSString *strCompanyName;
    NSString *strWebsiteURL;
    if ([[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails]valueForKey:@"role"]isEqualToString:@"CU"])
    {
        NSDictionary *selectedClient = [[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedClient"];
        strCompanyName = [selectedClient valueForKey:@"company_name"];
        strWebsiteURL = [selectedClient valueForKey:@"website"];
    }
    else
    {
        NSDictionary *userDetail = [[NSUserDefaults standardUserDefaults]valueForKey:@"LoginUserDetail"];
        strCompanyName = [userDetail valueForKey:@"company_name"];
        strWebsiteURL = [userDetail valueForKey:@"website"];
    }
    
    if ([strWebsiteURL rangeOfString:@"http://"].location==NSNotFound)
    {
        strWebsiteURL = [NSString stringWithFormat:@"http://%@",strWebsiteURL];
    }

    
    NSMutableString *emailBody = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"<a href=%@>%@</a>",strWebsiteURL,strCompanyName]];
    [emailBody appendString:@"&nbsp;|&nbsp;powered by "];
    [emailBody appendString:@"<a href=\"http://www.buildaga.com\">Buildaga</a>"];
    [emailShareController setMessageBody:emailBody isHTML:YES];
    for (int i=0; i<[shringimage count]; i++) {
        
        NSData *jpegData=  UIImageJPEGRepresentation([UIImage imageWithData:[shringimage objectAtIndex:i]], 1);
        [emailShareController addAttachmentData:jpegData mimeType:@"image/jpeg" fileName:[NSString stringWithFormat:@"%d.jpg",i]];
        //[controller addImage:[UIImage imageWithData:[shringimage objectAtIndex:i]]];
        
    }
    [emailShareController setToRecipients:[[editarray valueForKey:@"email_data"] valueForKey:@"email"]];
    if (emailShareController) [self presentViewController:emailShareController animated:YES completion:nil];
    //    [self dismissViewControllerAnimated:YES completion:nil];
    //    [[NSUserDefaults standardUserDefaults]setValue:@"1" forKey:@"FromGallery"];
}
- (IBAction)cancelbtn_click:(id)sender {
    
    [self.shringview removeFromSuperview];
}

-(void)messageContent
{
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
    if([MFMessageComposeViewController canSendText])
    {
        controller.body = @"Buildaga";
        //controller.recipients = @[@1234567890];
        controller.messageComposeDelegate = self;
        [self presentModalViewController:controller animated:YES];
    }
    //whatsapp
    /*NSURL *whatsappURL = [NSURL URLWithString:@"whatsapp://send?text=Hello%2C%20World!"];
     if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
     [[UIApplication sharedApplication] openURL: whatsappURL];
     }
     */
}
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    
    
    [self dismissModalViewControllerAnimated:YES];
    
    if (result == MessageComposeResultCancelled)
        NSLog(@"Message cancelled");
    else if (result == MessageComposeResultSent)
        NSLog(@"Message sent");
    else
        NSLog(@"Message failed");
}
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
    [[NSUserDefaults standardUserDefaults]setValue:@"1" forKey:@"FromGallery"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [self.shringview removeFromSuperview];
}
#pragma mark Multiple Image Delegate

- (void)UzysAssetsPickerController:(UzysAssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets
{
    //    albumImagedataarr=nil;
    //    albumImagedataarr=[[NSMutableArray alloc]init];
    //NSLog(@"%@",assets);
    
    
    
    for (int i=0; i<assets.count; i++)
    {
        if([[assets[i] valueForProperty:@"ALAssetPropertyType"] isEqualToString:@"ALAssetTypePhoto"]) //Photo
        {
            [assets enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                obj=assets[i];
                ALAsset *representation = obj;
                
                UIImage *img = [UIImage imageWithCGImage:representation.defaultRepresentation.fullResolutionImage
                                                   scale:representation.defaultRepresentation.scale
                                             orientation:(UIImageOrientation)representation.defaultRepresentation.orientation];
                
                
                NSData *imagedata = [self compressImage:img];
                
                NSData *imgdata = UIImagePNGRepresentation([UIImage imageWithData:[self compressImage:img]]);
                if ([albumImagedataarr containsObject:imagedata]) {
                    
                }
                else
                {
                    [orignalimages addObject:imagedata];
                    [albumImagedataarr addObject:imagedata];
                    [tempimages addObject:imgdata];
                }
                *stop = YES;
            }];
            
            
        }
    }
    //    NSData *imagedata=[self compressImage:[UIImage imageNamed:@"pic_album"]];
    //       [albumImagedataarr insertObject:imagedata atIndex:0];
    
    temp2=(int)[albumImagedataarr count];
    [self.albumimagestable reloadData];
    [[NSUserDefaults standardUserDefaults]setValue:@"1" forKey:@"FromGallery"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    if ([buttoncreate.titleLabel.text isEqualToString:@"UPDATE"]) {
        NSURL *url = [NSURL URLWithString:kEditProjectImage];
        [APPDELEGATE addLoader:nil];
        ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
        
        
        for (int i =(int)[[editarray valueForKey:@"image_data"] count]; i < [orignalimages count]; i++) {
            
            [request setData:[orignalimages objectAtIndex:i]  withFileName:[NSString stringWithFormat:@"image_%d.png",i] andContentType:@"image/png" forKey:[NSString stringWithFormat:@"image[%d]",i]];
            
        }
        [request addPostValue:[[editarray valueForKey:KData] valueForKey:@"id"]  forKey:@"project_id"];
        [request setDelegate:self];
        [request startAsynchronous];
        
        
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                
            });
        });
        
    }
    
    // counter = 0;
    
    //    delegate.HUD=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    delegate.HUD.mode=MBProgressHUDModeDeterminateHorizontalBar;
    //    delegate.HUD.labelText=[NSString stringWithFormat:@"Uploading %d/%lu",1,(unsigned long)[albumImagedataarr count]];
    //
    //    [self performSelector:@selector(getUploadalbum) withObject:nil afterDelay:0.1];
    
    // delegate.container.panMode = MFSideMenuPanModeNone;
    NSLog(@"Image Count = %lu",(unsigned long)[albumImagedataarr count]);
    
}


- (NSData *)compressImage:(UIImage *)image
{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 600;
    float maxWidth = 800;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth) {
        if(imgRatio < maxRatio){
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio){
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }else{
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img,compressionQuality);
    UIGraphicsEndImageContext();
    
    return imageData;
}
- (void)UzysAssetsPickerControllerDidExceedMaximumNumberOfSelection:(UzysAssetsPickerController *)picker
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Buildaga" message:@"You can select maximum 15 images at one time" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}
- (void)UzysAssetsPickerControllerDidCancel:(UzysAssetsPickerController *)picker
{
    
}
- (IBAction)delete_btn_click:(id)sender {
    
    //{"method":"delete_image","image_id":"3,4"}
    if ([arr_selected count]==0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Buildaga" message:@"Please select image" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else
    {
        
        
        if ([arr_selected count]==1) {
            
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Buildaga" message:@"Are you sure you want to delete this image?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
            alert.tag=1000;
            [alert show];

            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Buildaga" message:@"Are you sure you want to delete these images?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
            alert.tag=1000;
            [alert show];

        }
        
        
        
        
    }
    
    
    //    if ([arr_selected count]>0)
    //    {
    //        NSMutableArray *deleteimages=[[NSMutableArray alloc]init];
    //        for(int i=0;i<[arr_selected count];i++)
    //        {
    //
    //            [deleteimages addObject:[[[editarray valueForKey:@"image_data"] valueForKey:@"id"] objectAtIndex:[[NSString stringWithFormat:@"%@",[arr_selected objectAtIndex:i]]integerValue]-1]];
    //
    //
    //            [deletedimages addObject:[albumImagedataarr  objectAtIndex:[[NSString stringWithFormat:@"%@",[arr_selected objectAtIndex:i]]integerValue]]];
    //        }
    //        //             NSMutableArray *arraytemp=[[NSMutableArray alloc]init];
    //        //             arraytemp = [[editarray valueForKey:@"image_data"] mutableCopy];
    //        //             for(int i=0;i<[arr_selected count];i++)
    //        //             {
    //        //
    //        //                 [arraytemp removeObjectAtIndex:[[NSString stringWithFormat:@"%@",[arr_selected objectAtIndex:i]]integerValue]-1];
    //        //             }
    //        //             NSMutableArray *arraytemp1=[editarray mutableCopy];
    //        //             [arraytemp1 setValue:arraytemp forKey:@"image_data"];
    //        //             editarray=nil;
    //        //             editarray=[[NSMutableArray alloc]init];
    //        //
    //        //             editarray=arraytemp1;
    //        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    //        [dic setObject:@"delete_image" forKey:@"method"];
    //        NSString *joinedComponents = [deleteimages componentsJoinedByString:@","];
    //        [dic setObject:[[editarray valueForKey:KData] valueForKey:@"id"] forKey:@"project_id"];
    //        [dic setObject:joinedComponents forKey:@"image_id"];
    //        [APPDELEGATE addLoader:nil];
    //        [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"delete_image"];
    //    }
    
    
}

#pragma mark web service
-(void)webHandlerManager:(Webservice *)webHandlerManager didFinishLoadingWithData:(NSObject *)responseObject
{
    [APPDELEGATE removeLoader];
    if ([webHandlerManager.strMethod isEqualToString:@"delete_image"])
    {
        if([[responseObject valueForKey:@"sucess"]isEqualToString:@"1"])
        {
            editarray=nil;
            [shringimage removeAllObjects];
            [[NSUserDefaults standardUserDefaults]setObject:responseObject forKey:@"TempProject"];
            [[NSUserDefaults standardUserDefaults]setObject:responseObject forKey:@"EditProject"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            for (int i=0; i<[deletedimages count]; i++) {
                
                if ([tempimages containsObject:[deletedimages objectAtIndex:i]]) {
                    [tempimages removeObject:[deletedimages objectAtIndex:i]];
                }
            }
            
            editarray=[responseObject mutableCopy];
            
            [albumImagedataarr removeAllObjects];
            [orignalimages removeAllObjects];
            albumImagedataarr=nil;
            orignalimages=nil;
            albumImagedataarr=[[NSMutableArray alloc]init];
            orignalimages=[[NSMutableArray alloc]init];
            NSData *imagedata=[self compressImage:[UIImage imageNamed:@"pic_album"]];
            [albumImagedataarr insertObject:imagedata atIndex:0];
            for (int i=0; i<[[editarray valueForKey:@"image_data"] count]; i++) {
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                     NSUserDomainMask, YES);
                NSString *filename =[NSString stringWithFormat:@"Project_%@",[[[editarray valueForKey:@"image_data"] objectAtIndex:i] valueForKey:@"image"]];
                NSString *documentsDirectory = [paths objectAtIndex:0];
                NSString* path = [documentsDirectory stringByAppendingPathComponent:filename];
                UIImage* image = [UIImage imageWithContentsOfFile:path];
                if (image==nil) {
                    
                    NSString *url =  [NSString stringWithFormat:@"%@%@",ThumbImages,[[[editarray valueForKey:@"image_data"] objectAtIndex:i] valueForKey:@"image"]];
                    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                        
                        NSString *str=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                        NSURL *imagepath = [NSURL URLWithString:str];
                        
                        NSData *data = [NSData dataWithContentsOfURL:imagepath];
                        // UIImage *image = [UIImage imageWithData:data];
                        [data writeToFile:path atomically:YES];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [albumImagedataarr addObject:data];
                            [orignalimages addObject:data];
                            
                        });
                    });
                    
                }
                else
                {
                    
                    [albumImagedataarr addObject:[NSData dataWithContentsOfFile:path]];
                    [orignalimages addObject:[NSData dataWithContentsOfFile:path]];
                }
                
            }
            
            for (int i=0; i<[tempimages count]; i++) {
                
                [albumImagedataarr addObject:[tempimages objectAtIndex:i]];
                [orignalimages addObject:[tempimages objectAtIndex:i]];
            }
            
            
            
            temp2=(int)[albumImagedataarr count];
            [self.albumimagestable reloadData];
            
            
            //   for(int i=0;i<[arr_selected count];i++)
            //            {
            //
            ////                [deleteimages addObject:[[[editarray valueForKey:@"image_data"] valueForKey:@"id"] objectAtIndex:[[NSString stringWithFormat:@"%@",[arr_selected objectAtIndex:i]]integerValue]-1]];
            //              //  [albumImagedataarr removeObjectAtIndex:[[NSString stringWithFormat:@"%@",[arr_selected objectAtIndex:i]]integerValue]];
            //
            //                if ([albumImagedataarr containsObject:[deletedimages objectAtIndex:i]])
            //                {
            //                    [albumImagedataarr removeObject:[deletedimages objectAtIndex:i]];
            //
            //                }
            //
            //
            //                temp2--;
            //
            //            }
            //            [deletedimages removeAllObjects];
            
            [arr_selected removeAllObjects];
            [self.albumimagestable reloadData];
        }
        else
        {
            
        }
    }
}
-(void)webHandlerManager:(Webservice *)webHandlerManager didFailedLoadingWithError:(NSString *)responseError
{
    
    NSLog(@"%@",responseError);
    NSLog(@"%@",webHandlerManager.strMethod);
    
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    
    string_value_int=row;
    
    
    
    
    // This method is triggered whenever the user makes a change to the picker selection.
    // The parameter named row and component represents what was selected.
    
    
}

// The number of columns of data
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    return State_List_Array.count;
    
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    return State_List_Array[row];
}

-(void)ShowSelectedDate:(UIBarButtonItem *)sender
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    ContactDetailCell *cell=(ContactDetailCell *)[self.tableview cellForRowAtIndexPath:indexPath];
    
    
    string_value = [State_List_Array objectAtIndex:string_value_int];
    string_IOS = [State_List_iso objectAtIndex:string_value_int];
    
    cell.state.text=[State_List_Array objectAtIndex:string_value_int];
    
    
    [self.view endEditing:YES];
    
}

-(void)ShowSelectedCancel
{
    [self.view endEditing:YES];
    
}


@end
