//
//  SignupTableCellTableViewCell.h
//  Punch
//
//  Created by Jaimish on 01/07/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignupTableCellTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *label;
@property (strong, nonatomic) IBOutlet UITextField *textfield;
@property (strong, nonatomic) IBOutlet UITextView *textview;

@end
