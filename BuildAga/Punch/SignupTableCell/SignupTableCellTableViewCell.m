//
//  SignupTableCellTableViewCell.m
//  Punch
//
//  Created by Jaimish on 01/07/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import "SignupTableCellTableViewCell.h"

@implementation SignupTableCellTableViewCell
@synthesize label,textfield,textview;
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SignupTableCellTableViewCell" owner:self options:nil];
        self = [nib objectAtIndex:0];
        
    }
    return self;
}

@end
