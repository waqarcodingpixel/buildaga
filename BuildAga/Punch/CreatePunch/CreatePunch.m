//
//  CreatePunch.m
//  Punch
//
//  Created by Jaimish on 02/07/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import "CreatePunch.h"
#import "Punchcell.h"
@interface CreatePunch ()
{
    
    NSInteger countpunch;
    NSMutableArray *title,*details,*punch,*task,*taskdetails;
    
    NSMutableDictionary *dictask;
}
@end

@implementation CreatePunch

- (void)viewDidLoad {
    taskdetails=[[NSMutableArray alloc]init];
    dictask=[[NSMutableDictionary alloc]init];
    [dictask setObject:@"" forKey:@"task_title"];
    [dictask setObject:@"" forKey:@"details"];
    [taskdetails addObject:dictask];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    self.punch_title.leftView = paddingView;
    self.punch_title.leftViewMode = UITextFieldViewModeAlways;
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    countpunch=1;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)addpunch_click:(id)sender {
    
    dictask=[[NSMutableDictionary alloc]init];
    [dictask setObject:@"" forKey:@"task_title"];
    [dictask setObject:@"" forKey:@"details"];
    [taskdetails addObject:dictask];

    countpunch++;
    [self.tableview beginUpdates];
    NSIndexPath *myIP = [NSIndexPath indexPathForRow:0 inSection:countpunch-1];
  //  NSArray *myArray = [[NSArray alloc] initWithObjects:myIP, nil];
    
    [self.tableview insertSections:[NSIndexSet indexSetWithIndex:countpunch-1] withRowAnimation:UITableViewRowAnimationTop];
    
    //[self.mytableview deleteRowsAtIndexPaths:path withRowAnimation:UITableViewRowAnimationBottom];
    [self.tableview endUpdates];
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return countpunch;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10.0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Punchcell";
    
    Punchcell *cell = (Punchcell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier ];
    if (cell == nil)
    {
        cell =[[Punchcell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    cell.title.leftView = paddingView;
    cell.title.leftViewMode = UITextFieldViewModeAlways;
    [cell.contentView.layer setBorderColor:[UIColor whiteColor].CGColor];
    [cell.contentView.layer setBorderWidth:1.0f];
    cell.backgroundColor=[UIColor clearColor];
    cell.tasknumber.text=[NSString stringWithFormat:@"Task%ld",indexPath.section+1];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.title.tag=indexPath.section;
    cell.details.tag=indexPath.section;
    cell.title.text=[[taskdetails objectAtIndex:indexPath.section] valueForKey:@"task_title"];
    cell.details.text=[[taskdetails objectAtIndex:indexPath.section] valueForKey:@"details"];
    cell.title.delegate=self;
    cell.details.delegate=self;
    return cell;
    
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if (textField.text.length==1) {
        textField.text = [[[textField.text substringToIndex:1] uppercaseString] stringByAppendingString:[textField.text substringFromIndex:1]];
    }
    if (textField.tag!=5000) {
        [[taskdetails objectAtIndex:textField.tag ] setValue:[textField.text capitalizedString] forKey:@"task_title"];
    }
   
    NSLog(@"%ld",(long)textField.tag);

    return YES;

}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    
    if (textField.text.length==1) {
        textField.text = [[[textField.text substringToIndex:1] uppercaseString] stringByAppendingString:[textField.text substringFromIndex:1]];
    }
    if (textField.tag!=5000) {
         [[taskdetails objectAtIndex:textField.tag ] setValue:[textField.text capitalizedString] forKey:@"task_title"];
    }
    

    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (textView.text.length==1) {
        textView.text = [[[textView.text substringToIndex:1] uppercaseString] stringByAppendingString:[textView.text substringFromIndex:1]];
    }
    [[taskdetails objectAtIndex:textView.tag ] setValue:[textView.text capitalizedString] forKey:@"details"];

    return YES;
}
- (void)textViewDidChange:(UITextView *)textView
{

    if (textView.text.length==1) {
        textView.text = [[[textView.text substringToIndex:1] uppercaseString] stringByAppendingString:[textView.text substringFromIndex:1]];
    }
    [[taskdetails objectAtIndex:textView.tag ] setValue:[textView.text capitalizedString] forKey:@"details"];

    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 195.0;
}
- (IBAction)back_btn_cllick:(id)sender {
   
    [[NSUserDefaults standardUserDefaults]setValue:@"1" forKey:@"FromPunch"];
    
    [[NSUserDefaults standardUserDefaults]synchronize];
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"FromProfileAddPunch"]isEqualToString:@"1"]) {
         [[NSUserDefaults standardUserDefaults]setValue:@"0" forKey:@"FromProfileAddPunch"];
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"FromPunch"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)save_btn_click:(id)sender {
   
    
    
    if ([self.punch_title.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0 ){
        
        
      
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:@"Please enter title" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
        
        
    }
    
    NSMutableDictionary *tasks;
    task=nil;
    task=[[NSMutableArray alloc]init];
    
    for (int i=0; i<countpunch;i++) {
        
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:i];
        Punchcell *cell=(Punchcell *)[self.tableview cellForRowAtIndexPath:indexPath];
        
      
        
        if(cell.title.text.length>0)
        {
            tasks=nil;
    tasks=[[NSMutableDictionary alloc]init];
            [tasks setObject:cell.title.text forKey:@"task_title"];
            [tasks setObject:cell.details.text forKey:@"details"];
            
            [task addObject:tasks];
        
        }
        
        
    }
    
    
    
    
      //{"method":"add_punch","project_id": "5","punch_title": "punch 1","task":[{"task_title": "task1","details": "abcd"},{"task_title": "task2","details": "efg"},{"task_title": "task3","details": "xyz"}]}
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    [dic setObject:@"add_punch" forKey:@"method"];
    [dic setObject:[[[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedProject"] valueForKey:@"id"] forKey:@"project_id"];
    [dic setObject:self.punch_title.text forKey:@"punch_title"];
    [dic setObject:task forKey:@"task"];

    [APPDELEGATE addLoader:nil];
    [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"add_punch"];
    
    
}
#pragma mark web service
-(void)webHandlerManager:(Webservice *)webHandlerManager didFinishLoadingWithData:(NSObject *)responseObject
{
    [APPDELEGATE removeLoader];
    if ([webHandlerManager.strMethod isEqualToString:@"add_punch"])
    {
        if([[responseObject valueForKey:@"sucess"]isEqualToString:@"1"])
        {
           
            [[NSUserDefaults standardUserDefaults]setObject:[[responseObject valueForKey:KData]valueForKey:@"punch_title"] forKey:@"PuchNameList"];
            [[NSUserDefaults standardUserDefaults]setValue:@"1" forKey:@"FromPunch"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"FromProfileAddPunch"]isEqualToString:@"1"]) {
                [[NSUserDefaults standardUserDefaults]setValue:@"0" forKey:@"FromProfileAddPunch"];
                [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"FromPunch"];
                [[NSUserDefaults standardUserDefaults]synchronize];
            }
            [self.navigationController popViewControllerAnimated:YES];
            

        }
        else
        {
            
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Punch" message:[responseObject valueForKey:@"msg"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            
        }
    }

}
-(void)webHandlerManager:(Webservice *)webHandlerManager didFailedLoadingWithError:(NSString *)responseError
{
    
    NSLog(@"%@",responseError);
    NSLog(@"%@",webHandlerManager.strMethod);
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
