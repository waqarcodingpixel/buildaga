//
//  Punchcell.h
//  Punch
//
//  Created by Jaimish on 03/07/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Punchcell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *tasknumber;
@property (strong, nonatomic) IBOutlet UITextField *title;
@property (strong, nonatomic) IBOutlet UITextView *details;
@property (strong, nonatomic) IBOutlet UIButton *save;

@end
