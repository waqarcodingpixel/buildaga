//
//  CustomerView.m
//  Punch
//
//  Created by Jaimish on 06/07/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import "CustomerView.h"
#import "HomeviewCell.h"
#import "ProjectList.h"
@interface CustomerView ()
{

    NSMutableArray *list;
}
@end

@implementation CustomerView

- (void)viewDidLoad {

//    list=[[NSUserDefaults standardUserDefaults]valueForKey:@"List"];
    [super viewDidLoad];
    customertalbe.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    if ([[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails]valueForKey:@"role"]isEqualToString:@"CU"]) {
           namelabel.text=@"COMPANY";
        
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        [dic setObject:@"company_list" forKey:@"method"];
        [dic setObject:[[[NSUserDefaults standardUserDefaults] valueForKey:LoginUserDetails] valueForKey:@"id"] forKey:@"user_id"];
        [dic setObject:@"1" forKey:@"is_all_data"];
        [APPDELEGATE addLoader:nil];
        [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"company_list"];
    }
    else
    {
        namelabel.text=@"CUSTOMERS";

        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        [dic setObject:@"customer_list" forKey:@"method"];
        [dic setObject:[[[NSUserDefaults standardUserDefaults] valueForKey:LoginUserDetails] valueForKey:@"id"] forKey:@"user_id"];
        [dic setObject:@"1" forKey:@"is_all_data"];
        [APPDELEGATE addLoader:nil];
        [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"customer_list"];
     
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  [list count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"HomeTableViewCell";
    HomeviewCell *cell1 = (HomeviewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier ];
    if (cell1 == nil)
    {
        cell1 =[[HomeviewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier ];
        
        
    }
    else
        
    {
        cell1.imageView.image=nil;
    }
    cell1.selectionStyle = UITableViewCellSelectionStyleNone;
    UIButton *button=[[UIButton alloc]initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width-35, 18,35 , 36)];
    [button setImage:[UIImage imageNamed:@"next"] forState:UIControlStateNormal];
      button.userInteractionEnabled=FALSE;
    // cell.accessoryView=button;
    
    [cell1.contentView addSubview:button];
    cell1.backgroundColor=[UIColor clearColor];
    
    if ([[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails]valueForKey:@"role"]isEqualToString:@"CU"]) {
        
        cell1.name.text=[[list objectAtIndex:indexPath.row] valueForKey:@"company_name"];
    }
    else{
        
        cell1.name.text=[NSString stringWithFormat:@"%@ %@",[[list objectAtIndex:indexPath.row] valueForKey:@"lastname"],[[list objectAtIndex:indexPath.row] valueForKey:@"name"]];
    }
    
    cell1.imageview.image=[UIImage imageNamed:@"pic_signup"];
    
    
    //                cell1.contact.text=[[list objectAtIndex:indexPath.row] valueForKey:@"phoneno"];
    
    
    [cell1.display setTitle:[[list objectAtIndex:indexPath.row] valueForKey:@"phoneno"] forState:UIControlStateNormal];
    cell1.contactbtn.tag=indexPath.row;
    
    [cell1.contactbtn addTarget:self action:@selector(makecall:) forControlEvents:UIControlEventTouchUpInside];
    [cell1.address setTitle:[[list objectAtIndex:indexPath.row] valueForKey:@"address"] forState:UIControlStateDisabled];
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    //
    NSString *url = [NSString stringWithFormat:@"%@%@",UserProfile,[[list objectAtIndex:indexPath.row] valueForKey:@"image"]];
    NSString *filename =[[list objectAtIndex:indexPath.row] valueForKey:@"image"] ;
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:filename];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    if (image==nil) {
        
        NSString *url = [NSString stringWithFormat:@"%@%@",UserProfile,[[list objectAtIndex:indexPath.row] valueForKey:@"image"]];
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            
            NSString *str=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *imagepath = [NSURL URLWithString:str];
            
            NSData *data = [NSData dataWithContentsOfURL:imagepath];
            UIImage *image = [UIImage imageWithData:data];
            [data writeToFile:path atomically:YES];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [cell1.imageview setImage:image];
                
                
            });
        });
        
    }
    else
    {
        [cell1.imageview setImage:image];
        
    }
    return cell1;
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return  94;
}
- (IBAction)back_btn_click:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   // {"method":"project_list","user_id":"4","login_user_id":"5"}
    
    [[NSUserDefaults standardUserDefaults]setValue:[list objectAtIndex:indexPath.row] forKey:@"SelectedClient"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    ProjectList *Projects=[[ProjectList alloc]init];
    [self.navigationController pushViewController:Projects animated:YES];
    
}

-(void)makecall:(UIButton *)sender
{
    NSString *phoneNumber = [@"telprompt://" stringByAppendingString:[[list objectAtIndex:[sender tag]]valueForKey:@"phoneno"]];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
    
    
}
#pragma mark web service
-(void)webHandlerManager:(Webservice *)webHandlerManager didFinishLoadingWithData:(NSObject *)responseObject
{
    [APPDELEGATE removeLoader];
    
   
    if ([webHandlerManager.strMethod isEqualToString:@"company_list"])
    {
        if([[responseObject valueForKey:@"sucess"]isEqualToString:@"1"])
        {
            
            
            list=[responseObject valueForKey:KData];
            [customertalbe reloadData];
        }
        else
        {
            
        }
    }
    if ([webHandlerManager.strMethod isEqualToString:@"customer_list"])
    {
        if([[responseObject valueForKey:@"sucess"]isEqualToString:@"1"])
        {
            list=[responseObject valueForKey:KData];
            [customertalbe reloadData];
            
        }
        else
        {
            
        }
    }
}
-(void)webHandlerManager:(Webservice *)webHandlerManager didFailedLoadingWithError:(NSString *)responseError
{
    
    NSLog(@"%@",responseError);
    NSLog(@"%@",webHandlerManager.strMethod);
    
}


@end
