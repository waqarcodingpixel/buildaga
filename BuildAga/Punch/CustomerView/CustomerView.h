//
//  CustomerView.h
//  Punch
//
//  Created by Jaimish on 06/07/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomerView : UIViewController<UITableViewDelegate,UITableViewDataSource>
{

    IBOutlet UITableView *customertalbe;

    IBOutlet UILabel *namelabel;
}

@end
