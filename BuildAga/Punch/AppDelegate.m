//
//  AppDelegate.m
//  Punch
//
//  Created by Jaimish on 30/06/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import "AppDelegate.h"
#import "IQKeyboardManager.h"
#import "ListMessageviewViewController.h"
#import "SBJson.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "NetworkViewController.h"

#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

void onUncaughtException(NSException* exception)
{
    //[[Crashlytics sharedInstance] crash];
}

@interface AppDelegate ()<FBLoginViewDelegate>
{
    Reachability* hostReach;
    Reachability* internetReach;
    Reachability* wifiReach;
    BOOL _isNetAvailable;
      UIActivityIndicatorView *indicator;
    BOOL _changeReachability;

}
@end

@implementation AppDelegate
@synthesize devicetoken;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    
    NSSetUncaughtExceptionHandler(&onUncaughtException);
    
    _changeReachability = YES;
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reachabilityChanged:) name: kReachabilityChangedNotification object: nil];
    [self checkallTypesofInternet];
    
//   [[IQKeyboardManager sharedManager] disableToolbarInViewControllerClass:[ListMessageviewViewController class]];
   
    [Fabric with:@[[Crashlytics class]]];

    if(iOS8)
    {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIRemoteNotificationTypeBadge
                                                                                             |UIRemoteNotificationTypeSound
                                                                                             |UIRemoteNotificationTypeAlert) categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge |
                                                                               UIRemoteNotificationTypeSound |
                                                                               UIRemoteNotificationTypeAlert)];
        
    }
    
//    if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded) {
//        NSLog(@"Found a cached session");
//        // If there's one, just open the session silently, without showing the user the login UI
//        [FBSession openActiveSessionWithReadPermissions:@[@"public_profile",@"email"]
//                                           allowLoginUI:NO
//                                      completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
//                                          // Handler for session state changes
//                                          // This method will be called EACH time the session state changes,
//                                          // also for intermediate states and NOT just when the session open
//                                          [self sessionStateChanged:session state:state error:error];
//                                      }];
//        
//        // If there's no cached session, we will show a login button
//    }
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:KLoginSuccess]isEqualToString:@"1"]) {
        Homeview *view=[[Homeview alloc]init];
           self.nav=[[UINavigationController alloc]initWithRootViewController:view];
    }
    else
   {
    self.view=[[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
          self.nav=[[UINavigationController alloc]initWithRootViewController:self.view];
    }
    self.window=[[UIWindow alloc]initWithFrame:[[UIScreen mainScreen]bounds]];
 
    self.nav.navigationBarHidden=TRUE;
    self.window.rootViewController=self.nav;
    [self.window makeKeyAndVisible];
  
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                    didFinishLaunchingWithOptions:launchOptions];;
}
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}




- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    application.applicationIconBadgeNumber = 0;
    NSLog(@"userInfo %@",userInfo);
    for (id key in userInfo) {
        NSLog(@"key: %@, value: %@", key, [userInfo objectForKey:key]);
    }
    
    [application setApplicationIconBadgeNumber:[[[userInfo objectForKey:@"aps"] objectForKey:@"badge"] intValue]];
    
    NSLog(@"Badge %d",[[[userInfo objectForKey:@"aps"] objectForKey:@"id"] intValue]);
    
    NSLog(@"Badge %d",[[[userInfo objectForKey:@"aps"] objectForKey:@"type"] intValue]);
    
    
    if (application.applicationState == UIApplicationStateActive)
    {
        
        
            UIView *alertview = [[UIView alloc] initWithFrame:CGRectMake(0, -200, CGRectGetWidth(self.window.bounds), 80)];
            alertview.userInteractionEnabled=YES;
            alertview.backgroundColor=[UIColor whiteColor];
            
            
            [self.window addSubview:alertview];
            UIImageView *logoimg=[[UIImageView alloc]init];
            logoimg.frame=CGRectMake(20, 20, 29, 29);
            logoimg.image=[UIImage imageNamed:@"AppI.png"];
            logoimg.userInteractionEnabled=YES;
            [alertview addSubview:logoimg];
            
            
            
            //Create a label to display the message and add it to the alertView
            UILabel *app_name = [[UILabel alloc] initWithFrame:CGRectMake(logoimg.frame.origin.x+logoimg.frame.size.width+10, 30, 100, 15)];
            app_name.text = @"Punch";
            app_name.font=[UIFont boldSystemFontOfSize:14];
            app_name.textColor=[UIColor colorWithRed:36.0f/255.0f green:41.0f/255.0f blue:49.0f/255.0f alpha:1.0];
            app_name.backgroundColor=[UIColor clearColor];
            app_name.userInteractionEnabled=YES;
            [alertview addSubview:app_name];
            
        
            //Create a label to display the message and add it to the alertView
            UILabel *theMessage = [[UILabel alloc] initWithFrame:CGRectMake(logoimg.frame.origin.x+logoimg.frame.size.width+10, app_name.frame.origin.y+app_name.frame.size.height,alertview.frame.size.width-(logoimg.frame.origin.x+logoimg.frame.size.width+10), 35)];
            
            theMessage.lineBreakMode = NSLineBreakByWordWrapping;
            theMessage.numberOfLines = 0;
            
            theMessage.text = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
            theMessage.textColor=[UIColor colorWithRed:36.0f/255.0f green:41.0f/255.0f blue:49.0f/255.0f alpha:1.0];
            theMessage.font=[UIFont systemFontOfSize:12];
            theMessage.backgroundColor=[UIColor clearColor];
            // [theMessage sizeToFit];
            theMessage.userInteractionEnabled=YES;
            [alertview addSubview:theMessage];
            
            
            
            
            [UIView animateWithDuration:1.0
                                  delay:0.0
                                options: UIViewAnimationOptionCurveLinear
                             animations:^{
                                 alertview.frame =CGRectMake(0, 0, CGRectGetWidth(self.window.bounds), 80);
                                 
                                 
                                 
                                 [UIView animateWithDuration:1.0
                                                       delay:7.0
                                                     options: UIViewAnimationOptionCurveLinear
                                                  animations:^{
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      alertview.frame =CGRectMake(0, -200, CGRectGetWidth(self.window.bounds), 80);
                                                      
                                                      
                                                      
                                                  }
                                                  completion:nil];
                                 
                                 
                                 
                                 
                                 
                             }
                             completion:nil];
            
       
        
    }
    else
    {
        
        
        
    }
    
    
    
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"TempProject"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"EditProject"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [[NSUserDefaults standardUserDefaults]setValue:@"0" forKey:@"FromPunch"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"FromEditingMode"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"EditArray"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

#pragma Register Device Token
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    // Updates the device token and registers the token with UA
    
    NSString *deviceTokenString = [[[[deviceToken description]
                                     stringByReplacingOccurrencesOfString: @"<" withString: @""]
                                    stringByReplacingOccurrencesOfString: @">" withString: @""]
                                   stringByReplacingOccurrencesOfString: @" " withString: @""];
    
    devicetoken = deviceTokenString;
    
    
    
    [[NSUserDefaults standardUserDefaults]setValue:devicetoken forKey:@"device_token"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    NSLog(@"%@",devicetoken);
}


- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    [[NSUserDefaults standardUserDefaults]setValue:@"4727887462c7c8a205006e4ef9fa6a70344715b11fe2a1e7bb2254c22a9963fe" forKey:@"device_token"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

-(BOOL)checkInternetConnection
{
    //    return [[Reachability sharedReachability] internetConnectionStatus];
    
    if(!_isNetAvailable)
    {
        //Please, check your internet connection! ShowAndTell requires an active internet connection.
        return NO;
    }
    return YES;
}
- (BOOL)checkInternet
{
    if(!_isNetAvailable)
    {
        //Please, check your internet connection! ShowAndTell requires an active internet connection.
        return NO;
        
    }
    return YES;
}
- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    if (_changeReachability)
    {
        NetworkViewController *network;
        if(netStatus==NotReachable)
        {
//            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Punch List" message:@"Please Check Network Connetion" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//            [alert show];
//              [self removeLoader];
    
            network=[[NetworkViewController alloc]init];
            [self.nav.topViewController presentViewController:network animated:YES completion:nil];
            
            
            
            _isNetAvailable = NO;
        }
        else
        {
            
            [self.nav.topViewController dismissViewControllerAnimated:YES completion:^{
                
                //SHOW YOUR NEW VIEW CONTROLLER HERE!
            }];
            _isNetAvailable = YES;
            _changeReachability = NO;
        }
    }
}
//Called by Reachability whenever status changes.

- (void) reachabilityChanged: (NSNotification* )note
{
    // when internet connection lost it will redirect to home page
    
    _changeReachability = YES;
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
    
    if (!_isNetAvailable)
    {
        
        
        
    }
}
-(void)checkallTypesofInternet
{
    // For Individual Net Connection
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
}

-(BOOL)CheckConnection
{
    Reachability *reachable=[Reachability reachabilityForInternetConnection];
    NetworkStatus status=[reachable currentReachabilityStatus];
    
    [reachable startNotifier];
    
    if(status==NotReachable)
    {
        
            networkcheck=NO;
    }
    else if(status==ReachableViaWWAN)
    {
        networkcheck=TRUE;
    }
    else if(status==ReachableViaWiFi)
    {
        networkcheck=TRUE;
    }
    return networkcheck;
}
+ (AppDelegate *)sharedDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

#pragma mark addloader
-(void)addLoader:(NSString *)string
{
    
    
    
    if (!indicator) {
        indicator =[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        
    }
  
    indicator.backgroundColor=[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.7];
    
    indicator.center = self.window.center;
    //    [self.view addSubview:indicator];
    [self.window addSubview:indicator];
    
    [indicator bringSubviewToFront:self.window];
    //[UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    [indicator startAnimating];
    
    
    [APPDELEGATE.window setUserInteractionEnabled:FALSE];
    
}
-(void)removeLoader
{
    //    [hud hide:YES afterDelay:3];
    [indicator stopAnimating];
    [self.window setUserInteractionEnabled:TRUE];
}

#pragma mark handleNull
-(NSString *)handleNull:(NSObject *)response
{
    NSString *str=[NSString stringWithFormat:@"%@",response];

    if ([str rangeOfString:@"<NULL>"].location == NSNotFound) {
        return nil;
    } else {
        
        return  [str stringByReplacingOccurrencesOfString:@"<NULL>" withString:@""];
    }
    
    return nil;

}
- (void)showMessage:(NSString *)text withTitle:(NSString *)title
{
    [[[UIAlertView alloc] initWithTitle:title
                                message:text
                               delegate:self
                      cancelButtonTitle:@"OK!"
                      otherButtonTitles:nil] show];
}
#pragma mark CallWebService
-(void)callWebservice:(id)returnDic andconrtoller:(id)viewController1 methodName:(NSString *)methodName;
{
    NSLog(@"%@ %@",returnDic,viewController1);
    
    if ([self CheckConnection]) {
        
        Webservice *objWebservice = [[Webservice alloc]init];
        [objWebservice setDelegate:viewController1];
        [objWebservice webserviceCall:[returnDic JSONRepresentation] methodName:methodName];
        
    }
    else
    {
        // [[NSUserDefaults standardUserDefaults]setValue:keyYes forKey:keyMemberLogin];
      
        if ( [[[NSUserDefaults standardUserDefaults] valueForKey:KLoginSuccess] isEqualToString:@"1"]) {
            
            
        }
        else
        {
            
            
            
        }
        
        //[self.navigationController popToRootViewControllerAnimated:YES];
        
    }
    
    
}
-(void)webHandlerManager:(Webservice *)webHandlerManager didFinishLoadingWithData:(NSObject *)responseObject
{
    
    
}
-(void)webHandlerManager:(Webservice *)webHandlerManager didFailedLoadingWithError:(NSString *)responseError
{
    NSLog(@"%@",responseError);
    NSLog(@"%@",webHandlerManager.strMethod);
    
}

@end
