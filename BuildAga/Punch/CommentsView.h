//
//  CommentsView.h
//  Punch
//
//  Created by Hyperlink on 06/07/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentsView : UIView
@property (strong, nonatomic) IBOutlet UITableView *commenttable;
@property (weak, nonatomic) IBOutlet UILabel *detailTask;
@property (weak, nonatomic) IBOutlet UIView *detailview;
@property (weak, nonatomic) IBOutlet UIView *chatview;
@property (weak, nonatomic) IBOutlet UILabel *tasktitle;
@property (strong, nonatomic) IBOutlet UIView *mainview;
@property (strong, nonatomic) IBOutlet UIButton *closebtn;
@property (strong, nonatomic) IBOutlet UITextView *texview;


@property (weak, nonatomic) IBOutlet UITextView *chattextview;
@property (weak, nonatomic) IBOutlet UIButton *sendbutton;

@end
