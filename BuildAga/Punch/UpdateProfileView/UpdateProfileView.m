//
//  UpdateProfileView.m
//  Punch
//
//  Created by Jaimish on 18/07/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import "UpdateProfileView.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "SBJson.h"
#import "SignupTableCellTableViewCell.h"
#import  "Profileview.h"

@interface UpdateProfileView ()
{

    NSMutableArray *contractor,*customer,*fbgmail,*details;
    NSData *imagedataexixts;
    int i;
}
@end

@implementation UpdateProfileView

- (void)viewDidLoad {
    
     [super viewDidLoad];
    
    
    
    checkimage=false;
    
    
    
    
    string_value=@"";
    string_IOS=@"";
    
    
    
    State_List_iso=[[NSMutableArray alloc]initWithObjects:@"AL",
                    @"AK",
                    @"AZ",
                    @"AR",
                    @"CA",
                    @"CO",
                    @"CT",
                    @"DE",
                    @"DC",
                    @"FL",
                    @"GA",
                    @"HI",
                    @"ID",
                    @"IL",
                    @"IN",
                    @"IA",
                    @"KS",
                    @"KY",
                    @"LA",
                    @"ME",
                    @"MD",
                    @"MA",
                    @"MI",
                    @"MN",
                    @"MS",
                    @"MO",
                    @"MT",
                    @"NE",
                    @"NV",
                    @"NH",
                    @"NJ",
                    @"NM",
                    @"NY",
                    @"NC",
                    @"ND",
                    @"OH",
                    @"OK",
                    @"OR",
                    @"PA",
                    @"RI",
                    @"SC",
                    @"SD",
                    @"TN",
                    @"TX",
                    @"UT",
                    @"VT",
                    @"VA",
                    @"WA",
                    @"WV",
                    @"WI",
                    @"WY", nil];
    
    
    
    State_List_Array=[[NSMutableArray alloc]initWithObjects:@"Alabama",
                      @"Alaska",
                      @"Arizona",
                      @"Arkansas",
                      @"California",
                      @"Colorado",
                      @"Connecticut",
                      @"Delaware",
                      @"District of Columbia",
                      @"Florida",
                      @"Georgia",
                      @"Hawaii",
                      @"Idaho",
                      @"Illinois",
                      @"Indiana",
                      @"Iowa",
                      @"Kansas",
                      @"Kentucky",
                      @"Louisiana",
                      @"Maine",
                      @"Maryland",
                      @"Massachusetts",
                      @"Michigan",
                      @"Minnesota",
                      @"Mississippi",
                      @"Missouri",
                      @"Montana",
                      @"Nebraska",
                      @"Nevada",
                      @"New Hampshire",
                      @"New Jersey",
                      @"New Mexico",
                      @"New York",
                      @"North Carolina",
                      @"North Dakota",
                      @"Ohio",
                      @"Oklahoma",
                      @"Oregon",
                      @"Pennsylvania",
                      @"Rhode Island",
                      @"South Carolina",
                      @"South Dakota",
                      @"Tennessee",
                      @"Texas",
                      @"Utah",
                      @"Vermont",
                      @"Virginia",
                      @"Washington",
                      @"West Virginia",
                      @"Wisconsin",
                      @"Wyoming", nil];
    
   
    self.tableview.tableHeaderView = self.headerview;
    self.tableview.tableFooterView = self.footerview;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    //
   // NSString *url = [NSString stringWithFormat:@"%@%@",UserProfile,[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails] valueForKey:@"image"]];
    NSString *filename =[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails]valueForKey:@"image"] ;
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:filename];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    if (image==nil) {
        
        NSString *url = [NSString stringWithFormat:@"%@%@",UserProfile,[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails]valueForKey:@"image"]];
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            
            NSString *str=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *imagepath = [NSURL URLWithString:str];
            
            NSData *data = [NSData dataWithContentsOfURL:imagepath];
            UIImage *image = [UIImage imageWithData:data];
            [data writeToFile:path atomically:YES];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self.profileimage setImage:image];
               

                
            });
        });
        
    }
    else
    {
        [self.profileimage setImage:image];
       
        
    }
    
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(takePhoto1)];
    [self.profileimage addGestureRecognizer:tap];
    [super viewDidLoad];
    if ([[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails]valueForKey:@"role"]isEqualToString:@"CU"]) {
          [self.cusotmer setSelected:TRUE];
        
    }
    else
    {
     [self.contrator setSelected:TRUE];
    }
  
    
  contractor=[[NSMutableArray alloc]initWithObjects:@"First Name:",@"Last Name:",@"Company:",@"Email:",@"Website:",@"Address:",@"City",@"State",@"Zip",@"Phone No.:",@"About Us:", nil];
    customer=[[NSMutableArray alloc]initWithObjects:@"Name:",@"Email:",@"Password:",@"Address:",@"City",@"State",@"Zip",@"Phone No.:",@"Details:", nil];
    // Do any additional setup after loading the view from its nib.
    
    details=[[NSUserDefaults standardUserDefaults]valueForKey:@"CurrentUser"];
    
    NSLog(@"Width value :  %f",_profileimage.frame.origin.x+_profileimage.frame.size.width+35);
    
    
      NSLog(@"ALL DATA :  %@",details);
    
    
    edit_profile_pic=[UIButton buttonWithType:UIButtonTypeCustom];
    
    // edit_profile_pic.backgroundColor=[UIColor whiteColor];
    
    [edit_profile_pic setBackgroundImage:[UIImage imageNamed:@"pencil-40-40.png"] forState:UIControlStateNormal];
    
    
    [edit_profile_pic addTarget:self action:@selector(edit_action) forControlEvents:UIControlEventTouchUpInside];
    [_tableview addSubview:edit_profile_pic];
    
  

   
    // Do any additional setup after loading the view from its nib.
}



-(void)edit_action
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];

}

-(void)viewWillAppear:(BOOL)animated
{
    
    [self performSelector:@selector(callserver) withObject:nil afterDelay:0.3];
    

}


-(void)callserver
{
    edit_profile_pic.frame=CGRectMake(_profileimage.frame.origin.x+_profileimage.frame.size.width+18, 76, 20, 20);
    
    NSLog(@"Width value Next:  %f",_profileimage.frame.origin.x+_profileimage.frame.size.width+35);

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)takePhoto1
{
    /*
     
     UIImagePickerController *picker = [[UIImagePickerController alloc] init];
     picker.delegate = self;
     picker.allowsEditing = YES;
     picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
     
     if (iPhone) {
     [self presentViewController:picker animated:YES completion:NULL];
     }
     //if iPad
     else {
     // Change Rect to position Popover
     UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:picker];
     [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, profileimage.frame.origin.y+profileimage.frame.size.height, 0, 0)inView:mainscrollview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
     }
     */
//    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
//    picker.delegate = self;
//    picker.allowsEditing = YES;
//    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//    
//    [self presentViewController:picker animated:YES completion:NULL];
    
    
    
    
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    
    
    checkimage=true;
    
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    
    self.profileimage.image = chosenImage;
    
    imagedataexixts=UIImagePNGRepresentation(self.profileimage.image);
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    
    [picker dismissViewControllerAnimated:YES completion:^(void){
        
        
    }];
    
    
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    
    string_value_int=row;
    
    
    
    
    // This method is triggered whenever the user makes a change to the picker selection.
    // The parameter named row and component represents what was selected.
    
    
}

// The number of columns of data
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    return State_List_Array.count;
    
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    return State_List_Array[row];
}



- (BOOL)validateEmail:(NSString *)emailStr
{
    NSString *emailRegex = @"^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,4})$";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailStr];
}
- (BOOL) validateUrl: (NSString *) candidate {
    NSString *regexString = @"((http|https)://)*((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";

    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regexString];
    return [urlTest evaluateWithObject:candidate];
}

- (NSString *)validatePage1 {
    NSString *errorMessage;
    
    NSCharacterSet *notDigits = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    SignupTableCellTableViewCell *cell=(SignupTableCellTableViewCell *)[self.tableview cellForRowAtIndexPath:indexPath];
    
    // // contractor=[[NSMutableArray alloc]initWithObjects:@"First Name:",@"Last Name:",@"Company:",@"Email ID:",@"Website:",@"Address:",@"Phone No.:",@"Details:", nil];
    NSLog(@"%@",cell.textfield.text);
    if ([cell.textfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0 ){
        
        
        errorMessage = @"Please enter first name";
        return errorMessage;
        
        
    }
    
    indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
    cell=(SignupTableCellTableViewCell *)[self.tableview cellForRowAtIndexPath:indexPath];
    if ([cell.textfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0)
    {
        
        errorMessage = @"Please enter last name";
        return errorMessage;
        
        
        /* if ([self validateEmail:self.txt_email.text]) {
         
         }*/
    }
    
    if (self.contrator.selected) {
        indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
        cell=(SignupTableCellTableViewCell *)[self.tableview cellForRowAtIndexPath:indexPath];
        if ([cell.textfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0)
        {
            
            errorMessage = @"Please enter company name";
            return errorMessage;
            /* if ([self validateEmail:self.txt_email.text]) {
             
             }*/
        }
       
    }
   
    indexPath = [NSIndexPath indexPathForRow:3 inSection:0];
    cell=(SignupTableCellTableViewCell *)[self.tableview cellForRowAtIndexPath:indexPath];
    if ([cell.textfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0)
    {
        
        errorMessage = @"Please enter email";
        return errorMessage;
        /* if ([self validateEmail:self.txt_email.text]) {
         
         }*/
    }
    if (![self validateEmail:cell.textfield.text]) {
        errorMessage = @"Please enter a valid email address";
        return errorMessage;
    }
    if (self.contrator.selected) {
        indexPath = [NSIndexPath indexPathForRow:4 inSection:0];
        cell=(SignupTableCellTableViewCell *)[self.tableview cellForRowAtIndexPath:indexPath];
        if ([cell.textfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0)
        {
            
            errorMessage = @"Please enter website URL";
            return errorMessage;
            /* if ([self validateEmail:self.txt_email.text]) {
             
             }*/
        }
        if (![self validateUrl:cell.textfield.text])
        {
            
            errorMessage = @"Please enter valid website URL \n (http/https)://www.xyz.com)";
            return errorMessage;
            /* if ([self validateEmail:self.txt_email.text]) {
             
             }*/
        }
    }
    
    indexPath = [NSIndexPath indexPathForRow:5 inSection:0];
    cell=(SignupTableCellTableViewCell *)[self.tableview cellForRowAtIndexPath:indexPath];
    if ([cell.textfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0)
    {
        
        errorMessage = @"Please enter Address";
        return errorMessage;
        
        
        /* if ([self validateEmail:self.txt_email.text]) {
         
         }*/
    }
    
    
    
    indexPath = [NSIndexPath indexPathForRow:6 inSection:0];
    cell=(SignupTableCellTableViewCell *)[self.tableview cellForRowAtIndexPath:indexPath];
    if ([cell.textfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0)
    {
        
        errorMessage = @"Please enter City";
        return errorMessage;
        /* if ([self validateEmail:self.txt_email.text]) {
         
         }*/
    }
    
    
    
    indexPath = [NSIndexPath indexPathForRow:7 inSection:0];
    cell=(SignupTableCellTableViewCell *)[self.tableview cellForRowAtIndexPath:indexPath];
    if ([cell.textfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0)
    {
        
        errorMessage = @"Please enter State";
        return errorMessage;
        /* if ([self validateEmail:self.txt_email.text]) {
         
         }*/
    }
    
    
    
    indexPath = [NSIndexPath indexPathForRow:8 inSection:0];
    cell=(SignupTableCellTableViewCell *)[self.tableview cellForRowAtIndexPath:indexPath];
    if ([cell.textfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0)
    {
        
        errorMessage = @"Please enter Zip";
        return errorMessage;
        /* if ([self validateEmail:self.txt_email.text]) {
         
         }*/
    }
    
    
    indexPath = [NSIndexPath indexPathForRow:9 inSection:0];
    cell=(SignupTableCellTableViewCell *)[self.tableview cellForRowAtIndexPath:indexPath];
    if ([cell.textfield.text stringByTrimmingCharactersInSet:notDigits].length < 10)
    {
        if (cell.textfield.text.length==0) {
            errorMessage = @"Please enter phone number";
            return errorMessage;
        }
        
        
        errorMessage = @"Please enter valid phone number";
        return errorMessage;
        
        
        /* if ([self validateEmail:self.txt_email.text]) {
         
         }*/
    }
    
    

    
    return errorMessage;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (self.contrator.selected)
    {
        
        if (indexPath.row == [contractor count]-1)
        {
            return 73.0;
        }
        return 46.0;
    }
    else
    {
        if (indexPath.row == 2)
        {
            return 0.0;
        }
        if (indexPath.row == 4)
        {
            return 0.0;
        }
        else
        {
            if (indexPath.row == [contractor count]-1)
            {
                return 73.0;
            }
            
            return 46.0;
        }
    }
    return 46.0;
}


- (IBAction)back_btn_cllick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)signup_btn_click:(id)sender {
    NSString *errorMessage = [self validatePage1];
    if (errorMessage) {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        
        
        return;
    }
    
    [APPDELEGATE addLoader:nil];
    
    
    NSURL *url = [NSURL URLWithString:kEditProfile];
   // contractor=[[NSMutableArray alloc]initWithObjects:@"First Name:",@"Last Name:",@"Company:",@"Email ID:",@"Website:",@"Address:",@"Phone No.:",@"Details:", nil];
    //user_id=1&name=sonali&email=sonali@hyperlinkinfosystem.com&address=shahibaug ahmedabad&phoneno=98784575711&device_token=Afdrfdrvrf&device_type=A
    //@"Name:",@"Email ID:",@"Website:",@"Address:",@"Phone No.:",@"Details:
    ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    SignupTableCellTableViewCell *cell=(SignupTableCellTableViewCell *)[self.tableview cellForRowAtIndexPath:indexPath];
    
    [request addPostValue:cell.textfield.text forKey:@"name"];
    indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
    cell=(SignupTableCellTableViewCell *)[self.tableview cellForRowAtIndexPath:indexPath];
    
    [request addPostValue:cell.textfield.text forKey:@"lastname"];
    if (self.contrator.selected) {
        indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
        cell=(SignupTableCellTableViewCell *)[self.tableview cellForRowAtIndexPath:indexPath];
        [request addPostValue:cell.textfield.text forKey:@"company_name"];
        
        
        
    }

    indexPath = [NSIndexPath indexPathForRow:3 inSection:0];
    cell=(SignupTableCellTableViewCell *)[self.tableview cellForRowAtIndexPath:indexPath];
    
    [request addPostValue:cell.textfield.text forKey:@"email"];
    if (self.contrator.selected) {
        indexPath = [NSIndexPath indexPathForRow:4 inSection:0];
        cell=(SignupTableCellTableViewCell *)[self.tableview cellForRowAtIndexPath:indexPath];
        [request addPostValue:cell.textfield.text forKey:@"website"];
  
        
        
    }
    
    indexPath = [NSIndexPath indexPathForRow:5 inSection:0];
    cell=(SignupTableCellTableViewCell *)[self.tableview cellForRowAtIndexPath:indexPath];
    [request addPostValue:cell.textfield.text forKey:@"address"];
    
    indexPath = [NSIndexPath indexPathForRow:6 inSection:0];
    cell=(SignupTableCellTableViewCell *)[self.tableview cellForRowAtIndexPath:indexPath];
    [request addPostValue:cell.textfield.text forKey:@"city"];
    
    
    
    indexPath = [NSIndexPath indexPathForRow:7 inSection:0];
    cell=(SignupTableCellTableViewCell *)[self.tableview cellForRowAtIndexPath:indexPath];
    
    
    if ([string_IOS isEqualToString:@""]) {
        
        [request addPostValue:cell.textfield.text forKey:@"state"];
    }
    else
    {
        [request addPostValue:string_IOS forKey:@"state"];
    }
    
    
    

    
    
    indexPath = [NSIndexPath indexPathForRow:8 inSection:0];
    cell=(SignupTableCellTableViewCell *)[self.tableview cellForRowAtIndexPath:indexPath];
    [request addPostValue:cell.textfield.text forKey:@"zip"];
    

    
    
    

    
    
 
    
    
    
//    indexPath = [NSIndexPath indexPathForRow:7 inSection:0];
//    cell=(SignupTableCellTableViewCell *)[self.tableview cellForRowAtIndexPath:indexPath];
//    [request addPostValue:cell.textfield.text forKey:@"address"];
//
    
    
    indexPath = [NSIndexPath indexPathForRow:9 inSection:0];
    cell=(SignupTableCellTableViewCell *)[self.tableview cellForRowAtIndexPath:indexPath];
    [request addPostValue:cell.textfield.text  forKey:@"phoneno"];
    
    
    
    indexPath = [NSIndexPath indexPathForRow:10 inSection:0];
    cell=(SignupTableCellTableViewCell *)[self.tableview cellForRowAtIndexPath:indexPath];
    if (cell.textview.text.length>=1) {
        [request addPostValue:cell.textview.text forKey:@"note"];
    }
    
    
    NSData *imageData =UIImagePNGRepresentation(self.profileimage.image);
    
    if (imagedataexixts!=nil) {
        
        
        if (checkimage==true) {
              [request setData:imageData withFileName:@"png" andContentType:@"image/png" forKey:@"image"];
        }
        
        
        
       
        
    }
   
    
    
    if (TARGET_OS_SIMULATOR)
    {
        [request addPostValue:@"112354" forKey:@"device_token"];
    }
    else
    {
        [request addPostValue:APPDELEGATE.devicetoken forKey:@"device_token"];
    }
    
    [request addPostValue:@"I" forKey:@"device_type"];
    [request addPostValue:[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails] valueForKey:@"id"] forKey:@"user_id"];
    
    
    
    
    NSLog(@"%@",request);
    
    
    
    
    [request setDelegate:self];
    [request startAsynchronous];
    
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    if (textView.text.length==1) {
        textView.text = [[[textView.text substringToIndex:1] uppercaseString] stringByAppendingString:[textView.text substringFromIndex:1]];
    }
    return YES;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"SignupTableCellTableViewCell";
    SignupTableCellTableViewCell *cell = (SignupTableCellTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier ];
    if (cell == nil)
    {
        cell =[[SignupTableCellTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier ];
        // cell=[SignupTableCellTableViewCell blankCell];
        
    }
    // contractor=[[NSMutableArray alloc]initWithObjects:@"First Name:",@"Last Name:",@"Company:",@"Email ID:",@"Website:",@"Address:",@"Phone No.:",@"Details:", nil];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    cell.textfield.leftView = paddingView;
    cell.textfield.leftViewMode = UITextFieldViewModeAlways;
    
    cell.label.font=[UIFont systemFontOfSize:12];
    
    cell.label.text=[contractor objectAtIndex:indexPath.row];
    CGRect frame=cell.textfield.frame;
    frame.size.height=cell.textfield.frame.size.height/2;
    cell.textfield.frame=frame;
    
    if (indexPath.row == [contractor count]-1)
    {
        cell.textfield.hidden=TRUE;
        cell.textview.hidden=FALSE;
        cell.textview.delegate=self;
        cell.textview.text=[details objectAtIndex:indexPath.row];

       // string_IOS=[details objectAtIndex:indexPath.row];
    }
    
    
    if (indexPath.row==7) {
        State_List = [[UIPickerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-200, self.view.frame.size.width, 200)];
        State_List.showsSelectionIndicator = YES;
        State_List.delegate = self;
        State_List.backgroundColor = [UIColor colorWithRed:206.0f/255.0f green:210.0f/255.0f blue:215.0f/255.0f alpha:1.0];
        State_List.tintColor = [UIColor blackColor];
        [cell.textfield setInputView:State_List];
        
        UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
        [toolBar setTintColor:[UIColor grayColor]];
        
        UIBarButtonItem *Cancel=[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(ShowSelectedCancel)];
        
        UIBarButtonItem *doneBtn=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(ShowSelectedDate)];
        
        UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        [toolBar setItems:[NSArray arrayWithObjects:Cancel,space,doneBtn, nil]];
        [cell.textfield setInputAccessoryView:toolBar];

    }
    else
    {
        [State_List removeFromSuperview];
        
    }
    
    if(indexPath.row==9)
    {
        cell.textfield.keyboardType = UIKeyboardTypeNumberPad;
    
    }
    
    if (indexPath.row==2) {
      //  cell.textfield.enabled=FALSE;
    }
    if (indexPath.row==3) {
        cell.textfield.enabled=FALSE;
    }
    cell.textfield.delegate=self;
    cell.textfield.tag=indexPath.row;
    cell.textview.tag=indexPath.row;
    cell.textfield.text=[details objectAtIndex:indexPath.row];
    cell.backgroundColor=[UIColor clearColor];
    if (self.cusotmer.selected && indexPath.row==2) {
        
        for (UIView *view in cell.contentView.subviews) {
            [view removeFromSuperview];
        }
    }
    if (self.cusotmer.selected && indexPath.row==4) {
        
        for (UIView *view in cell.contentView.subviews) {
            [view removeFromSuperview];
        }
    }
    if (self.cusotmer.selected && indexPath.row==[contractor count]-1) {
        
        for (UIView *view in cell.contentView.subviews) {
            [view removeFromSuperview];
        }
    }
    
    
    if ([_Msub_role isEqualToString:@"SU"]) {
        
        
        
        if (indexPath.row==2) {
            cell.textfield.enabled=FALSE;
        }

        if (indexPath.row==3) {
            cell.textfield.enabled=FALSE;
        }

        if (indexPath.row==4) {
            cell.textfield.enabled=FALSE;
        }

        if (indexPath.row==5) {
            cell.textfield.enabled=FALSE;
        }
        
        if (indexPath.row==6) {
            cell.textfield.enabled=FALSE;
        }
        if (indexPath.row==7) {
            cell.textfield.enabled=FALSE;
        }
        if (indexPath.row==8) {
            cell.textfield.enabled=FALSE;
        }

        if (indexPath.row==10) {
            cell.textfield.enabled=FALSE;
        }

        cell.textview.userInteractionEnabled=false;
        
        
    }
    
    
    
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    //    if (self.contrator.isSelected) {
    return [contractor count];
    //    }else
    //    {
    //    return [customer count]+2;
    //    }
}



-(void)ShowSelectedCancel
{
    [self.view endEditing:YES];
    
}

-(void)ShowSelectedDate
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:7 inSection:0];
    SignupTableCellTableViewCell *cell=(SignupTableCellTableViewCell *)[self.tableview cellForRowAtIndexPath:indexPath];
    
    
    string_value = [State_List_Array objectAtIndex:string_value_int];
    string_IOS = [State_List_iso objectAtIndex:string_value_int];
    
    cell.textfield.text=[State_List_Array objectAtIndex:string_value_int];
    
    
    [self.view endEditing:YES];
    
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if (textField.tag!=3  && textField.tag!=4) {
        if (textField.text.length==1) {
            textField.text = [[[textField.text substringToIndex:1] uppercaseString] stringByAppendingString:[textField.text substringFromIndex:1]];
        }
    }
    if((textField.text.length ==0 && [string isEqualToString:@" "]) || [[textField.text substringFromIndex:0]isEqualToString:@" "])
    {
        return NO;
    }
    return YES;
    
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField;
{
    if (textField.tag!=3 && textField.tag!=5 && textField.tag!=4) {
        if (textField.text.length==1) {
            textField.text = [[[textField.text substringToIndex:1] uppercaseString] stringByAppendingString:[textField.text substringFromIndex:1]];
        }
    }
    
    NSString *string = textField.text;
    textField.text = [string stringByTrimmingCharactersInSet:
                      [NSCharacterSet whitespaceCharacterSet]];
    
    
    
    return YES;
}
#pragma mark ASIHTTPRequest Delegate (FOR MULTIPART)


- (void)requestFinished:(ASIHTTPRequest *)request
{
    
    [APPDELEGATE removeLoader];
    
    NSString *responseStr = [request responseString];
    SBJsonParser1 *parseData = [[SBJsonParser1 alloc] init];
    NSArray *responseArray = [parseData objectWithString:responseStr];
    
    if ([[responseArray valueForKey:KSucess] isEqualToString:@"1"]) {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:[responseArray valueForKey:@"msg"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        
       
                [[NSUserDefaults standardUserDefaults]setObject:[responseArray valueForKey:KData] forKey:LoginUserDetails];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
//            Profileview *home=[[Profileview alloc]init];
            [self.navigationController popViewControllerAnimated:YES];
    }
    
    
}
- (void)requestFailed:(ASIHTTPRequest *)request
{
    [APPDELEGATE removeLoader];
    NSLog(@"%@",request);
    
    
    
  }
- (IBAction)contractor:(UIButton *)sender {
    
    if (sender==self.contrator) {
        if (self.cusotmer.selected) {
            [self.cusotmer setSelected:FALSE];
            
            
        }
        if (sender.isSelected) {
            
        }
        else
        {
            [sender setSelected:TRUE];
            [self.tableview reloadData];
        }
        
        
    }
    else
    {
        if (self.contrator.selected) {
            [self.contrator setSelected:FALSE];
            
            
        }
        if (sender.isSelected) {
            
        }
        else
        {
            [sender setSelected:TRUE];
            [self.tableview reloadData];
        }
    }
    }
@end
