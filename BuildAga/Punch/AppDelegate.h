//
//  AppDelegate.h
//  Punch
//
//  Created by Jaimish on 30/06/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "Reachability.h"
#import "Webservice.h"
#import "Homeview.h"
#import <FacebookSDK/FacebookSDK.h>



#define APPDELEGATE [AppDelegate sharedDelegate]
@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    
    NSMutableDictionary *dicts;
    
    
    BOOL networkcheck;
}
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) ViewController *view;
@property (strong, nonatomic) UINavigationController *nav;
@property(retain,nonatomic)NSString *devicetoken;
-(void)addLoader:(NSString *)string;
-(void)removeLoader;
+(AppDelegate*)sharedDelegate;
-(BOOL)CheckConnection;
-(void)callWebservice:(id)returnDic andconrtoller:(id)viewController methodName:(NSString *)methodName;
-(NSString *)handleNull:(NSObject *)response;
//- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error;
@end

