//
//  Webservice.h
//  Test
//
//  Created by hyperlink on 03/01/15.
//  Copyright (c) 2015 Hyperlink Infosystem. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol WebserviceDelegate;


@interface Webservice : NSObject
{
    NSMutableData *responseData;
    NSURLConnection *objConnection;
    NSString *strMethod;
}
@property(nonatomic,retain)id<WebserviceDelegate>delegate;
@property (nonatomic, retain) NSString *strMethod;

-(void)webserviceCall:(NSString *)dic methodName:(NSString *)methodName;
@end

@protocol WebserviceDelegate <NSObject>

-(void)webHandlerManager:(Webservice *)webHandlerManager didFinishLoadingWithData:(NSObject *)responseObject;
-(void)webHandlerManager:(Webservice *)webHandlerManager didFailedLoadingWithError:(NSString *)responseError;

@end