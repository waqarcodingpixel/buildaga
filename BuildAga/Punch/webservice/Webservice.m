//
//  Webservice.m
//  Test
//
//  Created by hyperlink on 03/01/15.
//  Copyright (c) 2015 Hyperlink Infosystem. All rights reserved.
//

#import "Webservice.h"
#import "NSObject+SBJson.h"
#import "SBJson.h"
//#import "JSON.h"


@implementation Webservice
@synthesize delegate,strMethod;
-(void)webserviceCall:(NSString *)dic methodName:(NSString *)methodName
{
    responseData = [[NSMutableData alloc]init];
    
    self.strMethod = methodName;
    
    NSString *requestString=[NSString stringWithFormat:@"data=%@",dic];
    
    NSLog(@"%@",requestString);
    
    NSURL *url=[NSURL URLWithString:webURL];
    
    NSLog(@"%@",url);
    
    
    requestString= [requestString stringByReplacingOccurrencesOfString:@"\\\\" withString:@"%5C"];
    
    requestString= [requestString stringByReplacingOccurrencesOfString:@"\\\"" withString:@"%5C%5C%22"];
    
    requestString= [requestString stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
    
      requestString= [requestString stringByReplacingOccurrencesOfString:@"\\n" withString:@"%5C%5Cn"];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    [request setURL:url];
    
    [request setHTTPMethod:@"POST"];
    
    NSData *requestData = [NSData dataWithBytes: [requestString UTF8String] length: [requestString length]];
    [request setHTTPBody:requestData];
    [request setTimeoutInterval:180]; //24/3 for solve signup requst time out
    //[request setTimeoutInterval:120];//14_5
    // dispatch_async(dispatch_get_main_queue(), ^{
    
    objConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
}
#pragma mark ConnectionURL Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [responseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
     NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"%@",responseString);
    [responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    @try {
      
        if ([delegate respondsToSelector:@selector(webHandlerManager:didFailedLoadingWithError:)])
        {
         
            [delegate webHandlerManager:self didFailedLoadingWithError:error.description];
            
            if ([error.description rangeOfString:@"The request timed out."].location == NSNotFound)
            {
                   [APPDELEGATE removeLoader];
//                UIAlertView *alert1=[[UIAlertView alloc]initWithTitle:@"Punch List" message:@"The request timed out" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//             
//                [alert1 show];
            
                
               
            }
            else {
               
            }
            
        }
    
    }
    
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
   
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *retuenDIC = [responseString JSONValue];
     NSLog(@"responce %@",retuenDIC);
    
    if ([delegate respondsToSelector:@selector(webHandlerManager:didFinishLoadingWithData:)])
    {
        [delegate webHandlerManager:self didFinishLoadingWithData:retuenDIC];
    }
    
    responseData = nil;
}

@end
