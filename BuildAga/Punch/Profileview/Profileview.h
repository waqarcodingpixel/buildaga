//
//  Profileview.h
//  Punch
//
//  Created by Jaimish on 09/07/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Profileview : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *details1;
    
    NSString *user_Role;
    
}
@property (strong, nonatomic) IBOutlet UITableView *tableview;
@property (strong, nonatomic) IBOutlet UIImageView *profileimage;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityindicator;

@end
