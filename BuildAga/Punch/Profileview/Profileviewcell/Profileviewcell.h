//
//  Profileviewcell.h
//  Punch
//
//  Created by Jaimish on 09/07/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Profileviewcell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *titlelabels;
@property (strong, nonatomic) IBOutlet UILabel *details;

@end
