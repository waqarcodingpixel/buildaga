//
//  Profileview.m
//  Punch
//
//  Created by Jaimish on 09/07/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import "Profileview.h"
#import "Profileviewcell.h"
#import "SignupviewView.h"
#import "UpdateProfileView.h"
@interface Profileview ()
{
    NSMutableArray *labels,*details;

    UILabel *temp;
}
@end

@implementation Profileview

- (void)viewDidLoad {
//    labels=[[NSMutableArray alloc]initWithObjects:@"Client's Name:",@"Email:",@"Phone No.:",@"Address:",@"About Us:", nil];
    
    
    user_Role=@"";


    if ([[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails]valueForKey:@"role"]isEqualToString:@"CU"])
        labels=[[NSMutableArray alloc]initWithObjects:@"Name:",@"Email:",@"Address:",@"City",@"State",@"Zip",@"Phone No.:", nil];
    else
        labels=[[NSMutableArray alloc]initWithObjects:@"Name:",@"Company Name:",@"Email:",@"Website:",@"Address:",@"City",@"State",@"Zip",@"Phone No.:",@"About Us:", nil];

    
    
    
    //    details=[[NSMutableArray alloc]initWithObjects:@"John Mathew",@"abc@abc.com",@"1234567890",@"Boston",@"Since these are two different types of controls, I would strongly consider having them point to different functions, and then move the code that is common into a different routine that you can call from both of those functions. I like to keep the code segregated so that it is nice and easy to follow ", nil];
    
   details=[[NSMutableArray alloc]initWithObjects:@"",@"",@"",@"",@"",@"",@"",@"",@"",@"", nil];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    
    
    //{"method":"user_profile","user_id":"5"}
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    [dic setObject:@"user_profile" forKey:@"method"];
    [dic setObject:[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails] valueForKey:@"id"] forKey:@"user_id"];
    [APPDELEGATE addLoader:nil];
    [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"user_profile"];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        static NSString *cellIdentifier = @"ProfileTableViewCell";
        
        Profileviewcell *cell = (Profileviewcell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier ];
        if (cell == nil)
        {
            cell =[[Profileviewcell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier ];
            
        }
//    cell.titlelabels.font=[UIFont fontWithName:@"OpenSans" size:12.0];
    cell.titlelabels.text=[labels objectAtIndex:indexPath.row];
    cell.backgroundColor=[UIColor clearColor];
//    cell.details.font=[UIFont fontWithName:@"OpenSans" size:12.0];
    NSLog(@"%@",[details objectAtIndex:indexPath.row ]);
    cell.details.text=[details objectAtIndex:indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
   
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    temp=nil;
    temp=[[UILabel alloc]init];
    temp.font=[UIFont fontWithName:@"OpenSans" size:15.0];
    temp.text=[details objectAtIndex:indexPath.row];

   
        return [self resizeMessageLabel:temp].height+40;
   
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [labels count];
}
- (CGSize) resizeMessageLabel:(UILabel *)lblMessage
{
    CGSize maxLabelSize = CGSizeMake([[UIScreen mainScreen] bounds].size.width-20,9999);
    
    CGSize expectedLabelSize = [lblMessage.text sizeWithFont:lblMessage.font
                                           constrainedToSize:maxLabelSize
                                               lineBreakMode:lblMessage.lineBreakMode];
    expectedLabelSize.height=expectedLabelSize.height+20;
    
    //adjust the label the the new height.
    //    CGRect newFrame = lblMessage.frame;
    //    newFrame.size.height = expectedLabelSize.height;
    //    lblMessage.frame = newFrame;
    //    viewMessage.frame = CGRectMake(viewMessage.frame.origin.x, viewMessage.frame.origin.y, viewMessage.frame.size.width, lblMessage.frame.size.height);
    
    return expectedLabelSize;
}
- (IBAction)back_btn_cllick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)editbtn_click:(id)sender {
  //UpdateProfileView.h
    
    
    NSLog(@"%@",details1);
    
    
    UpdateProfileView *signup=[[UpdateProfileView alloc]init];
    signup.Msub_role=user_Role;
    
    [self.navigationController pushViewController:signup animated:YES];
    
    
}


#pragma mark web service
-(void)webHandlerManager:(Webservice *)webHandlerManager didFinishLoadingWithData:(NSObject *)responseObject
{
    [APPDELEGATE removeLoader];
    if ([webHandlerManager.strMethod isEqualToString:@"user_profile"])
    {
        if([[responseObject valueForKey:KSucess]isEqualToString:@"1"])
        {

            [details removeAllObjects];
            
            if ([[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails]valueForKey:@"role"]isEqualToString:@"CU"])
            {
            
                [details addObject:[NSString stringWithFormat:@"%@ %@",[[responseObject valueForKey:KData]valueForKey:@"name"],[[responseObject valueForKey:KData]valueForKey:@"lastname"]]];
                [details addObject:[[responseObject valueForKey:KData] valueForKey:@"email"]];
                [details addObject:[[responseObject valueForKey:KData] valueForKey:@"address"]];
                 [details addObject:[[responseObject valueForKey:KData] valueForKey:@"city"]];
                [details addObject:[[responseObject valueForKey:KData] valueForKey:@"state"]];
                 [details addObject:[[responseObject valueForKey:KData] valueForKey:@"zip"]];
               
                [details addObject:[[responseObject valueForKey:KData] valueForKey:@"phoneno"]];
            }
            else
            {
                [details addObject:[NSString stringWithFormat:@"%@ %@",[[responseObject valueForKey:KData]valueForKey:@"name"],[[responseObject valueForKey:KData]valueForKey:@"lastname"]]];
                [details addObject:[NSString stringWithFormat:@"%@",[[responseObject valueForKey:KData]valueForKey:@"company_name"]]];
                [details addObject:[[responseObject valueForKey:KData] valueForKey:@"email"]];
                [details addObject:[[responseObject valueForKey:KData] valueForKey:@"website"]];
                [details addObject:[[responseObject valueForKey:KData] valueForKey:@"address"]];
                  [details addObject:[[responseObject valueForKey:KData] valueForKey:@"city"]];
                
                [details addObject:[[responseObject valueForKey:KData] valueForKey:@"state"]];
                   [details addObject:[[responseObject valueForKey:KData] valueForKey:@"zip"]];
             
                [details addObject:[[responseObject valueForKey:KData] valueForKey:@"phoneno"]];
                [details addObject:[[responseObject valueForKey:KData] valueForKey:@"note"]];

            }

            [self.tableview reloadData];
            
           
         details1=[[NSMutableArray alloc]init];
            // contractor=[[NSMutableArray alloc]initWithObjects:@"First Name:",@"Last Name:",@"Company:",@"Email ID:",@"Website:",@"Address:",@"Phone No.:",@"Details:", nil];
            [details1 addObject:[[responseObject valueForKey:KData] valueForKey:@"name"]];
             [details1 addObject:[[responseObject valueForKey:KData] valueForKey:@"lastname"]];
            [details1 addObject:[[responseObject valueForKey:KData] valueForKey:@"company_name"]];
            [details1 addObject:[[responseObject valueForKey:KData] valueForKey:@"email"]];
            [details1 addObject:[[responseObject valueForKey:KData] valueForKey:@"website"]];
            [details1 addObject:[[responseObject valueForKey:KData] valueForKey:@"address"]];
             [details1 addObject:[[responseObject valueForKey:KData] valueForKey:@"city"]];
            
            
             [details1 addObject:[[responseObject valueForKey:KData] valueForKey:@"state"]];
            
            [details1 addObject:[[responseObject valueForKey:KData] valueForKey:@"zip"]];
            
             [details1 addObject:[[responseObject valueForKey:KData] valueForKey:@"phoneno"]];
            [details1 addObject:[[responseObject valueForKey:KData] valueForKey:@"note"]];
            
              user_Role=[[responseObject valueForKey:KData] valueForKey:@"sub_role"];
            
            [[NSUserDefaults standardUserDefaults]setObject:details1 forKey:@"CurrentUser"];
            [[NSUserDefaults standardUserDefaults]synchronize];

         
        }
        else
        {
            
        }
    }
    
    
    
    
    [self.activityindicator startAnimating];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    //
    NSString *url = [NSString stringWithFormat:@"%@%@",UserProfile,[[responseObject valueForKey:KData] valueForKey:@"image"]];
    NSString *filename =[[responseObject valueForKey:KData] valueForKey:@"image"] ;
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:filename];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    if (image==nil) {
        
        NSString *url = [NSString stringWithFormat:@"%@%@",UserProfile,[[responseObject valueForKey:KData] valueForKey:@"image"]];
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            
            NSString *str=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *imagepath = [NSURL URLWithString:str];
            
            NSData *data = [NSData dataWithContentsOfURL:imagepath];
            UIImage *image = [UIImage imageWithData:data];
            [data writeToFile:path atomically:YES];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self.profileimage setImage:image];
                [self.activityindicator stopAnimating];
                [self.activityindicator setHidden:YES];
                
            });
        });
        
    }
    else
    {
        [self.profileimage setImage:image];
        [self.activityindicator stopAnimating];
        [self.activityindicator setHidden:YES];
        
    }
    

    
    
    NSLog(@"Yes :---> %@",user_Role);
    
    
}
-(void)webHandlerManager:(Webservice *)webHandlerManager didFailedLoadingWithError:(NSString *)responseError
{
    
    NSLog(@"%@",responseError);
    NSLog(@"%@",webHandlerManager.strMethod);
    
}
@end
