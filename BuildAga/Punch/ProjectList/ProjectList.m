//
//  ProjectList.m
//  Punch
//
//  Created by Jaimish on 06/07/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import "ProjectList.h"
#import "ProjectDetailsView.h"
#import "Profileviewcell.h"
#import "CreateProject.h"



#import "ASIFormDataRequest.h"
#import "ASIHTTPRequest.h"
#import "SBJsonParser1.h"
#import "ContactDetailCell.h"
#import "SBJsonWriter1.h"
@interface ProjectList ()
{
    NSMutableArray *array,*labels,*details,*running,*past;
    UILabel *temp;
    
}
@end



@implementation ProjectList

- (void)viewDidLoad {
    
//    labels=[[NSMutableArray alloc]initWithObjects:@"Client's Name:",@"Email:",@"Phone No.:",@"Address:",@"About Us:", nil];
    details=[[NSMutableArray alloc]init];
    if ([[[[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedClient"] valueForKey:@"role"] isEqualToString:@"CU"]){
        labels=[[NSMutableArray alloc]initWithObjects:@"Customer:",@"Email:",@"Address:",@"City:",@"State:",@"Zip",@"Phone No.:", nil];
        
        
        
        NSLog(@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedClient"]);
        
        

        [details addObject:[NSString stringWithFormat:@"%@ %@",[[[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedClient"] valueForKey:@"name"],[[[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedClient"] valueForKey:@"lastname"]]];
        
            [details addObject:[[[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedClient"] valueForKey:@"email"]];
            [details addObject:[[[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedClient"] valueForKey:@"address"]];
        
         [details addObject:[[[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedClient"] valueForKey:@"city"]];
         [details addObject:[[[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedClient"] valueForKey:@"state"]];
         [details addObject:[[[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedClient"] valueForKey:@"zip"]];
        
        
        
        [details addObject:[[[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedClient"] valueForKey:@"phoneno"]];
    }
    else
    {
        labels=[[NSMutableArray alloc]initWithObjects:@"Name:",@"Company Name:",@"Email:",@"Website:",@"Address:",@"City:",@"State:",@"Zip",@"Phone No.:",@"About Us:", nil];

        [details addObject:[NSString stringWithFormat:@"%@ %@",[[[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedClient"] valueForKey:@"name"],[[[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedClient"] valueForKey:@"lastname"]]];
        [details addObject:[NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedClient"] valueForKey:@"company_name"]]];
            [details addObject:[[[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedClient"] valueForKey:@"email"]];
        [details addObject:[[[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedClient"] valueForKey:@"website"]];
        [details addObject:[[[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedClient"] valueForKey:@"address"]];
        
        [details addObject:[[[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedClient"] valueForKey:@"city"]];
        [details addObject:[[[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedClient"] valueForKey:@"state"]];
        [details addObject:[[[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedClient"] valueForKey:@"zip"]];
        
        
        [details addObject:[[[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedClient"] valueForKey:@"phoneno"]];
        [details addObject:[[[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedClient"] valueForKey:@"note"]];
    }

   




    self.detailbtn.selected=TRUE;
    self.projectlist.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    array=[[NSMutableArray alloc]initWithObjects:@"RUNNING PROJECTS",@"PAST PROJECTS", nil];
    
    self.clientname.adjustsFontSizeToFitWidth=TRUE;
    self.clientname1.adjustsFontSizeToFitWidth=TRUE;
    
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    //
    NSString *url = [NSString stringWithFormat:@"%@%@",UserProfile,[[[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedClient"] valueForKey:@"image"]];
    NSString *filename =[[[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedClient"] valueForKey:@"image"];
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:filename];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    if (image==nil) {
        
        NSString *url = [NSString stringWithFormat:@"%@%@",UserProfile,[[[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedClient"] valueForKey:@"image"]];
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            
            NSString *str=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *imagepath = [NSURL URLWithString:str];
            
            NSData *data = [NSData dataWithContentsOfURL:imagepath];
            UIImage *image = [UIImage imageWithData:data];
            [data writeToFile:path atomically:YES];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self.profileimage setImage:image];
                
                
            });
        });
        
    }
    else
    {
        [self.profileimage setImage:image];
        
    }
    
    
    
    self.clientname.text=[[[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedClient"] valueForKey:@"name"];
    self.clientname1.text=[[[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedClient"] valueForKey:@"name"];
    
    //{"method":"project_list","user_id":"4","login_user_id":"5"}
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    [dic setObject:@"project_list" forKey:@"method"];
    [dic setObject:[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails] valueForKey:@"id"] forKey:@"login_user_id"];
    [dic setObject:[[[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedClient"] valueForKey:@"id"] forKey:@"user_id"];
    [APPDELEGATE addLoader:nil];
    [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"project_list"];
    
//    if ([[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails]valueForKey:@"role"]isEqualToString:@"CO"]) {
//        
//        
//        self.createprojectbtn.hidden=false;
//        
//        
//        NSLog(@"false : ---->");
//        
//        
//    }
//    else
//    {
//        self.createprojectbtn.hidden=TRUE;
//        
//        
//        NSLog(@"TRUE : ---->");
//        
//    }
    
    
    NSLog(@"User Role : ----> %@",[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails]valueForKey:@"role"]);
    
    
   
}



-(void)CheckStautes
{
    
    
    NSURL *url = [NSURL URLWithString:Count_project];
    //fb_gmail_id=1234&name=riya&email=riya@gmail.com&login_type=F&device_token=Afdrfdrvrf&device_type=A
    [APPDELEGATE addLoader:nil];
    ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
    [request addPostValue:[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails] valueForKey:@"id"]  forKey:@"user_id"];
    //  NSString *joinedComponents = [emailsaddress componentsJoinedByString:@","];
    
    SBJsonWriter1 *writer=[[SBJsonWriter1 alloc]init];
    [[NSUserDefaults standardUserDefaults]setValue:@"1" forKey:@"FirstTime"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [request setDelegate:self];
    [request startAsynchronous];
    
    
}


-(void)requestFinished:(ASIHTTPRequest *)request
{
    
    
    [APPDELEGATE removeLoader];
    NSString *responseStr = [request responseString];
    SBJsonParser1 *parseData = [[SBJsonParser1 alloc] init];
    NSArray *responseArray = [parseData objectWithString:responseStr];
    
    
    if ([[responseArray valueForKey:@"sucess"] isEqualToString:@"0"]) {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Look who's growing!" message:[responseArray valueForKey:@"msg"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        
        
        
    }
    else
    {
        NSMutableDictionary *tempdic=[[NSMutableDictionary alloc]init];
        [tempdic setValue:[[[NSUserDefaults standardUserDefaults]valueForKey:@"SelectedClient"] valueForKey:@"name"] forKey:@"name"];
        [tempdic setValue:[[[NSUserDefaults standardUserDefaults]valueForKey:@"SelectedClient"] valueForKey:@"phoneno"] forKey:@"phoneno"];
        [tempdic setValue:[[[NSUserDefaults standardUserDefaults]valueForKey:@"SelectedClient"] valueForKey:@"lastname"] forKey:@"lastname"];
        [tempdic setValue:[[[NSUserDefaults standardUserDefaults]valueForKey:@"SelectedClient"] valueForKey:@"email"] forKey:@"email"];
        [tempdic setValue:[[[NSUserDefaults standardUserDefaults]valueForKey:@"SelectedClient"] valueForKey:@"address"] forKey:@"address"];
        
         [tempdic setValue:[[[NSUserDefaults standardUserDefaults]valueForKey:@"SelectedClient"] valueForKey:@"city"] forKey:@"city"];
        
         [tempdic setValue:[[[NSUserDefaults standardUserDefaults]valueForKey:@"SelectedClient"] valueForKey:@"state"] forKey:@"state"];
        
        
         [tempdic setValue:[[[NSUserDefaults standardUserDefaults]valueForKey:@"SelectedClient"] valueForKey:@"zip"] forKey:@"zip"];
        
        
        
        NSMutableDictionary *temp1=[[NSMutableDictionary alloc]init];
        [temp1 setObject:tempdic forKey:@"email_data"];
        
        [[NSUserDefaults standardUserDefaults]setObject:temp1 forKey:@"FromProjectList"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        CreateProject *create=[[CreateProject alloc]init];
        [self.navigationController pushViewController:create animated:YES];
        
        
        
    }
    
    
}
- (void)requestFailed:(ASIHTTPRequest *)request
{
    [APPDELEGATE removeLoader];
    NSLog(@"%@",request);
    
    
    
   }







- (IBAction)craeteproject_btn_click:(id)sender {
    

 [self CheckStautes];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView==self.profileview) {
        return nil;
    }
    
    UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 36)];
    
    
    view.backgroundColor=[UIColor clearColor];


    //    UIImageView *image=[[UIImageView alloc]initWithFrame:CGRectMake(0, -6, [[UIScreen mainScreen] bounds].size.width,2)];
    UIImageView *image; /* Hide a seprotor for last cell -6 farame for second header */
    if (section == 0)
    {
        image=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width,2)];
    }
    else
    {
        image=[[UIImageView alloc]initWithFrame:CGRectMake(0, -6, [[UIScreen mainScreen] bounds].size.width,2)];
    }

    
    
    UIImageView *image1=[[UIImageView alloc]initWithFrame:CGRectMake(0, 34, [[UIScreen mainScreen] bounds].size.width,2)];
    image.backgroundColor=[UIColor whiteColor];
    image1.backgroundColor=[UIColor whiteColor];
    
    //    image.image=[UIImage imageNamed:[sectionimage objectAtIndex:section]];
    UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(15, 0, 250, 32)];
    
    label.text=[array objectAtIndex:section];
    label.textColor=[UIColor whiteColor];
    label.font=[UIFont fontWithName:@"OpenSans" size:20.0];
    [view addSubview:image];
    [view addSubview:image1];
    [view addSubview:label];
    
    return  view;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if (tableView==self.profileview) {
        temp=nil;
        temp=[[UILabel alloc]init];
        temp.font=[UIFont fontWithName:@"OpenSans" size:12.0];
        temp.text=[details objectAtIndex:indexPath.row];
        
        
        return [self resizeMessageLabel:temp].height+40;
    }
    return 44.0;
    
}
- (CGSize) resizeMessageLabel:(UILabel *)lblMessage
{
    CGSize maxLabelSize = CGSizeMake([[UIScreen mainScreen] bounds].size.width-20,9999);
    
    CGSize expectedLabelSize = [lblMessage.text sizeWithFont:lblMessage.font
                                           constrainedToSize:maxLabelSize
                                               lineBreakMode:lblMessage.lineBreakMode];
    expectedLabelSize.height=expectedLabelSize.height+20;
    
    //adjust the label the the new height.
    //    CGRect newFrame = lblMessage.frame;
    //    newFrame.size.height = expectedLabelSize.height;
    //    lblMessage.frame = newFrame;
    //    viewMessage.frame = CGRectMake(viewMessage.frame.origin.x, viewMessage.frame.origin.y, viewMessage.frame.size.width, lblMessage.frame.size.height);
    
    return expectedLabelSize;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    
    return [[UIView alloc]initWithFrame:CGRectZero];
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==self.profileview) {
        return [labels count];
    }
    else
    {
        if (section==0) {
            return [running count];
        }
        else if(section==1)
        {return [past count];
            
        }
    }
    return  0;
    
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    
    if (tableView==self.profileview) {
        return 1;
    }
    
    return 2;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==self.profileview) {
        static NSString *cellIdentifier = @"ProfileTableViewCell";
        
        Profileviewcell *cell = (Profileviewcell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier ];
        if (cell == nil)
        {
            cell =[[Profileviewcell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier ];
            
        }
        cell.titlelabels.text=[labels objectAtIndex:indexPath.row];
        
        cell.backgroundColor=[UIColor clearColor];
        cell.details.text=[details objectAtIndex:indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
        
    }
    else
        
    {
        static NSString *cellIdentifier = @"TableViewCell";
        
        UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier ];
        if (cell == nil)
        {
            cell =[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier ];
            
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor=[UIColor clearColor];
        
        switch (indexPath.section) {
                
            case 0:
            {
                cell.textLabel.text=[[running objectAtIndex:indexPath.row] valueForKey:@"title"];
                cell.textLabel.font=[UIFont fontWithName:@"OpenSans" size:15.0];
                cell.textLabel.textColor=[UIColor whiteColor];
//                UIButton *button=[[UIButton alloc]initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width-70, 0,70 , 36)];
//                [button setImage:[UIImage imageNamed:@"next"] forState:UIControlStateNormal];
//                
//                cell.accessoryView = button;
                
                  cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
                
                UIView *seperatorLine = [[UIView alloc] initWithFrame:CGRectMake(15.0f, 49, [UIScreen mainScreen].bounds.size.width - 30.0f, 1.0f)];
                [seperatorLine setBackgroundColor:[UIColor whiteColor]];
                [cell addSubview:seperatorLine];
                
                break;
            }
            case 1:
            {
                
                cell.textLabel.text=[[past objectAtIndex:indexPath.row] valueForKey:@"title"];
                cell.textLabel.font=[UIFont fontWithName:@"OpenSans" size:15.0];
                cell.textLabel.textColor=[UIColor whiteColor];
//                UIButton *button=[[UIButton alloc]initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width-70, 0,70 , 36)];
//                [button setImage:[UIImage imageNamed:@"next"] forState:UIControlStateNormal];
//                
//                cell.accessoryView = button;
                
                
                cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
                UIView *seperatorLine = [[UIView alloc] initWithFrame:CGRectMake(15.0f, 49, [UIScreen mainScreen].bounds.size.width - 30.0f, 1.0f)];
                [seperatorLine setBackgroundColor:[UIColor whiteColor]];
                [cell addSubview:seperatorLine];
                
                break;
            }
        }
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==self.projectlist) {
        
        if (indexPath.section==0) {
            [[NSUserDefaults standardUserDefaults]setObject:[running objectAtIndex:indexPath.row] forKey:@"SelectedProject"];
            [[NSUserDefaults standardUserDefaults]synchronize];

            //Date At : 19/12 Time 3:00 PM
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"FromEditingMode"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
        else if(indexPath.section==1)
        {
            [[NSUserDefaults standardUserDefaults]setObject:[past objectAtIndex:indexPath.row] forKey:@"SelectedProject"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
        }
        
        ProjectDetailsView *projectDetail=[[ProjectDetailsView alloc]init];

        [self.navigationController pushViewController:projectDetail animated:YES];
    }
    
    
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView==self.profileview) {
        return 0.0;
    }
    return 36.0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (tableView==self.profileview) {
        return 0.0;
    }
    return 0.0;
}
- (IBAction)back_btn_click:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)detail_album_click:(UIButton *)sender {
    
    
    
    if (sender==self.album || sender==self.album1) {
        if (self.detailbtn.selected || self.detailbtn1.selected) {
            [self.detailbtn setSelected:FALSE];
            [self.detailbtn1 setSelected:FALSE];
            
            
        }
        if (sender.isSelected) {
            [self.album setSelected:TRUE];
            [self.album1 setSelected:TRUE];
            
            
        }
        else
        {
            [sender setSelected:TRUE];
            [self.album1 setSelected:TRUE];
            self.profile.frame=CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
            
            [self.view addSubview:self.profile];
            
        }
        
        
    }
    else
    {
        if (self.album.selected || self.album1.selected) {
            [self.album setSelected:FALSE];
            [self.album1 setSelected:FALSE];
            
            
            
        }
        if (sender.isSelected) {
            
            [self.detailbtn setSelected:TRUE];
            [self.detailbtn1 setSelected:TRUE];
        }
        else
        {
            [sender setSelected:TRUE];
            [self.detailbtn1 setSelected:TRUE];
            [self.detailbtn setSelected:TRUE];
            [self.profile removeFromSuperview];
            
        }
        
        
        
    }
    
    
    
    
}

#pragma mark web service
-(void)webHandlerManager:(Webservice *)webHandlerManager didFinishLoadingWithData:(NSObject *)responseObject
{
    [APPDELEGATE removeLoader];
    if ([webHandlerManager.strMethod isEqualToString:@"project_list"])
    {
        if([[responseObject valueForKey:@"sucess"]isEqualToString:@"1"])
        {
            running=nil;
            running=[[NSMutableArray alloc]init];
            running=[responseObject valueForKey:@"running_project_data"];
            NSIndexSet *sections = [[NSIndexSet alloc] initWithIndex:0];
            
            [self.projectlist reloadSections:sections withRowAnimation:UITableViewRowAnimationNone];
            
            past=nil;
            past=[[NSMutableArray alloc]init];
            past=[responseObject valueForKey:@"past_project_data"];
            sections=nil;
            
            sections=[[NSIndexSet alloc] initWithIndex:1];
            [self.projectlist reloadSections:sections withRowAnimation:UITableViewRowAnimationNone];
        }
        else
        {
        }
    }
}
-(void)webHandlerManager:(Webservice *)webHandlerManager didFailedLoadingWithError:(NSString *)responseError
{
    
    NSLog(@"%@",responseError);
    NSLog(@"%@",webHandlerManager.strMethod);
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
