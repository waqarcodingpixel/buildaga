//
//  ProjectList.h
//  Punch
//
//  Created by Jaimish on 06/07/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProjectList : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *projectlist;
@property (strong, nonatomic) IBOutlet UITableView *profileview;
@property (strong, nonatomic) IBOutlet UIImageView *profileimage;
@property (strong, nonatomic) IBOutlet UIView *profile;
@property (strong, nonatomic) IBOutlet UIButton *projectbtn;
@property (strong, nonatomic) IBOutlet UIButton *profilebtn;
@property (strong, nonatomic) IBOutlet UIButton *projectbtn1;
@property (strong, nonatomic) IBOutlet UIButton *profilebtn1;
@property (strong, nonatomic) IBOutlet UIButton *detailbtn;
@property (strong, nonatomic) IBOutlet UIButton *album;
@property (strong, nonatomic) IBOutlet UIButton *detailbtn1;
@property (strong, nonatomic) IBOutlet UIButton *album1;
@property (strong, nonatomic) IBOutlet UILabel *clientname;
@property (strong, nonatomic) IBOutlet UILabel *clientname1;

@property (strong,nonatomic) IBOutlet  UIButton *createprojectbtn;
@end
