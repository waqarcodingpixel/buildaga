//
//  CommentsCell.h
//  Punch
//
//  Created by Jaimish on 06/07/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentsCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *comment;
@property (strong, nonatomic) IBOutlet UIImageView *userimage;
@property (strong, nonatomic) IBOutlet UILabel *nameofuser;
@property (strong, nonatomic) IBOutlet UITextView *commet;

@end
