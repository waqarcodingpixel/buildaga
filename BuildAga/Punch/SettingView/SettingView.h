//
//  SettingView.h
//  Punch
//
//  Created by Jaimish on 06/07/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FAQVC.h"

@interface SettingView : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableview;
@property (strong, nonatomic) IBOutlet UIView      *changepasswordview;
@property (strong, nonatomic) IBOutlet UITextField *oldpass;
@property (strong, nonatomic) IBOutlet UITextField *newpass;
@property (strong, nonatomic) IBOutlet UITextField *confirmpass;
@property (strong, nonatomic) IBOutlet UIImageView *bgimage;

@end
