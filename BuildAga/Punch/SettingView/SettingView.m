//
//  SettingView.m
//  Punch
//
//  Created by Jaimish on 06/07/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import "SettingView.h"
#import "Profileview.h"
#import "ViewController.h"
#import "AboutVC.h"
@interface SettingView ()<UIAlertViewDelegate>
{

    NSMutableArray *settingitem,*images;
}

@end

@implementation SettingView

- (void)viewDidLoad {
    
    
//    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails]);
//    
//   NSLog(@"Gmail ID :---->  %@",[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails]valueForKey:@"fb_gmail_id"]);
//    
//    NSLog(@"Facebook ID ----> %@",[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails]valueForKey:@"password"]);
    
    
    
    [settingitem removeAllObjects];
    
    
    
//  if  (![[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails]valueForKey:@"fb_gmail_id"]isEqualToString:@""] && [[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails]valueForKey:@"password"]isEqualToString:@""] )
//    {
//        NSLog(@"Change Password Hide");
//
//        
//       settingitem=[[NSMutableArray alloc]initWithObjects:@"      Profile",@"      Notification",@"      About",@"      FAQ",@"      Logout", nil];
//        images=[[NSMutableArray alloc]initWithObjects:@"profile",@"notification",@"about",@"faq",@"logout",nil];
//        
//       
//
//    }
//    
//    
//    
//    else
//    {
//        
//        NSLog(@"Change Password Show");
    
        
        
       settingitem=[[NSMutableArray alloc]initWithObjects:@"      Profile",@"      Notification",@"      Change Password",@"      About",@"      FAQ",@"      Logout", nil];
        
        images=[[NSMutableArray alloc]initWithObjects:@"profile",@"notification",@"faq",@"about",@"faq",@"logout",nil];
//    }
//    
//    
//      NSLog(@"Value Array %@",settingitem);
//    
    
        self.tableview.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}


-(void)Logout_API
{
               // [APPDELEGATE addLoader:nil];
                NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                [dic setObject:@"user_logout" forKey:@"method"];
    
                [dic setObject:[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails] valueForKey:@"id"] forKey:@"user_id"];
                [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"user_logout"];

}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
{

    if ([alertView tag]==1001) {
        
        if (buttonIndex==0) {
               [self performSelectorInBackground:@selector(Logout_API) withObject:nil];
            
            [[NSUserDefaults standardUserDefaults]setValue:@"0" forKey:KLoginSuccess];
            [[NSUserDefaults standardUserDefaults]synchronize];
            ViewController *profile=[[ViewController alloc]init];
            [self.navigationController pushViewController:profile animated:YES];
            
            
            
         
            
            
            
        }
    }


}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return [settingitem count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"TableViewCell";
    
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier ];
    if (cell == nil)
    {
        cell =[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier ];
        
    }
    cell.backgroundColor=[UIColor clearColor];
        cell.textLabel.font=[UIFont fontWithName:@"OpenSans" size:15.0];
    cell.imageView.image=[UIImage imageNamed:[images objectAtIndex:indexPath.row]];
    cell.textLabel.text=[settingitem objectAtIndex:indexPath.row];
    cell.textLabel.textColor=[UIColor whiteColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.row==1) {
        UISwitch *switchbtn=[[UISwitch alloc]init];
        
        cell.accessoryView=switchbtn;
        switchbtn.on = YES;
    }
    if (indexPath.row==0) {
        cell.accessoryView= [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"next"]];
    }
    cell.backgroundColor=[UIColor clearColor];
    if (![[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails]valueForKey:login_type]isEqualToString:@"S"]) {
        
        if (indexPath.row==2) {
            for (UIView *view in cell.contentView.subviews) {
                [view removeFromSuperview];
                  cell.textLabel.text=@"";
            }
        }
        
    }
    
    return cell;

    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (![[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails]valueForKey:login_type]isEqualToString:@"S"]) {
     
        if (indexPath.row==2) {
            return 0.0;
        }
       
    }
   return 50.0;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0) {
        Profileview *profile=[[Profileview alloc]init];
        [self.navigationController pushViewController:profile animated:YES];
    }
    
    else if(indexPath.row==2)
    {
        
        
        
        
        UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTap:)];
        
        [self.bgimage addGestureRecognizer:tap];
        self.oldpass.text=@"";
        self.newpass.text=@"";
        self.confirmpass.text=@"";
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
        self.oldpass.leftView = paddingView;
        self.oldpass.leftViewMode = UITextFieldViewModeAlways;
        UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
        self.newpass.leftView = paddingView1;
        self.newpass.leftViewMode = UITextFieldViewModeAlways;
        UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
        self.confirmpass.leftView = paddingView2;
        self.confirmpass.leftViewMode = UITextFieldViewModeAlways;
        self.changepasswordview.frame=CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width,  [[UIScreen mainScreen] bounds].size.height);
        [self.view addSubview:self.changepasswordview];
       // }
        
    
    }
    else if (indexPath.row==3)
    {
        
           AboutVC *obj = [[AboutVC alloc]initWithNibName:@"AboutVC" bundle:nil];
            [self.navigationController pushViewController:obj animated:YES];
    
        
      
    }
    else if (indexPath.row==4)
    {
       

        
            FAQVC *obj = [[FAQVC alloc]initWithNibName:@"FAQVC" bundle:nil];
            [self.navigationController pushViewController:obj animated:YES];
    
        
        
       
    }
    else if(indexPath.row==5)
    {
        //{"method":"user_logout","user_id":"1"}are you sure yo want to logout?
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:@"Are you sure want to logout?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
        alert.tag=1001;
        [alert show];
       
      
        
    }
}
-(void)handleTap:(UITapGestureRecognizer *)getsture
{
    if([self.changepasswordview superview])
        [self.changepasswordview removeFromSuperview];
    
}
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)back_btn_click:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
   
    
    
    
}

- (IBAction)changepassowrd_click:(id)sender {

    
    
    
    if ([self.oldpass.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0 ){
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:@"Please enter old password" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    
        
        
    }
    if ([self.newpass.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 5)
    {
        if (self.newpass.text.length==0) {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:@"Please enter password" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            return;
        }
       
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:@"Your password must be at least 6 characters" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
        /* if ([self validateEmail:self.txt_email.text]) {
         
         }*/
    }
    if ([self.confirmpass.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0) {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:@"Please enter confirm password" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
        
    }
    if (![self.confirmpass.text isEqualToString:self.newpass.text])
    {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:@"Passwords do not match" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }//{"method":"change_password","user_id":"1","old_password":"123456","new_password":"12345"}
    
    [self.changepasswordview endEditing:YES];
    [APPDELEGATE addLoader:nil];
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    [dic setObject:@"change_password" forKey:@"method"];
    [dic setObject:self.oldpass.text forKey:@"old_password"];
    [dic setObject:self.newpass.text forKey:@"new_password"];
   
    [dic setObject:[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails] valueForKey:@"id"] forKey:@"user_id"];
    [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"change_password"];
    
}
#pragma mark web service
-(void)webHandlerManager:(Webservice *)webHandlerManager didFinishLoadingWithData:(NSObject *)responseObject
{
    
    NSLog(@"Yes Log OUT");
    
    
    
    
    [APPDELEGATE removeLoader];
    if ([webHandlerManager.strMethod isEqualToString:@"user_logout"])
    {
        if([[responseObject valueForKey:@"sucess"]isEqualToString:@"1"])
        {
            
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"personal_logo"];
            [[NSUserDefaults standardUserDefaults] synchronize];

//            [[NSUserDefaults standardUserDefaults]setValue:@"0" forKey:KLoginSuccess];
//            [[NSUserDefaults standardUserDefaults]synchronize];
//            ViewController *profile=[[ViewController alloc]init];
//            [self.navigationController pushViewController:profile animated:YES];
        }
        else
        {
           
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:[responseObject valueForKey:@"msg"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            
        }
    }
    if ([webHandlerManager.strMethod isEqualToString:@"change_password"])
    {
        if([[responseObject valueForKey:@"sucess"]isEqualToString:@"1"])
        {
            [self.changepasswordview removeFromSuperview];
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:[responseObject valueForKey:@"msg"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
        else
        {
//             [self.changepasswordview removeFromSuperview];
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:[responseObject valueForKey:@"msg"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            
        }
    }
}
-(void)webHandlerManager:(Webservice *)webHandlerManager didFailedLoadingWithError:(NSString *)responseError
{
    
    NSLog(@"%@",responseError);
    NSLog(@"%@",webHandlerManager.strMethod);
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
