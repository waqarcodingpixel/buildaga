//
//  AboutVC.h
//  Punch
//
//  Created by Pratik on 12/16/15.
//  Copyright © 2015 Hyperlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutVC : UIViewController
{
    IBOutlet UIButton       *btnBack;
    IBOutlet UITextView     *txtAbout;
}
-(IBAction)btnBackClicked:(id)sender;

@end
