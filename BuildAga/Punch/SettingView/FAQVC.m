//
//  FAQVC.m
//  Punch
//
//  Created by Pratik on 11/27/15.
//  Copyright © 2015 Hyperlink. All rights reserved.
//

#import "FAQVC.h"

@interface FAQVC ()

@end

@implementation FAQVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self WebViewCall];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)btnBackClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)WebViewCall
{
    
    webView.scrollView.showsHorizontalScrollIndicator = NO;
    webView.scrollView.showsVerticalScrollIndicator = NO;

    NSURL *url = [NSURL URLWithString:@"https://buildaga.com/faq/"];
    
    //URL Requst Object
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    
    //Load the request in the UIWebView.
    [webView loadRequest:requestObj];
    
    
    
}
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [APPDELEGATE addLoader:nil];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [APPDELEGATE removeLoader];
    
}
@end
