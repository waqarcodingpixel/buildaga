//
//  FAQVC.h
//  Punch
//
//  Created by Pratik on 11/27/15.
//  Copyright © 2015 Hyperlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FAQVC : UIViewController
{
    IBOutlet UIButton    *btnBack;
    IBOutlet UIWebView   *webView;
}
-(IBAction)btnBackClicked:(id)sender;
@end
