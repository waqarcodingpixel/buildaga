//
//  PunchListCell.h
//  Punch
//
//  Created by Jaimish on 09/07/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PunchListCellDelegate <NSObject>

- (void)deleteTaskButtonTapped:(UIButton *)sender;

@end

@interface PunchListCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIButton *commentbtn;
@property (strong, nonatomic) IBOutlet UIButton *tickbtn;
@property (strong, nonatomic) IBOutlet UILabel *taskname;
@property (strong, nonatomic) IBOutlet UILabel *taskdetails;
@property (strong, nonatomic) IBOutlet UIButton *edittask,*deletetask;
@property (strong, nonatomic) IBOutlet UIButton *counterbtn;

@property (weak, nonatomic) id<PunchListCellDelegate> delegate;

@end
