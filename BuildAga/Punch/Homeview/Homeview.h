//
//  Homeview.h
//  Punch
//
//  Created by Jaimish on 01/07/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommentsView.h"


@interface Homeview : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextViewDelegate,UITextFieldDelegate>
{
   IBOutlet UIImageView *Mlogo_Image;
    
}
@property (strong, nonatomic) IBOutlet UIView *sectionheaderview;
@property (strong, nonatomic) IBOutlet UIImageView *sectionheaderimage;
@property (strong, nonatomic) IBOutlet UILabel *sectiontitle;
@property (strong, nonatomic) IBOutlet UIButton *messagecounter;
@property (strong, nonatomic) IBOutlet UITableView *tableview;
@property (strong, nonatomic) IBOutlet CommentsView  *commentsview1;

@property (strong, nonatomic) IBOutlet UIButton *createprojectbtn;


@end
