//
//  Homeview.m
//  Punch
//
//  Created by Jaimish on 01/07/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import "Homeview.h"
#import "HomeviewCell.h"
#import "ListMessageviewViewController.h"
#import "CreateProject.h"
#import "SettingView.h"
#import "CustomerView.h"
#import "CommentsCell.h"
#import "ProjectList.h"
#import "ToolSheldViewContrller.h"

#import "ASIFormDataRequest.h"
#import "ASIHTTPRequest.h"
#import "SBJsonParser1.h"
#import "ContactDetailCell.h"
#import "SBJsonWriter1.h"


@interface Homeview ()
{
    int i;
    NSArray *sectionimage,*titlesection;
    NSMutableArray *commnets;
    UILabel *temp;
    CGFloat keyboard_height;
    NSMutableArray *list,*companylist,*projectlist,*tempprojectlist;
    CGRect chatview,textview,commenttableview;
    UIButton *counter;
    NSMutableArray *tempobje;
    
    NSString *toolshedurl;
    
}
@end

@implementation Homeview

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    
    if (![[[NSUserDefaults standardUserDefaults]valueForKey:@"logo_image"]isEqualToString:@""])
        
        
    {
        
        
        NSString *url = [NSString stringWithFormat:@"%@%@",UserProfile,[[NSUserDefaults standardUserDefaults]valueForKey:@"logo_image"]];
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            
            NSString *str=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *imagepath = [NSURL URLWithString:str];
            
            NSData *data = [NSData dataWithContentsOfURL:imagepath];
            UIImage *image = [UIImage imageWithData:data];
            // [data writeToFile:paths atomically:YES];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [Mlogo_Image setImage:image];
                
                
            });
        });
        
        
        
        
    }
    else
    {
        // Mlogo_Image.backgroundColor=[UIColor yellowColor];
    }
    
    
    
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardShow:) name:UIKeyboardWillChangeFrameNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardHide:) name:UIKeyboardDidHideNotification object:nil];
    commnets=[[NSMutableArray alloc]init];
    projectlist=[[NSMutableArray alloc]init];
    
    counter=[[UIButton alloc]initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width-36, 5,26 ,26)];
    counter.hidden=TRUE;
    
    
    
    //    if ([[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails]valueForKey:@"role"]isEqualToString:@"CU"]) {
    //      //{"method":"company_list","user_id":"4"}
    //        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    //        [dic setObject:@"company_list" forKey:@"method"];
    //        [dic setObject:[[[NSUserDefaults standardUserDefaults] valueForKey:LoginUserDetails] valueForKey:@"id"] forKey:@"user_id"];
    //        [APPDELEGATE addLoader:nil];
    //        [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"company_list"];
    //          titlesection=[[NSArray alloc]initWithObjects:@"MESSAGES",@"TOOL SHED",@"COMPANY", nil];
    //    }
    //    else
    //    {//{"method":"customer_list","user_id":"5"}
    //        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    //        [dic setObject:@"customer_list" forKey:@"method"];
    //        [dic setObject:[[[NSUserDefaults standardUserDefaults] valueForKey:LoginUserDetails] valueForKey:@"id"] forKey:@"user_id"];
    //        [APPDELEGATE addLoader:nil];
    //        [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"customer_list"];
    //      titlesection=[[NSArray alloc]initWithObjects:@"MESSAGES",@"TOOL SHED",@"CUSTOMERS", nil];
    //    }
    sectionimage=[[NSArray alloc]initWithObjects:@"message",@"customers", nil];
    
    self.tableview.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    // Do any additional setup after loading the view from its nib.
    
    
    //    //Date 27/11 Time 7:00
    //    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    //    [dic setObject:@"toolshed_list" forKey:@"method"];
    //    [dic setObject:[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails]valueForKey:@"role"] forKey:@"user_type"];
    //    [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"toolshed_list"];
}
-(void)viewWillAppear:(BOOL)animated
{
    
    if ([[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails]valueForKey:@"role"]isEqualToString:@"CU"]) {
        
        //{"method":"company_list","user_id":"4"}
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        [dic setObject:@"company_list" forKey:@"method"];
        [dic setObject:[[[NSUserDefaults standardUserDefaults] valueForKey:LoginUserDetails] valueForKey:@"id"] forKey:@"user_id"];
        [dic setObject:@"0" forKey:@"is_all_data"];
        [APPDELEGATE addLoader:nil];
        [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"company_list"];
        titlesection=[[NSArray alloc]initWithObjects:@"MESSAGES",@"COMPANY", nil];
        
    }
    else
    {//{"method":"customer_list","user_id":"5"}
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        [dic setObject:@"customer_list" forKey:@"method"];
        [dic setObject:[[[NSUserDefaults standardUserDefaults] valueForKey:LoginUserDetails] valueForKey:@"id"] forKey:@"user_id"];
        [dic setObject:@"0" forKey:@"is_all_data"];
        [APPDELEGATE addLoader:nil];
        [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"customer_list"];
        titlesection=[[NSArray alloc]initWithObjects:@"MESSAGES",@"CUSTOMERS", nil];
    }
    
    i=0;
    // [self tableFooterview];
    
    
        if ([[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails]valueForKey:@"role"]isEqualToString:@"CO"]) {
    
    
            self.createprojectbtn.hidden=false;
    
    
        }
        else
        {
            self.createprojectbtn.hidden=TRUE;
    
    
    
        }
    
    
    
    
    
    NSLog(@"User Role : ----> %@",[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails]valueForKey:@"role"]);
    
    
    
}
-(void)tableFooterview
{
    
    UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 36)];
    view.backgroundColor=[UIColor clearColor];
    UIButton *button=[[UIButton alloc]initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width-70, 0,70 , 36)];
    
    NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
    
    NSMutableAttributedString *str=[[NSMutableAttributedString alloc] initWithString:@"view all"
                                                                          attributes:underlineAttribute];
    
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, [@"view all" length])];
    button.tag=10002;
    [button addTarget:self action:@selector(viewAll:) forControlEvents:UIControlEventTouchUpInside];
    [button setAttributedTitle:str  forState:UIControlStateNormal];
    [button setFont:[UIFont fontWithName:@"OpenSans" size:17.0]];
    // cell1.separatorInset = UIEdgeInsetsMake(0.f, cell1.bounds.size.width, 0.f, 0.f);
    
    self.tableview.tableFooterView=nil;
    if([list count ]>0)
    {
        [view addSubview:button];
        self.tableview.tableFooterView=view;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==self.commentsview1.commenttable) {
        return [commnets count];
    }
    else
    {
        switch (section) {
            case 0:
                if ([projectlist count]>2) {
                    return  1;
                }
                else
                {
                    return  [projectlist count];
                    
                    
                }
                break;
                //        case 1:
                //            return  1;
                //            break;
            case 1:
                /*            if ([list count]>3) {
                 return  3;
                 }
                 else
                 {
                 return  [list count];
                 }
                 */
                return  [list count];
                break;
            default:
                break;
        }
        
        return 2;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView==self.commentsview1.commenttable) {
        return 1;
    }
    
    return 2;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==self.commentsview1.commenttable) {
        
        static NSString *cellIdentifier = @"CommentTableViewCell";
        
        CommentsCell *cell = (CommentsCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier ];
        if (cell == nil)
        {
            cell =[[CommentsCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier ];
            
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.comment.font=[UIFont fontWithName:@"OpenSans" size:12.0];
        //        cell.backgroundColor=[UIColor clearColor];
        //        cell.textLabel.text=[NSString stringWithFormat:@"I have to concede. There is now way using"];
        
        
        
        cell.commet.text=[[commnets valueForKey:@"comment"]objectAtIndex:indexPath.row];
        
        
        
        
        
        
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                             NSUserDomainMask, YES);
        //
        NSString *url = [NSString stringWithFormat:@"%@%@",UserProfileThumbs,[[commnets valueForKey:@"image"] objectAtIndex:indexPath.row]];
        NSString *filename =[NSString stringWithFormat:@"Profile_%@",[[commnets valueForKey:@"image"] objectAtIndex:indexPath.row] ];
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:filename];
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        if (image==nil) {
            
            NSString *url = [NSString stringWithFormat:@"%@%@",UserProfileThumbs,[[commnets valueForKey:@"image"] objectAtIndex:indexPath.row]];
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                
                NSString *str=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                NSURL *imagepath = [NSURL URLWithString:str];
                
                NSData *data = [NSData dataWithContentsOfURL:imagepath];
                UIImage *image = [UIImage imageWithData:data];
                [data writeToFile:path atomically:YES];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [cell.userimage setImage:image];
                    //                    [self.activityindicator stopAnimating];
                    //                    [self.activityindicator setHidden:YES];
                    
                });
            });
            
        }
        else
        {
            [cell.userimage setImage:image];
            //            [self.activityindicator stopAnimating];
            //            [self.activityindicator setHidden:YES];
            
        }
        
        //cell.userimage.image=[UIImage imageNamed:@"1.png"];
        cell.nameofuser.text=[[commnets valueForKey:@"name"] objectAtIndex:indexPath.row];
        //        cell.textLabel.textColor=[UIColor whiteColor];
        //       UIButton *button=[[UIButton alloc]initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width-70, 0,70 , 36)];
        //       [button setImage:[UIImage imageNamed:@"round_comment"] forState:UIControlStateNormal];
        
        //       cell.accessoryView=button;
        
        return cell;
    }
    else
    {
        
        static NSString *cellIdentifier = @"TableViewCell";
        
        UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier ];
        if (cell == nil)
        {
            cell =[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier ];
            
        }
        
        cell.backgroundColor=[UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        switch (indexPath.section) {
            case 0:
            {
                cell.textLabel.text=[NSString stringWithFormat:@"%@ - %@",[[projectlist objectAtIndex:indexPath.row] valueForKey:@"project_name"],[[projectlist objectAtIndex:indexPath.row] valueForKey:@"task_title"]];
                cell.textLabel.font=[UIFont fontWithName:@"OpenSans" size:15.0];
                cell.textLabel.textColor=[UIColor whiteColor];
                UIButton *button=[[UIButton alloc]initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width-35, 12,25, 25)];
                [button setTitle:[NSString stringWithFormat:@"%d",[[[projectlist objectAtIndex:indexPath.row] valueForKey:@"count"]intValue]] forState:UIControlStateNormal];
                [button setBackgroundImage:[UIImage imageNamed:@"round_blue_message"] forState:UIControlStateNormal];
                
                [cell.contentView addSubview:button];
                
                
                //line seperator
                
                UIView *seperatorLine = [[UIView alloc] initWithFrame:CGRectMake(20.0f, 49, [UIScreen mainScreen].bounds.size.width - 40.0f, 1.0f)];
                [seperatorLine setBackgroundColor:[UIColor whiteColor]];
                [cell addSubview:seperatorLine];
                
                return cell;
                break;
            }
                //            case 1:
                //            {
                //                //            cell.textLabel.text=[NSString stringWithFormat:@"I have to concede. There is now way using NSString to achieve what you want, unless someone else has more to offer. I am going to have to suggest like the others before me to use"];
                //                //
                //                //            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 15,60 , 60)];
                //                //            imageView.image=[UIImage imageNamed:@"1.png"];
                //                //
                //                //            cell.textLabel.numberOfLines=0;
                //                //            cell.textLabel.textColor=[UIColor whiteColor];
                //                //
                //
                //                UIButton *button=[[UIButton alloc]initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width-35, 30, 35, 51)];
                //                [button setImage:[UIImage imageNamed:@"next"] forState:UIControlStateNormal];
                //
                //                [cell.contentView addSubview:button];
                //                self.sectionheaderview.frame=CGRectMake(0,0,[[UIScreen mainScreen]bounds].size.width , 136);
                //
                //                [cell.contentView addSubview:self.sectionheaderview];
                //              //  self.sectionheaderimage.image=[UIImage imageNamed:@"1.png"];
                //                cell.separatorInset = UIEdgeInsetsMake(0.f, 10000.f, 0.f, cell.bounds.size.width);
                //                cell.backgroundColor = [UIColor clearColor];
                //                   return cell;
                //                break;
                //            }
            case 1:
            {
                static NSString *cellIdentifier = @"HomeTableViewCell";
                HomeviewCell *cell1 = (HomeviewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier ];
                if (cell1 == nil)
                {
                    cell1 =[[HomeviewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier ];
                    
                    
                }
                else
                    
                {
                    cell1.imageView.image=nil;
                }
                cell1.selectionStyle = UITableViewCellSelectionStyleNone;
                UIButton *button=[[UIButton alloc]initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width-35, 18,35 , 36)];
                [button setImage:[UIImage imageNamed:@"next"] forState:UIControlStateNormal];
                button.userInteractionEnabled=FALSE;
                // cell.accessoryView=button;
                
                
                [cell1.contentView addSubview:button];
                cell1.backgroundColor=[UIColor clearColor];
                
                if ([[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails]valueForKey:@"role"]isEqualToString:@"CU"]) {
                    
                    cell1.name.text=[[list objectAtIndex:indexPath.row] valueForKey:@"company_name"];
                }
                else{
                    
                    cell1.name.text=[NSString stringWithFormat:@"%@; %@",[[list objectAtIndex:indexPath.row] valueForKey:@"lastname"],[[list objectAtIndex:indexPath.row] valueForKey:@"name"]];
                }
                
                cell1.imageview.image=[UIImage imageNamed:@"pic_signup"];
                
                
                //                cell1.contact.text=[[list objectAtIndex:indexPath.row] valueForKey:@"phoneno"];
                //                UIFont *font1 = [UIFont fontWithName:@"OpenSans" size:15.0f];
                //                UIColor *color = [UIColor whiteColor];
                //                NSDictionary *dict1 = @{NSUnderlineStyleAttributeName:@(NSUnderlineStyleSingle),
                //                                        NSFontAttributeName:font1,NSForegroundColorAttributeName:color};
                //                NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] init];
                //                [attString appendAttributedString:[[NSAttributedString alloc] initWithString:[[list objectAtIndex:indexPath.row] valueForKey:@"phoneno"]   attributes:dict1]];
                
                // [cell1.contactbtn setAttributedTitle:attString forState:UIControlStateNormal];
                
                [cell1.display setTitle:[[list objectAtIndex:indexPath.row] valueForKey:@"phoneno"] forState:UIControlStateNormal];
                cell1.contactbtn.tag=indexPath.row;
                
                [cell1.contactbtn addTarget:self action:@selector(makecall:) forControlEvents:UIControlEventTouchUpInside];
                
                [cell1.address setTitle:[[list objectAtIndex:indexPath.row] valueForKey:@"address"] forState:UIControlStateDisabled];
                
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                     NSUserDomainMask, YES);
                //
                NSString *url = [NSString stringWithFormat:@"%@%@",UserProfile,[[list objectAtIndex:indexPath.row] valueForKey:@"image"]];
                NSString *filename =[[list objectAtIndex:indexPath.row] valueForKey:@"image"] ;
                NSString *documentsDirectory = [paths objectAtIndex:0];
                NSString* path = [documentsDirectory stringByAppendingPathComponent:filename];
                UIImage* image = [UIImage imageWithContentsOfFile:path];
                if (image==nil) {
                    
                    NSString *url = [NSString stringWithFormat:@"%@%@",UserProfile,[[list objectAtIndex:indexPath.row] valueForKey:@"image"]];
                    NSLog(@"image url is%@",url);

                    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                        
                        NSString *str=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                        NSURL *imagepath = [NSURL URLWithString:str];
                        
                        NSData *data = [NSData dataWithContentsOfURL:imagepath];
                        UIImage *image = [UIImage imageWithData:data];
                        [data writeToFile:path atomically:YES];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [cell1.imageview setImage:image];
                            
                            
                        });
                    });
                    
                }
                else
                {
                    [cell1.imageview setImage:image];
                    
                }
                
                
                
                
                
                
                
                //            if (indexPath.row==3) {
                //                for (UIView *view in cell1.contentView.subviews) {
                //                    [view removeFromSuperview];
                //                    UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 36)];
                //                    view.backgroundColor=[UIColor clearColor];
                //                    UIButton *button=[[UIButton alloc]initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width-70, 0,70 , 36)];
                //
                //                    NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
                //
                //                    NSMutableAttributedString *str=[[NSMutableAttributedString alloc] initWithString:@"view all"
                //                                                                                          attributes:underlineAttribute];
                //
                //                    [str addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, [@"view all" length])];
                //                    button.tag=1000+indexPath.section;
                //                    [button addTarget:self action:@selector(viewAll:) forControlEvents:UIControlEventTouchUpInside];
                //                    [button setAttributedTitle:str  forState:UIControlStateNormal];
                //                    [button setFont:[UIFont fontWithName:@"OpenSans" size:12.0]];
                //                   // cell1.separatorInset = UIEdgeInsetsMake(0.f, cell1.bounds.size.width, 0.f, 0.f);
                //
                //                    [view addSubview:button];
                //                     [cell1.contentView addSubview:view];
                //                }
                //
                //            }
                
                UIView *seperatorLine = [[UIView alloc] initWithFrame:CGRectMake(20.0f, 88, [UIScreen mainScreen].bounds.size.width - 40.0f, 1.0f)];
                [seperatorLine setBackgroundColor:[UIColor whiteColor]];
                [cell1 addSubview:seperatorLine];
                
                return cell1;
                
                
                break;
            }
            default:
                break;
        }
        
        
        //    self.sectionheaderimage.image=[UIImage imageNamed:[sectionimage objectAtIndex:indexPath.section]];
        //    self.sectiontitle.text=[titlesection objectAtIndex:indexPath.section];
        
        return cell;
    }
    
}
-(void)makecall:(UIButton *)sender
{
    NSString *phoneNumber = [@"telprompt://" stringByAppendingString:[[list objectAtIndex:[sender tag]]valueForKey:@"phoneno"]];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    
    UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 36)];
    view.backgroundColor=[UIColor blackColor];
    
    UIButton *image=[[UIButton alloc]initWithFrame:CGRectMake(24, 5, 25, 25)];
    image.userInteractionEnabled=FALSE;
    [image setImage:[UIImage imageNamed:[sectionimage objectAtIndex:section]] forState:UIControlStateNormal];
    // image.image=[UIImage imageNamed:[sectionimage objectAtIndex:section]];
    //    UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(83, 0, 172, 36)];
    //
    //    label.text=[titlesection objectAtIndex:section];
    //    label.textColor=[UIColor whiteColor];
    //    label.font=[UIFont fontWithName:@"OpenSans" size:17.0];
    UIButton *btn1 = [[UIButton alloc]initWithFrame:CGRectMake(image.frame.origin.x + image.frame.size.width + 15, 0, 100, 36)];
    [btn1 setTitle:[titlesection objectAtIndex:section] forState:UIControlStateNormal];
    [btn1 setFont:[UIFont fontWithName:@"OpenSans" size:17.0]];
    [btn1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn1.tag=1000+section;
    [btn1 addTarget:self action:@selector(viewAll:) forControlEvents:UIControlEventTouchUpInside];
    btn1.titleLabel.textAlignment=NSTextAlignmentLeft;
    
    
    //  btn1.backgroundColor=[UIColor blueColor];
    //  btn1.tag=section;
    counter.titleLabel.adjustsFontSizeToFitWidth=TRUE;
    [counter setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];;
    [counter setBackgroundImage:[UIImage imageNamed:@"round_white_message"] forState:UIControlStateNormal];
    
    UILabel *lblCounter = [[UILabel alloc]initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width - 50, 0, 50, 36)];
    [lblCounter setFont:[UIFont fontWithName:@"OpenSans" size:17.0]];
    [lblCounter setTextColor:[UIColor whiteColor]];
    
    
    if (section==0) {
        [view addSubview:counter];
    }
    if (section==1) {
        [lblCounter setText:[NSString stringWithFormat:@"%ld",list.count]];
        [view addSubview:lblCounter];
    }
    [view addSubview:image];
    [view addSubview:btn1];
    
    return  view;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (section!=2) {
        UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 36)];
        view.backgroundColor=[UIColor clearColor];
        UIButton *button=[[UIButton alloc]initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width-70, 0,70 , 36)];
        
        NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
        
        NSMutableAttributedString *str=[[NSMutableAttributedString alloc] initWithString:@"view all"
                                                                              attributes:underlineAttribute];
        
        [str addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, [@"view all" length])];
        button.tag=1000+section;
        [button addTarget:self action:@selector(viewAll:) forControlEvents:UIControlEventTouchUpInside];
        [button setAttributedTitle:str  forState:UIControlStateNormal];
        [button setFont:[UIFont fontWithName:@"OpenSans" size:17.0]];
        
        //        if (section==0 && [projectlist count]>0) {
        if (section==0) {
            [view addSubview:button];
        }
        //        if(section==1)
        //        {
        //        [view addSubview:button];
        //        }
        return view;
    }
    
    return nil;
}
//- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    CGFloat sectionHeaderHeight = 40;
//    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
//        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
//    } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
//        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
//    }
//}
-(void)viewAll:(UIButton *)sender
{
    
    if (sender.tag==1000) {
        ListMessageviewViewController *listmessage=[[ListMessageviewViewController alloc] init];
        
        [self.navigationController pushViewController:listmessage animated:YES];
        
    }
    //    else if (sender.tag==1001)
    //    {
    //        ToolSheldViewContrller *toolshed=[[ToolSheldViewContrller alloc]init];
    //        [self.navigationController pushViewController:toolshed animated:YES];
    //
    //    }
    else
    {
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"FromEditingMode"];
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"EditArray"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        CustomerView *customer=[[CustomerView alloc] init];
        
        [self.navigationController pushViewController:customer animated:YES];
        
    }
}
//
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if (tableView==self.commentsview1.commenttable) {
        
        // NSIndexPath *indexPath = [NSIndexPath indexPathForRow:indexPath inSection:0];
        
        //        CommentsCell *cell=(CommentsCell *)[self.commentsview1.commenttable cellForRowAtIndexPath:indexPath];
        temp=nil;
        temp=[[UILabel alloc]init];
        temp.font=[UIFont fontWithName:@"OpenSans" size:13.0];
        temp.text=[[commnets valueForKey:@"comment"] objectAtIndex:indexPath.row];
        
        if ([self resizeMessageLabel:temp].height < 80) {
            return 90;
        }
        else
        {
            return [self resizeMessageLabel:temp].height;
        }
    }
    else
    {
        switch (indexPath.section) {
            case 0:
                return  50;
                break;
                //            case 1:
                //                return  155;
                //                break;
            case 1:
                return  94;
                break;
            default:
                break;
        }
    }
    return 0.0f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView==self.commentsview1.commenttable) {
        
        return 1.0f;
    }
    else
    {
        return 36.0f;
    }
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    
    if (tableView==self.commentsview1.commenttable) {
        
        return 1.0f;
    }
    else
    {
        if (section==0 && [projectlist count]==0) {
            
            return 36.0f;
        }
        else if (section!=2) {
            return 36.0;
        }
        
        
    }
    return 0.0f;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0) {
        
        i=0;
        
        self.commentsview1=[[CommentsView alloc]initWithFrame:self.view.frame];
        [self.view addSubview:self.commentsview1];
        
        self.commentsview1.chattextview.delegate=self;
        [ self.commentsview1.sendbutton addTarget:self action:@selector(send:) forControlEvents:UIControlEventTouchUpInside];
        self.commentsview1.detailview.frame=CGRectMake(0, 0, self.commentsview1.detailview.frame.size.width, 130+[self resizeMessageLabel:self.commentsview1.detailTask].height);
        self.commentsview1.commenttable.tableHeaderView=self.commentsview1.detailview;
        self.commentsview1.commenttable.delegate=self;
        self.commentsview1.commenttable.dataSource=self;
        [self performSelector:@selector(maindelay) withObject:self afterDelay:1];
        [ self.commentsview1.closebtn addTarget:self action:@selector(close_btn_click:) forControlEvents:UIControlEventTouchUpInside];
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        
        tempobje=[[NSMutableArray alloc]init];
        
        [tempobje addObject:[projectlist objectAtIndex:indexPath.row]];
        [dic setObject:@"comment_list" forKey:@"method"];
        [dic setObject:[[[NSUserDefaults standardUserDefaults] valueForKey:LoginUserDetails] valueForKey:@"id"] forKey:@"user_id"];
        [dic setObject:[[projectlist objectAtIndex:indexPath.row] valueForKey:@"task_id"] forKey:@"task_id"];
        [APPDELEGATE addLoader:nil];
        [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"comment_list"];
        
    }
    //    if (indexPath.section==1) {
    //        NSURL *url = [NSURL URLWithString:toolshedurl];
    //        [[UIApplication sharedApplication] openURL:url];
    //    }
    if (indexPath.section==1) {
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"FromEditingMode"];
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"EditArray"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [[NSUserDefaults standardUserDefaults]setValue:[list objectAtIndex:indexPath.row] forKey:@"SelectedClient"];
        
        
        NSLog(@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedClient"]);

        
        
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        ProjectList *projectlist=[[ProjectList   alloc]init];
        [self.navigationController pushViewController:projectlist animated:YES];
    }
    
    
    
    
}
-(void)close_btn_click:(UIButton *)sender
{
    [self.commentsview1 removeFromSuperview];
}
-(void)maindelay
{
    commenttableview=self.commentsview1.commenttable.frame;
    chatview=self.commentsview1.chatview.frame;
    textview=self.commentsview1.chattextview.frame;
    
}

-(void)CheckStautes
{
    
    
    NSURL *url = [NSURL URLWithString:Count_project];
    //fb_gmail_id=1234&name=riya&email=riya@gmail.com&login_type=F&device_token=Afdrfdrvrf&device_type=A
    [APPDELEGATE addLoader:nil];
    ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
    [request addPostValue:[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails] valueForKey:@"id"]  forKey:@"user_id"];
    //  NSString *joinedComponents = [emailsaddress componentsJoinedByString:@","];
    
    SBJsonWriter1 *writer=[[SBJsonWriter1 alloc]init];
    [[NSUserDefaults standardUserDefaults]setValue:@"1" forKey:@"FirstTime"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [request setDelegate:self];
    [request startAsynchronous];
    
    
}


-(void)requestFinished:(ASIHTTPRequest *)request
{
    
    
    [APPDELEGATE removeLoader];
    NSString *responseStr = [request responseString];
    SBJsonParser1 *parseData = [[SBJsonParser1 alloc] init];
    NSArray *responseArray = [parseData objectWithString:responseStr];
    
    
    if ([[responseArray valueForKey:@"sucess"] isEqualToString:@"0"]) {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Look who's growing!" message:[responseArray valueForKey:@"msg"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        
        
        
    }
    else
    {
        [[NSUserDefaults standardUserDefaults]setValue:@"0" forKey:@"FromPunch"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"FromEditingMode"];
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"EditArray"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        CreateProject *create=[[CreateProject alloc]init];
        [self.navigationController pushViewController:create animated:YES];
        
    }
    
    
}
- (void)requestFailed:(ASIHTTPRequest *)request
{
    [APPDELEGATE removeLoader];
    NSLog(@"%@",request);
    
    
    
    //    [appDelegate.loading removeFromSuperview];
    //    [appDelegate.alertPopUp.popUpDescription setText:@"ERROR"];
    //    [self.view addSubview:appDelegate.alertPopUp];
}



#pragma mark btn click evenet
- (IBAction)create_project:(id)sender {
    
    
    [self CheckStautes];
    
    
    
}
- (IBAction)setting_btn_click:(id)sender {
    SettingView *setting=[[SettingView alloc]init];
    [self.navigationController pushViewController:setting  animated:YES];
    
}
- (void)keyboardShow:(NSNotification *)notification
{
    NSLog(@"%f", [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height);
    keyboard_height = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    
    //  self.commentsview1.chatview.frame = CGRectMake(self.commentsview1.chatview.frame.origin.x, [[UIScreen mainScreen] bounds].size.height- self.commentsview1.chatview.frame.size.height,self.commentsview1.chatview.frame.size.width, self.commentsview1.chatview.frame.size.height);
}

- (void)keyboardHide:(NSNotification *)notification
{
    
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    
    [self adjustFrames];
    
    return YES;
}
-(void) adjustFrames
{
    //    CGRect textFrame = self.commentsview1.chattextview.frame;
    //
    //
    //    NSLog(@"%f",textFrame.size.height);
    //
    //
    //    if (i<10) {
    //        if (textFrame.size.height< self.commentsview1.chattextview.contentSize.height) {
    //
    //
    //            self.commentsview1.chatview.frame = CGRectMake(self.commentsview1.chatview.frame.origin.x,self.commentsview1.chatview.frame.origin.y+self.commentsview1.chatview.frame.size.height-self.commentsview1.chattextview.contentSize.height-5, self.commentsview1.chatview.frame.size.width,  self.commentsview1.chattextview.contentSize.height+5);
    //            textFrame.size.height =  self.commentsview1.chattextview.contentSize.height;
    //            self.commentsview1.chattextview.frame = textFrame;
    //            i++;
    //        }
    //
    //
    //    }
    //
    //
    //    //
    //    NSLog(@"%f",textFrame.size.height);
    [self performSelector:@selector(delay123) withObject:self afterDelay:0];
}
-(void)delay123
{
    CGRect textFrame = self.commentsview1.chattextview.frame;
    
    
    NSLog(@"%f",self.commentsview1.chattextview.contentSize.height);
    
    self.commentsview1.commenttable.translatesAutoresizingMaskIntoConstraints=YES;
    
    
    if ((textFrame.size.height-self.commentsview1.chattextview.contentSize.height)>20) {
        self.commentsview1.commenttable.frame=CGRectMake(self.commentsview1.commenttable.frame.origin.x, self.commentsview1.commenttable.frame.origin.y+(textFrame.size.height-self.commentsview1.chattextview.contentSize.height),self.commentsview1.commenttable.frame.size.width ,self.commentsview1.commenttable.frame.size.height);
        
        
        if (self.commentsview1.chattextview.contentSize.height < textFrame.size.height && self.commentsview1.chattextview.contentSize.height<150) {
            
            
            self.commentsview1.chatview.translatesAutoresizingMaskIntoConstraints=YES;
            self.commentsview1.chattextview.translatesAutoresizingMaskIntoConstraints=YES;
            self.commentsview1.chatview.frame = CGRectMake(self.commentsview1.chatview.frame.origin.x,self.commentsview1.chatview.frame.origin.y+self.commentsview1.chatview.frame.size.height-self.commentsview1.chattextview.contentSize.height-5, self.commentsview1.chatview.frame.size.width,  self.commentsview1.chattextview.contentSize.height+5);
            textFrame.size.height =  self.commentsview1.chattextview.contentSize.height;
            self.commentsview1.chattextview.translatesAutoresizingMaskIntoConstraints=YES;
            
            self.commentsview1.chattextview.frame=CGRectMake(self.commentsview1.chattextview.frame.origin.x,(self.commentsview1.chattextview.frame.origin.y+self.commentsview1.chattextview.contentSize.height)-self.commentsview1.chattextview.contentSize.height, self.commentsview1.chattextview.frame.size.width,  self.commentsview1.chattextview.contentSize.height);
            
            
            // self.commentsview1.chattextview.frame = textFrame;
            // [self.commentsview1.chattextview sizeToFit];
            
        }
        
        
        
        
    }
    else
    {
        if ((self.commentsview1.chattextview.contentSize.height-textFrame.size.height)>20 && self.commentsview1.chattextview.contentSize.height<150) {
            //         self.commentsview1.commenttable.frame=CGRectMake(self.commentsview1.commenttable.frame.origin.x, self.commentsview1.commenttable.frame.origin.y, self.commentsview1.commenttable.frame.size.width,self.commentsview1.commenttable.frame.size.height-(self.commentsview1.chattextview.contentSize.height-textFrame.size.height)-6);
            self.commentsview1.commenttable.frame=CGRectMake(self.commentsview1.commenttable.frame.origin.x, self.commentsview1.commenttable.frame.origin.y-(self.commentsview1.chattextview.contentSize.height-textFrame.size.height),self.commentsview1.commenttable.frame.size.width ,self.commentsview1.commenttable.frame.size.height);
        }
        
        
        
        //    if (i<10) {
        if (textFrame.size.height< self.commentsview1.chattextview.contentSize.height && self.commentsview1.chattextview.contentSize.height<150) {
            
            
            self.commentsview1.chatview.translatesAutoresizingMaskIntoConstraints=YES;
            self.commentsview1.chattextview.translatesAutoresizingMaskIntoConstraints=YES;
            self.commentsview1.chatview.frame = CGRectMake(self.commentsview1.chatview.frame.origin.x,self.commentsview1.chatview.frame.origin.y+self.commentsview1.chatview.frame.size.height-self.commentsview1.chattextview.contentSize.height-5, self.commentsview1.chatview.frame.size.width,  self.commentsview1.chattextview.contentSize.height+5);
            textFrame.size.height =  self.commentsview1.chattextview.contentSize.height;
            self.commentsview1.chattextview.translatesAutoresizingMaskIntoConstraints=YES;
            
            self.commentsview1.chattextview.frame=CGRectMake(self.commentsview1.chattextview.frame.origin.x,(self.commentsview1.chattextview.frame.origin.y+self.commentsview1.chattextview.contentSize.height)-self.commentsview1.chattextview.contentSize.height, self.commentsview1.chattextview.frame.size.width,  self.commentsview1.chattextview.contentSize.height);
            
            
            // self.commentsview1.chattextview.frame = textFrame;
            // [self.commentsview1.chattextview sizeToFit];
            
        }
    }
    
    // }
    // jfdklsf
    
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    
    [self.commentsview1 removeFromSuperview];
    
    
}
-(void)send:(UIButton *)sendbutton
{
    i=0;
    
    self.commentsview1.chatview.frame=chatview;
    self.commentsview1.chattextview.frame=textview;
    self.commentsview1.commenttable.frame=commenttableview;
    
    //    self.commentsview1.chattextview.frame=CGRectMake(self.commentsview1.chatview.frame.origin.x,self.commentsview1.commenttable.frame.origin.y+self.commentsview1.commenttable.frame.size.height+5,self.commentsview1.chatview.frame.size.width,38);
    //    self.commentsview1.chatview.frame = CGRectMake(self.commentsview1.chatview.frame.origin.x,self.commentsview1.commenttable.frame.origin.y+self.commentsview1.commenttable.frame.size.height,self.commentsview1.chatview.frame.size.width,43);
    
    if ([self.commentsview1.chattextview.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0 || [self.commentsview1.chattextview.text isEqualToString:@"Write here..."] ) {
        
        
        self.commentsview1.chattextview.text=@"Write here...";
        [self.commentsview1.chattextview endEditing:TRUE];
        return;
    }
    else
    {
        NSString *comment=self.commentsview1.chattextview.text;
        // {"method":"give_comment","task_id":"1","comment":"please complete task fast","user_id":"5"}
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        
        [dic setObject:@"give_comment" forKey:@"method"];
        
        [dic setObject:[[tempobje objectAtIndex:0] valueForKey:@"task_id"] forKey:@"task_id"];
        [dic setObject:[[[NSUserDefaults standardUserDefaults] valueForKey:LoginUserDetails] valueForKey:@"id"] forKey:@"user_id"];
        [dic setObject:comment forKey:@"comment"];
        [APPDELEGATE addLoader:nil];
        [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"give_comment"];
        // comment = sfsdjfhksjafhaldksjfhlaskdjfhlaskjfhaslkdjfhsaldkfjhsalfjkhsaldkfjhsaldkfjhsalkfjhsaldkfjhsadklfjhasklfjhaskldjfhsakldfjhaskldfjhsakldfjhsakljdfhajksfhalksjfhalskdjfhaskljdfhsdjkfhskdjfhaskjfhklajsdhfjklsahfklsajdhfjklsdhfklasjfhalskjfhaklsjdfhaklsjfhaskldfhajklsfh;
        //        id = 102;
        //        image = "1438667571.png";
        //        insertdate = "2015-08-04 09:47:49";
        //        name = john;
        //        "task_id" = 9;
        //        "user_id" = 8;
        NSMutableDictionary *tempcomment=[[NSMutableDictionary alloc]init];
        [tempcomment  setValue:comment forKey:@"comment"];
        [tempcomment setValue:[[[NSUserDefaults standardUserDefaults] valueForKey:LoginUserDetails] valueForKey:@"image"] forKey:@"image"];
        [tempcomment setValue:[[[NSUserDefaults standardUserDefaults] valueForKey:LoginUserDetails] valueForKey:@"name"] forKey:@"name"];
        [commnets insertObject:tempcomment atIndex:[commnets count]];
        [self.commentsview1.commenttable beginUpdates];
        NSIndexPath *myIP = [NSIndexPath indexPathForRow:([commnets count]-1) inSection:0];
        NSArray *myArray = [[NSArray alloc] initWithObjects:myIP, nil];
        [self.commentsview1.commenttable insertRowsAtIndexPaths:myArray withRowAnimation:UITableViewRowAnimationBottom];
        
        //[self.mytableview deleteRowsAtIndexPaths:path withRowAnimation:UITableViewRowAnimationBottom];
        [self.commentsview1.commenttable endUpdates];
        
        
        
        
        self.commentsview1.chattextview.text=@"Write here...";
    }
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[commnets count]-1 inSection:0];
    [self.commentsview1.commenttable scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    [self.commentsview1.chattextview endEditing:TRUE];
    
    
    
    self.commentsview1.chattextview.text=@"Write here...";
    
    
    
}
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if ([commnets count]>0) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[commnets count]-1 inSection:0];
        [self.commentsview1.commenttable scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
        
    }
    textView.text=@"";
    return YES;
    
}
- (void)textViewDidEndEditing:(UITextView *)textView;
{
    
    
}
- (void)textViewDidChange:(UITextView *)textView;
{
    
    [self adjustFrames];
    
    
}

#pragma mark labelSize
- (CGSize) resizeMessageLabel:(UILabel *)lblMessage
{
    CGSize maxLabelSize = CGSizeMake([[UIScreen mainScreen] bounds].size.width-20,9999);
    
    CGSize expectedLabelSize = [lblMessage.text sizeWithFont:lblMessage.font
                                           constrainedToSize:maxLabelSize
                                               lineBreakMode:lblMessage.lineBreakMode];
    expectedLabelSize.height=expectedLabelSize.height+20;
    
    //adjust the label the the new height.
    //    CGRect newFrame = lblMessage.frame;
    //    newFrame.size.height = expectedLabelSize.height;
    //    lblMessage.frame = newFrame;
    //    viewMessage.frame = CGRectMake(viewMessage.frame.origin.x, viewMessage.frame.origin.y, viewMessage.frame.size.width, lblMessage.frame.size.height);
    
    return expectedLabelSize;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark web service
-(void)webHandlerManager:(Webservice *)webHandlerManager didFinishLoadingWithData:(NSObject *)responseObject
{
    [APPDELEGATE removeLoader];
    
    if ([webHandlerManager.strMethod isEqualToString:@"comment_list"])
    {
        if([[responseObject valueForKey:@"sucess"]isEqualToString:@"1"])
        {
            
            
            UILabel *templabel=[[UILabel alloc]init];
            templabel.text=[[responseObject valueForKey:KData] valueForKey:@"details"];
            templabel.font=[UIFont fontWithName:@"OpenSans" size:14.0];
            int hight=[self resizeMessageLabel:templabel].height;
            self.commentsview1.tasktitle.text=[[responseObject valueForKey:KData] valueForKey:@"task_title"];
            self.commentsview1.texview.text=[[responseObject valueForKey:KData] valueForKey:@"details"];
            self.commentsview1.detailTask.text=[[responseObject valueForKey:KData] valueForKey:@"details"];
            self.commentsview1.detailview.frame=CGRectMake(0, 0, self.commentsview1.detailview.frame.size.width, 130+hight);
            self.commentsview1.commenttable.tableHeaderView=self.commentsview1.detailview;
            [commnets removeAllObjects];
            
            ////// Bhushan Rana
            
            
            commnets=[responseObject valueForKey:@"comment_data"];
            [self.commentsview1.commenttable reloadData];
            
            
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                    [dic setObject:@"message_listing" forKey:@"method"];
                    [dic setObject:[[[NSUserDefaults standardUserDefaults] valueForKey:LoginUserDetails] valueForKey:@"id"] forKey:@"user_id"];
                    
                    [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"message_listing"];
                    
                    //                    [self.activityindicator stopAnimating];
                    //                    [self.activityindicator setHidden:YES];
                    
                });
            });
            
        }
        else
        {
            
            //            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Punch List" message:[responseObject valueForKey:@"msg"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            //            [alert show];
            
            
        }
        
    }
    if ([webHandlerManager.strMethod isEqualToString:@"give_comment"])
    {
        if([[responseObject valueForKey:@"sucess"]isEqualToString:@"1"])
        {
            [self.commentsview1 endEditing:YES];
        }
        else
        {
            [self.commentsview1 endEditing:YES];
        }
    }
    if ([webHandlerManager.strMethod isEqualToString:@"company_list"])
    {
        if([[responseObject valueForKey:@"sucess"]isEqualToString:@"1"])
        {
            
            
            
            list=[responseObject valueForKey:KData];
            list = [[[list reverseObjectEnumerator] allObjects] mutableCopy];
            
            //            [[NSUserDefaults standardUserDefaults]setObject:list forKey:@"List"];
            //            [[NSUserDefaults standardUserDefaults]synchronize];
            //            NSIndexSet *sections = [[NSIndexSet alloc] initWithIndex:2];
            //
            //            [self.tableview reloadSections:sections withRowAnimation:UITableViewRowAnimationFade];
            
            //{"method":"message_listing","user_id":"2"}
            
            [self tableFooterview];
            [self.tableview reloadData];
            NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
            [dic setObject:@"message_listing" forKey:@"method"];
            [dic setObject:[[[NSUserDefaults standardUserDefaults] valueForKey:LoginUserDetails] valueForKey:@"id"] forKey:@"user_id"];
            [APPDELEGATE addLoader:nil];
            [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"message_listing"];
            
            
        }
        else
        {
//            list = [NSMutableArray array];
//            NSIndexSet *sections = [[NSIndexSet alloc] initWithIndex:2];
//            [self.tableview reloadSections:sections withRowAnimation:UITableViewRowAnimationFade];
//            [self tableFooterview];
        }
    }
    if ([webHandlerManager.strMethod isEqualToString:@"message_listing"])
    {
        if([[responseObject valueForKey:@"sucess"]isEqualToString:@"1"])
        {
            
            projectlist=[responseObject valueForKey:KData];
            
            NSSortDescriptor* sortOrder = [NSSortDescriptor sortDescriptorWithKey: @"count" ascending: NO];
            projectlist=[[projectlist sortedArrayUsingDescriptors: [NSArray arrayWithObject: sortOrder]] mutableCopy];
            
            int countmessage=0;
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"count != %@",@"0"];
            NSArray *filtered = [projectlist filteredArrayUsingPredicate:predicate];
            
            //            NSArray *array2 =
            //            [projectlist filteredArrayUsingPredicate:
            //             [NSPredicate predicateWithBlock:^BOOL(id obj, NSDictionary *d) {
            //                return [[obj valueForKey:@"count"] isEqual:@0];
            //            }]];
            projectlist=[filtered mutableCopy];
            
            for (int j=0; j<[projectlist count]; j++) {
                countmessage=countmessage+[[[projectlist objectAtIndex:j]valueForKey:@"count"] intValue];
                
            }
            
            // projectlist
            //            if (countmessage!=0) {
            
            [counter setTitle:[NSString stringWithFormat:@"%d",countmessage] forState:UIControlStateNormal];
            
            if (countmessage==0) {
                counter.hidden=TRUE;
                projectlist=nil;
                projectlist =[[NSMutableArray alloc]init];
            }
            else
            {
                
                counter.hidden=FALSE;
            }
            
            
            
            //                NSIndexSet *sections = [[NSIndexSet alloc] initWithIndex:0];
            //
            //                [self.tableview reloadSections:sections withRowAnimation:UITableViewRowAnimationFade];
            
            
            NSIndexSet *sections = [[NSIndexSet alloc] initWithIndex:0];
            
            [self.tableview reloadSections:sections withRowAnimation:UITableViewRowAnimationFade];
            [self.tableview reloadData];
            
            /*Date 27/11 TIme : 6:58
             NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
             [dic setObject:@"toolshed_list" forKey:@"method"];
             [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"toolshed_list"];
             */
            
            // [self.tableview reloadData];
            //                [[NSUserDefaults standardUserDefaults]setObject:[responseObject valueForKey:KData] forKey:@"MessageListing"];
            //                [[NSUserDefaults standardUserDefaults]synchronize];
            // }
            
            
            
        }
        else
        {
            counter.hidden=TRUE;
            projectlist=nil;
            projectlist=[[NSMutableArray alloc]init];
            NSIndexSet *sections = [[NSIndexSet alloc] initWithIndex:0];
            
            [self.tableview reloadSections:sections withRowAnimation:UITableViewRowAnimationFade];
            
        }
    }
    if ([webHandlerManager.strMethod isEqualToString:@"toolshed_list"])
    {
        if([[responseObject valueForKey:@"sucess"]isEqualToString:@"1"])
        {
            
            
            toolshedurl=[[[responseObject valueForKey:KData] objectAtIndex:0] valueForKey:@"url"];
            
            NSString *temppath=[[[responseObject valueForKey:KData] objectAtIndex:0] valueForKey:@"image_name"];
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                 NSUserDomainMask, YES);
            //
            NSString *url = [NSString stringWithFormat:@"%@%@",AdThumbsImages,temppath];
            NSString *filename =[NSString stringWithFormat:@"Ad_%@",temppath ];
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString* path = [documentsDirectory stringByAppendingPathComponent:filename];
            UIImage* image = [UIImage imageWithContentsOfFile:path];
            if (image==nil) {
                
                NSString *url = [NSString stringWithFormat:@"%@%@",AdThumbsImages,temppath];
                dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                    
                    NSString *str=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                    NSURL *imagepath = [NSURL URLWithString:str];
                    
                    NSData *data = [NSData dataWithContentsOfURL:imagepath];
                    UIImage *image = [UIImage imageWithData:data];
                    [data writeToFile:path atomically:YES];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [self.sectionheaderimage setImage:image];
                        //                    [self.activityindicator stopAnimating];
                        //                    [self.activityindicator setHidden:YES];
                        
                    });
                });
                
            }
            else
            {
                [ self.sectionheaderimage setImage:image];
                //            [self.activityindicator stopAnimating];
                //            [self.activityindicator setHidden:YES];
                
            }
            
            self.sectiontitle.text=[[[responseObject valueForKey:KData] objectAtIndex:0] valueForKey:@"description"];
            //            [self.sectiontitle setNumberOfLines:0];
            //            [self.sectiontitle sizeToFit];
            
            [[NSUserDefaults standardUserDefaults]setObject:[responseObject valueForKey:KData] forKey:@"toolshed"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
        }
        else
        {
            
        }
    }
    if ([webHandlerManager.strMethod isEqualToString:@"customer_list"])
    {
        if([[responseObject valueForKey:@"sucess"]isEqualToString:@"1"])
        {
            list=[responseObject valueForKey:KData];
            // list = [[[list reverseObjectEnumerator] allObjects] mutableCopy];
            //            [[NSUserDefaults standardUserDefaults]setObject:list forKey:@"List"];
            //            [[NSUserDefaults standardUserDefaults]synchronize];
            NSIndexSet *sections = [[NSIndexSet alloc] initWithIndex:1];
            
            
            //            [self.tableview reloadSections:sections withRowAnimation:UITableViewRowAnimationFade];
            
            [self.tableview reloadData];
            [self tableFooterview];
            NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
            [dic setObject:@"message_listing" forKey:@"method"];
            [dic setObject:[[[NSUserDefaults standardUserDefaults] valueForKey:LoginUserDetails] valueForKey:@"id"] forKey:@"user_id"];
            [APPDELEGATE addLoader:nil];
            [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"message_listing"];
            
        }
        else
        {
            
        }
    }
    
    
    
    
    
    
    
    
}
-(void)webHandlerManager:(Webservice *)webHandlerManager didFailedLoadingWithError:(NSString *)responseError
{
    
    NSLog(@"%@",responseError);
    NSLog(@"%@",webHandlerManager.strMethod);
    
}

@end
