//
//  ViewController.m
//  Punch
//
//  Created by Jaimish on 30/06/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import "ViewController.h"
#import "SignupviewView.h"
#import "Homeview.h"
#import "GTMHTTPFetcher.h"
#import "GTMOAuth2Authentication.h"
#import "GTMOAuth2ViewControllerTouch.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "SBJson.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "ForgotPassword.h"
@interface ViewController ()
{

    //  FBLoginView *loginview;
    NSString *logintype;
}
//@property (strong, nonatomic) id<FBGraphUser> loggedInUser;
@end

@implementation ViewController

- (void)viewDidLoad {
    
    //  Set attribute text on button title
    UIFont *font1 = [UIFont fontWithName:@"OpenSans" size:17.0f];
    UIColor *color = [UIColor whiteColor];
    NSDictionary *dict1 = @{NSUnderlineStyleAttributeName:@(NSUnderlineStyleSingle),
                            NSFontAttributeName:font1,NSForegroundColorAttributeName:color};
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] init];
    [attString appendAttributedString:[[NSAttributedString alloc] initWithString:@"Forgot password?"    attributes:dict1]];

    [self.forgotpassword setAttributedTitle:attString forState:UIControlStateNormal];
    
    NSMutableAttributedString *attString1 = [[NSMutableAttributedString alloc] init];
    [attString1 appendAttributedString:[[NSAttributedString alloc] initWithString:@"New User? SIGNUP here"    attributes:dict1]];
  
    
     [self.signupbtn setAttributedTitle:attString1 forState:UIControlStateNormal];
    [self changeColor:self.txt_email];
    [self changeColor:self.txt_password];
    [self.txt_password addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
//    [txtPassWord addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    
    self.navigationController.navigationBarHidden=true;

    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:LoginUser];
    [[NSUserDefaults standardUserDefaults]synchronize];
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
  
    logintype=nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)textFieldDidChange:(id)sender
{
    UITextField *textField = (UITextField *)sender;
    
    if (textField == _txt_password && !iOS8)
    {
        // Set to custom font if the textfield is cleared, else set it to system font
        // This is a workaround because secure text fields don't play well with custom fonts
        if (textField.text.length == 0)
        {
            textField.font = [UIFont fontWithName:@"Open Sans" size:18.0];
        }
        else
        {
            textField.font = [UIFont systemFontOfSize:textField.font.pointSize];
        }
    }
}
- (IBAction)signup_btn_click:(id)sender {
    SignupviewView *signup=[[SignupviewView alloc]init];
    [self.navigationController pushViewController:signup animated:YES];
    
}
- (IBAction)login_btn_click:(id)sender {
    
    if (![self.txt_email.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0) {
        
        
        if ([self validateEmail:self.txt_email.text]) {
            
            if ([self.txt_password.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length==0) {
                
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:@"Please enter password" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
                return;
                
            }
            //{"method":"user_login","email":"sonali@hyperlinkinfosystem.com","password":"123456","device_type":"A","device_token":"12345"}
            NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
            [dic setObject:@"user_login" forKey:@"method"];
            [dic setObject:self.txt_email.text forKey:@"email"];
              [dic setObject:self.txt_password.text forKey:@"password"];
             [dic setObject:@"I" forKey:@"device_type"];
            if (TARGET_OS_SIMULATOR)
            {
                [dic setObject:@"0" forKey:@"device_token"];
            }
            else 
            {
                if (APPDELEGATE.devicetoken)
                    [dic setObject:APPDELEGATE.devicetoken forKey:@"device_token"];
                else
                    [dic setObject:@"0" forKey:@"device_token"];
            }
            [APPDELEGATE addLoader:nil];
            [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"user_login"];
            
        }
        else
        {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:@"Please enter a valid email address" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            
        }
    }
    else
    {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:@"Please enter email" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        
    }

   
    
}
- (BOOL)validateEmail:(NSString *)emailStr
{
    NSString *emailRegex = @"^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,4})$";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailStr];
}
-(void)changeColor:(UITextField *)textField
{
    
    if ([textField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        UIColor *color = [UIColor blackColor];
        textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: color}];
    } else {
        NSLog(@"Cannot set placeholder text's color, because deployment target is earlier than iOS 6.0");
       
    }
    
}

- (IBAction)forgotpass_ok_click:(id)sender {
    [self.txt_forgetemail resignFirstResponder];
    if (![self.txt_forgetemail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0) {
        if ([self validateEmail:self.txt_forgetemail.text]) {
            
            //{"method":"forgot_password","email":"sonali@hyperlinkinfosystem.com"}
            NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
            [dic setObject:@"forgot_password" forKey:@"method"];
            [dic setObject:self.txt_forgetemail.text forKey:@"email"];
               [APPDELEGATE addLoader:nil];
            [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"forgot_password"];
            
        }
        else
        {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:@"Please enter a valid email address" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            
            
        }
    }
    else
    {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:@"Please enter email" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        
    }
    
}

- (IBAction)forgot_password:(id)sender {
    
    
    
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
   self.txt_forgetemail.leftView = paddingView;
    self.txt_forgetemail.leftViewMode = UITextFieldViewModeAlways;
    self.forgotpassview.frame=CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width,  [[UIScreen mainScreen] bounds].size.height);
    [self.view addSubview:self.forgotpassview];
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    
    [self.forgotpassview removeFromSuperview];
    
    
}

#pragma mark btn_click event
- (IBAction)fb_loginbtn_click:(id)sender {
    

    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    logintype=@"F";
    [login logInWithReadPermissions:@[@"email"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error)
        {
            // Process error
        }
        else if (result.isCancelled)
        {
            // Handle cancellations
        }
        else {
            // If you ask for multiple permissions at once, you
            // should check if specific permissions missing
            if ([result.grantedPermissions containsObject:@"email"]) {
                
                [self getUserInfo];
                
            }
        }
    }];
    
    
    
//    loginview = [[FBLoginView alloc]initWithReadPermissions:@[@"email",@"public_profile"]];
//    loginview.delegate = self;
//    logintype=@"F";
//    [FBSession openActiveSessionWithReadPermissions:@[@"public_profile",@"email"]
//                                       allowLoginUI:YES
//                                  completionHandler:
//     ^(FBSession *session, FBSessionState state, NSError *error) {
//         
//         // Retrieve the app delegate
//         
//         // Call the app delegate's sessionStateChanged:state:error method to handle session state changes
//         [APPDELEGATE sessionStateChanged:session state:state error:error];
//         // [fbloginbtn setTitle:@"Logout" forState:UIControlStateNormal];
//     }];
    
}
-(void)getUserInfo
{
//    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me"
//                                       parameters:@{@"fields": @"public_profile, email"}]
//     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
//         if (!error) {
//             NSLog(@"%@",result);
//             //              NSString *pictureURL = [NSString stringWithFormat:@"%@",[result objectForKey:@"picture"]];
//             //
//             //              NSLog(@"email is %@", [result objectForKey:@"email"]);
//             //
//             //              NSData  *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:pictureURL]];
//             //              _imageView.image = [UIImage imageWithData:data];
//             
//         }
//         else{
//             NSLog(@"%@", [error localizedDescription]);
//         }
//     }];
    
    
    
    FBSDKGraphRequest *requestMe = [[FBSDKGraphRequest alloc]initWithGraphPath:@"me" parameters:@{@"fields": @"name,id,email,first_name,last_name"}];
    
    FBSDKGraphRequestConnection *connection = [[FBSDKGraphRequestConnection alloc] init];
    
    [connection addRequest:requestMe completionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
        
        if(result)
        {
            if ([result objectForKey:@"email"]) {
                
                NSLog(@"Email: %@",[result objectForKey:@"email"]);
                
            }
            if ([result objectForKey:@"first_name"]) {
                
                NSLog(@"First Name : %@",[result objectForKey:@"first_name"]);
                
            }
            if ([result objectForKey:@"id"]) {
                
                NSLog(@"User id : %@",[result objectForKey:@"id"]);
                
            }
            if ([result objectForKey:@"name"]) {
                
                NSLog(@"Email: %@",[result objectForKey:@"name"]);
                
            }
            
            
            [APPDELEGATE addLoader:nil];
           
            
                NSURL *url = [NSURL URLWithString:kFBGmailRegister];
                //fb_gmail_id=1234&name=riya&email=riya@gmail.com&login_type=F&device_token=Afdrfdrvrf&device_type=A
                ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
                [request addPostValue:[result objectForKey:@"name"]  forKey:@"name"];
                [request addPostValue:[result objectForKey:@"id"] forKey:@"fb_gmail_id"];
                [request addPostValue:[result objectForKey:@"email"] forKey:@"email"];
                [request addPostValue:@"F" forKey:login_type];
                
                if (TARGET_OS_SIMULATOR)
                {
                    [request addPostValue:@"4727887462c7c8a205006e4ef9fa6a70344715b11fe2a1e7bb2254c22a9963fe" forKey:@"device_token"];
                }
                else
                {
                    [request addPostValue:APPDELEGATE.devicetoken forKey:@"device_token"];
                }
                
                [request addPostValue:@"I" forKey:@"device_type"];
                [[NSUserDefaults standardUserDefaults]setObject:[request valueForKey:@"postData"] forKey:LoginUser];
                [[NSUserDefaults standardUserDefaults]synchronize];
                NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:LoginUser]);
                [request setDelegate:self];
                [request startAsynchronous];

            
        }
        
    }];
    
    [connection start];
}
-(IBAction)fb_logoutbtn_click:(id)sender
{
//    
//    if (FBSession.activeSession.state == FBSessionStateOpen
//        || FBSession.activeSession.state == FBSessionStateOpenTokenExtended) {
//        
//        // Close the session and remove the access token from the cache
//        // The session state handler (in the app delegate) will be called automatically
//        [FBSession.activeSession closeAndClearTokenInformation];
//        //[fbloginbtn setTitle:@"Login" forState:UIControlStateNormal];
//        // If the session state is not any of the two "open" states when the button is clicked
//    }
    
    
}


//- (void)loginViewShowingLoggedInUser:(FBLoginView *)loginView {
//    // first get the buttons set for login mode
//    
//    
//    // "Post Status" available when logged on and potentially when logged off.  Differentiate in the label.
//    
//}
//- (BOOL)isUser:(id<FBGraphUser>)firstUser equalToUser:(id<FBGraphUser>)secondUser {
//
//      return  [firstUser.objectID isEqual:secondUser.objectID] && [firstUser.name isEqual:secondUser.name] &&
//    [firstUser.first_name isEqual:secondUser.first_name];
//   
////}
//- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView
//                            user:(id<FBGraphUser>)user {
//    // here we use helper properties of FBGraphUser to dot-through to first_name and
//    // id properties of the json response from the server; alternatively we could use
//    // NSDictionary methods such as objectForKey to get values from the my json object
//    //self.labelFirstName.text = [NSString stringWithFormat:@"Hello %@!", user.first_name];
//    // setting the profileID property of the FBProfilePictureView instance
//    // causes the control to fetch and display the profile picture for the user
//    //self.profilePic.profileID = user.objectID;
//    //NSLog(@"%@",user.objectID);
//    if (![self isUser:self.loggedInUser equalToUser:user]) {
//        [APPDELEGATE addLoader:nil];
//       self.loggedInUser = user;
//        if (logintype!=nil && user!=nil) {
//            NSURL *url = [NSURL URLWithString:@"http://hyperlinkserver.com/punchlist/ws/fb_gamil_register.php?"];
//            //fb_gmail_id=1234&name=riya&email=riya@gmail.com&login_type=F&device_token=Afdrfdrvrf&device_type=A
//            ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
//            [request addPostValue:user.name  forKey:@"name"];
//            [request addPostValue:user.objectID forKey:@"fb_gmail_id"];
//            [request addPostValue:[user objectForKey:@"email"] forKey:@"email"];
//            [request addPostValue:@"F" forKey:login_type];
//            
//            NSString *model = [[UIDevice currentDevice] model];//for dettct simulator or device
//            if ([model isEqualToString:@"iPhone Simulator"])
//            {
//                [request addPostValue:@"112354" forKey:@"device_token"];
//            }
//            else
//            {
//                //[request addPostValue:APPDELEGATE.devicetoken forKey:@"device_token"];
//                 [request addPostValue:@"112354" forKey:@"device_token"];
//            }
//            
//            [request addPostValue:@"I" forKey:@"device_type"];
//            [[NSUserDefaults standardUserDefaults]setObject:[request valueForKey:@"postData"] forKey:LoginUser];
//            [[NSUserDefaults standardUserDefaults]synchronize];
//            NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:LoginUser]);
//            [request setDelegate:self];
//            [request startAsynchronous];
//
//        }
//
//        /// Do something
//    }
//   
//    
//}

//- (void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView {
//    // test to see if we can use the share dialog built into the Facebook application
//    //    FBLinkShareParams *p = [[FBLinkShareParams alloc] init];
//    //    p.link = [NSURL URLWithString:@"http://developers.facebook.com/ios"];
//    //    BOOL canShareFB = [FBDialogs canPresentShareDialogWithParams:p];
//    //    BOOL canShareiOS6 = [FBDialogs canPresentOSIntegratedShareDialogWithSession:nil];
//    //    BOOL canShareFBPhoto = [FBDialogs canPresentShareDialogWithPhotos];
//    
//    
//    
//    // "Post Status" available when logged on and potentially when logged off.  Differentiate in the label.
//    //    [self.buttonPostStatus setTitle:@"Post Status Update (Logged Off)" forState:self.buttonPostStatus.state];
//    //
//    //    self.profilePic.profileID = nil;
//    //    self.labelFirstName.text = nil;
//    //    self.loggedInUser = nil;
//}

//- (void)loginView:(FBLoginView *)loginView handleError:(NSError *)error {
//    // see https://developers.facebook.com/docs/reference/api/errors/ for general guidance on error handling for Facebook API
//    // our policy here is to let the login view handle errors, but to log the results
//    NSLog(@"FBLoginView encountered an error=%@", error);
//}
#pragma mark Gmail Login
/*
 AIzaSyC_Vq9f38ja3w1kCFyIuqK10n41TxNXZ_U
 Client ID
 722768863532-m96s07v6mbvb6h4tnu5pjsqu81qrsed5.apps.googleusercontent.com
 Client secret
 br9VILbgbM1apaKbuYZDS9OC
 Redirect URIs
 https://www.example.com/oauth2callback
 JavaScript origins
 https://www.example.com
*/

//old
//#define GoogleClientID    @"722768863532-m96s07v6mbvb6h4tnu5pjsqu81qrsed5.apps.googleusercontent.com"
//#define GoogleClientSecret @"br9VILbgbM1apaKbuYZDS9OC"

//Other(ios)
//44114125212-79pu8o29po1352tflrp9ejpjiiqksmm0.apps.googleusercontent.com
//S1R973ve3dnY1gvQ6ukpyeW_

//Web
//44114125212-agbfv8mjg4gc78t6u3ignh8ge3c60fm1.apps.googleusercontent.com
//pz6FUDlvPHPLe2lAnq1qITld

#define GoogleClientID    @"44114125212-agbfv8mjg4gc78t6u3ignh8ge3c60fm1.apps.googleusercontent.com"
#define GoogleClientSecret @"pz6FUDlvPHPLe2lAnq1qITld"
#define GoogleAuthURL   @"https://accounts.google.com/o/oauth2/auth"
#define GoogleTokenURL  @"https://accounts.google.com/o/oauth2/token"




-(IBAction)gmailBtn_click:(id)sender
{
 
    // NSLog(@"%@",GetDataString);
     logintype=@"G";

    [self signInToGoogle];
        
    
    
    //[self signInToGoogle];
}
- (GTMOAuth2Authentication * )authForGoogle
{
    //This URL is defined by the individual 3rd party APIs, be sure to read their documentation
    
    NSURL * tokenURL = [NSURL URLWithString:GoogleTokenURL];
    // We'll make up an arbitrary redirectURI.  The controller will watch for
    // the server to redirect the web view to this URI, but this URI will not be
    // loaded, so it need not be for any actual web page. This needs to match the URI set as the
    // redirect URI when configuring the app with Instagram.
    NSString * redirectURI = @"https://www.example.com/oauth2callback";
    GTMOAuth2Authentication * auth;
    
    auth = [GTMOAuth2Authentication authenticationWithServiceProvider:@"lifebeat"
                                                             tokenURL:tokenURL
                                                          redirectURI:redirectURI
                                                             clientID:GoogleClientID
                                                         clientSecret:GoogleClientSecret];
    auth.scope = @"https://www.googleapis.com/auth/userinfo.profile";
    return auth;
    
}


- (void)signInToGoogle
{
   
    GTMOAuth2Authentication * auth = [self authForGoogle];
    
    // Display the authentication view
    GTMOAuth2ViewControllerTouch * viewController = [[GTMOAuth2ViewControllerTouch alloc] initWithAuthentication:auth
                                                                                                authorizationURL:[NSURL URLWithString:GoogleAuthURL]
                                                                                                keychainItemName:@"GoogleKeychainName"
                                                                                                        delegate:self
                                                                                                finishedSelector:@selector(viewController:finishedWithAuth:error:)];
    
    [self.navigationController pushViewController:viewController animated:YES];
    
}


- (void)viewController:(GTMOAuth2ViewControllerTouch * )viewController
      finishedWithAuth:(GTMOAuth2Authentication * )auth
                 error:(NSError * )error
{
    
    // [SVProgressHUD showWithStatus:NSLocalizedString(@"Wait", nil) maskType:SVProgressHUDMaskTypeGradient];
    //    NSLog(@"finished");
    [APPDELEGATE addLoader:nil];

    if (error != nil) {
        
//        [SVProgressHUD dismiss];
//        DoAlertView *_vAlert = [[DoAlertView alloc] init];
//        _vAlert.dRound = 2.0;
//        
//        _vAlert.bDestructive = NO;
//        [_vAlert doYes:@"Fehler!!"
//                  body:@"Fehler Autorisieren mit Google"
//           yesBtnTitle:@"Ok"
//                   yes:^(DoAlertView *alertView) {
//                       
//                       NSLog(@"Yeeeeeeeeeeeees!!!!");
//                       
//                   }];
//        _vAlert = nil;
        
    } else {
        
        [APPDELEGATE removeLoader];
          [APPDELEGATE addLoader:nil];
        //https://www.googleapis.com/oauth2/v1/userinfo?access_token=ya29.1.AADtN_X9mXbtIxVYay3ILxxD0Y442TJay8DrVX74oSStyDpB2Fb9bY0NoC1qc7bFHGfKeJs
        NSString *popularURLString = [NSString stringWithFormat:@"https://www.googleapis.com/oauth2/v1/userinfo?access_token=%@",auth.accessToken];
        // NSLog(@"URL %@",popularURLString);
        
        NSURLResponse *response = nil;
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:popularURLString]];
        [auth authorizeRequest:request];
        NSData *data = [NSURLConnection sendSynchronousRequest:request
                                             returningResponse:&response
                                                         error:&error];
        if (data) {
            
          
            // API fetch succeeded
       // NSString *str =[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] ;
            //  NSLog(@"API response: %@", str);
            id val = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
            
            NSArray *data1 = val;
           logintype=@"G";
            NSURL *url = [NSURL URLWithString:kFBGmailRegister];
            //fb_gmail_id=1234&name=riya&email=riya@gmail.com&login_type=F&device_token=Afdrfdrvrf&device_type=A
            if (logintype!=nil) {
                ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];
                [request addPostValue:[data1 valueForKey:@"given_name"]  forKey:@"name"];
                  [request addPostValue:[data1 valueForKey:@"family_name"]  forKey:@"lastname"];
                
                [request addPostValue:[data1 valueForKey:@"id"] forKey:@"fb_gmail_id"];
                [request addPostValue:[data1 valueForKey:@"email"] forKey:@"email"];
                [request addPostValue:@"G" forKey:login_type];
                
                if (TARGET_OS_SIMULATOR)
                {
                    [request addPostValue:@"4727887462c7c8a205006e4ef9fa6a70344715b11fe2a1e7bb2254c22a9963fe" forKey:@"device_token"];
                }
                else
                {
                    [request addPostValue:APPDELEGATE.devicetoken forKey:@"device_token"];
                }
                
                [request addPostValue:@"I" forKey:@"device_type"];
                [[NSUserDefaults standardUserDefaults]setObject:[request valueForKey:@"postData"] forKey:LoginUser];
                [[NSUserDefaults standardUserDefaults]synchronize];
                [request setDelegate:self];
                [request startAsynchronous];

            }
            
            
            
//            
//            NSArray* name = [[data1 valueForKey:@"email"] componentsSeparatedByString: @"@"];
//            NSString* Username = [name objectAtIndex: 0];
            // NSLog(@"%@",Username);
            
//         //   NSString *chat_password=[self getRandomPassword];
//            
//            NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
//            [dic setValue:@"A" forKey:@"app_name"];
//            [dic setValue:Username forKey:@"username"];
//            [dic setValue:chat_password forKey:@"password"];
//            [dic setValue:chat_password forKey:@"chat_password"];
//            [dic setValue:devicetoken forKey:@"device_token"];
//            [dic setValue:[NSString stringWithFormat:@"%f",appDelegate.current_Latitude] forKey:@"current_latitude"];
//            [dic setValue:[NSString stringWithFormat:@"%f",appDelegate.current_Longitude] forKey:@"current_longitude"];
//            [dic setValue:@"1" forKey:@"device_type"];
//            [dic setValue:@"en" forKey:@"lang"];
//            [dic setValue:[data1 valueForKey:@"email"] forKey:@"email"];
//            [dic setValue:[data1 valueForKey:@"gender"] forKey:@"gender"];
//            [dic setValue:[data1 valueForKey:@"family_name"] forKey:@"surname"];
//            [dic setValue:[data1 valueForKey:@"given_name"] forKey:@"name"];
            //            if ([[data1 valueForKey:@"picture"] length]>0)
            //            {
            //                NSData *data=[NSData dataWithContentsOfURL:[NSURL URLWithString:[data1 valueForKey:@"picture"]]];
            //               // NSLog(@"%@",[data base64Encoding]);
            //                [dic setValue:[data base64Encoding] forKey:@"picture"];
            //
            //            }
//            [dic setValue:@"" forKey:@"picture"];
//            [appDelegate callWebservice:@"gmail_login.php" andParameters:[NSArray arrayWithObject:dic] andconrtoller:self];
            
            
            //            NSURL *url = [NSURL URLWithString:@"http://go-quicky.com/ws/gmail_login.php?"];
            //            ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
            //
            //            [request setDelegate:self];
            //            [request setRequestMethod:@"POST"];
            //
            //
            //            NSArray* name = [[data1 valueForKey:@"email"] componentsSeparatedByString: @"@"];
            //            NSString* Username = [name objectAtIndex: 0];
            //            NSLog(@"%@",Username);
            //
            //            NSString *chat_password=[self getRandomPassword];
            //
            //            [request addPostValue:@"A" forKey:@"app_name"];
            //            [request addPostValue:Username forKey:@"username"];
            //            [request addPostValue:chat_password forKey:@"password"];
            //            [request addPostValue:chat_password forKey:@"chat_password"];
            //            [request addPostValue:[NSString stringWithFormat:@"%f",appDelegate.current_Latitude] forKey:@"current_latitude"];
            //            [request addPostValue:[NSString stringWithFormat:@"%f",appDelegate.current_Longitude] forKey:@"current_longitude"];
            //            [request addPostValue:@"1" forKey:@"device_type"];
            //            [request addPostValue:@"en" forKey:@"lang"];
            //            [request addPostValue:[data1 valueForKey:@"email"] forKey:@"email"];
            //            [request addPostValue:[data1 valueForKey:@"gender"] forKey:@"gender"];
            //            [request addPostValue:[data1 valueForKey:@"family_name"] forKey:@"surname"];
            //            [request addPostValue:[data1 valueForKey:@"given_name"] forKey:@"name"];
            //
            //
            //
            //            if ([[data1 valueForKey:@"picture"] length]>0)
            //            {
            //                NSData *data=[NSData dataWithContentsOfURL:[NSURL URLWithString:[data1 valueForKey:@"picture"]]];
            //                [request addPostValue:[data base64Encoding] forKey:@"picture"];
            //            }
            //
            //
            //
            //           //
            //            [request startAsynchronous];
            
            
            
        } else {
            // fetch failed
            
        }
        
    }
    
}



- (void)requestFinished:(ASIHTTPRequest *)request
{
    
    
    NSError *error = [request error];
    if (!error)
    {
        [APPDELEGATE removeLoader];
        NSString *returnString = [request responseString];
        
        
        
        SBJsonParser1 *json=[[SBJsonParser1 alloc] init];
        
        NSMutableDictionary *resultDic=[json objectWithString:returnString];
        
     //   NSString *message=[resultDic objectForKey:@"msg"];
        if ([[resultDic valueForKey:@"sucess"]isEqualToString:@"2"]) {

//            [[NSUserDefaults standardUserDefaults]setObject:[[NSUserDefaults standardUserDefaults]valueForKey:LoginUser] forKey:LoginUserDetails];
//            [[NSUserDefaults standardUserDefaults]synchronize];
            
            SignupviewView *signup=[[SignupviewView alloc]init];
            [self.navigationController pushViewController:signup animated:YES];
            
        }
        if ([[resultDic valueForKey:KSucess]isEqualToString:@"1"]) {
            
            [[NSUserDefaults standardUserDefaults]setObject:[[resultDic valueForKey:@"data"]valueForKey:@"personal_logo"] forKey:@"logo_image"];
            [[NSUserDefaults standardUserDefaults]synchronize];

            
            
            [[NSUserDefaults standardUserDefaults]setValue:@"1" forKey:KLoginSuccess];
           

            [[NSUserDefaults standardUserDefaults]setObject:[resultDic valueForKey:KData] forKey:LoginUserDetails];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            Homeview *home=[[Homeview alloc]init];
            [self.navigationController pushViewController:home animated:YES];
            
            
            
            
            
            
            
        }
        
    }
    
}



//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//    if (alertView.tag==501) {
//        
//        
//        ForgotPassword *objForgotPassword=[[ForgotPassword alloc]init];
//        objForgotPassword.otp_value=otp_value;
//        objForgotPassword.Email_id=self.txt_forgetemail.text;
//        
//        [self.navigationController pushViewController:objForgotPassword animated:YES];
//        
//        
//        
//        
//    }
//}


#pragma mark web service
-(void)webHandlerManager:(Webservice *)webHandlerManager didFinishLoadingWithData:(NSObject *)responseObject
{
       [APPDELEGATE removeLoader];
    
    
   
    
    
    if ([webHandlerManager.strMethod isEqualToString:@"forgot_password"])
    {
        if([[responseObject valueForKey:@"sucess"]isEqualToString:@"1"])
        {
            
            
            otp_value=[responseObject valueForKey:@"otp"];
            
            
            [self.forgotpassview removeFromSuperview];
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:[responseObject valueForKey:@"msg"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
             alert.tag=501;
            
        }
        else
        {
            [self.forgotpassview removeFromSuperview];
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:[responseObject valueForKey:@"msg"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
           
            
            
        }
    }
    //user_login
    if ([webHandlerManager.strMethod isEqualToString:@"user_login"])
    {
        if([[responseObject valueForKey:@"sucess"]isEqualToString:@"1"])
        {
            
            [[NSUserDefaults standardUserDefaults]setObject:[[responseObject valueForKey:@"data"]valueForKey:@"personal_logo"] forKey:@"logo_image"];
            [[NSUserDefaults standardUserDefaults]synchronize];

            
            
            
            [[NSUserDefaults standardUserDefaults]setValue:@"1" forKey:KLoginSuccess];
            [[NSUserDefaults standardUserDefaults]setObject:[responseObject valueForKey:KData] forKey:LoginUserDetails];
            [[NSUserDefaults standardUserDefaults]synchronize];
            Homeview *home=[[Homeview alloc]init];
            [self.navigationController pushViewController:home animated:YES];

            
            
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:[responseObject valueForKey:@"msg"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];

         
            
        }
    }
}
-(void)webHandlerManager:(Webservice *)webHandlerManager didFailedLoadingWithError:(NSString *)responseError
{
    
    NSLog(@"%@",responseError);
    NSLog(@"%@",webHandlerManager.strMethod);
    
}
@end
