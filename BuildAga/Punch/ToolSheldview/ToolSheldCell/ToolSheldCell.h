//
//  ToolSheldCell.h
//  Punch
//
//  Created by Jaimish on 14/08/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ToolSheldCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *texttool;
@property (strong, nonatomic) IBOutlet UIImageView *imagetool;

@end
