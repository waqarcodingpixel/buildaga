
#import "ToolSheldViewContrller.h"
#import "ToolSheldCell.h"
@interface ToolSheldViewContrller ()
{
    NSMutableArray *toolshed;
    
}
@end

@implementation ToolSheldViewContrller

- (void)viewDidLoad
{
    toolshed=[[NSMutableArray alloc]init];
    toolshed=[[NSUserDefaults standardUserDefaults]valueForKey:@"toolshed"];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
//    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
//    [dic setObject:@"toolshed_list" forKey:@"method"];
//    [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"toolshed_list"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  [toolshed count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"ToolSheldCell";
    
    ToolSheldCell *cell1 = (ToolSheldCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier ];
    
    if (cell1 == nil)
    {
        cell1 = [[ToolSheldCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier ];
        
        
    }
    cell1.texttool.text = [[toolshed objectAtIndex:indexPath.row] valueForKey:@"description"];
    [cell1.texttool setNumberOfLines:0];
    [cell1.texttool sizeToFit];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                        NSUserDomainMask, YES);
    //
    //NSString *url = [NSString stringWithFormat:@"%@%@",AdThumbsImages,[[toolshed objectAtIndex:indexPath.row] valueForKey:@"image_name"]];
    NSString *filename =[NSString stringWithFormat:@"Ad_%@",[[toolshed objectAtIndex:indexPath.row] valueForKey:@"image_name"]];
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:filename];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    if (image==nil) {
        
        NSString *url = [NSString stringWithFormat:@"%@%@",AdThumbsImages,[[toolshed objectAtIndex:indexPath.row] valueForKey:@"image_name"]];
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            
            NSString *str=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *imagepath = [NSURL URLWithString:str];
            
            NSData *data = [NSData dataWithContentsOfURL:imagepath];
            UIImage *image = [UIImage imageWithData:data];
            [data writeToFile:path atomically:YES];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [cell1.imagetool setImage:image];
                
                
            });
        });
        
    }
    else
    {
        [cell1.imagetool setImage:image];
        
    }
    cell1.selectionStyle=UITableViewCellSelectionStyleNone;
    cell1.backgroundColor=[UIColor clearColor];
    return cell1;
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return  138;
}
- (IBAction)back_btn_click:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // {"method":"project_list","user_id":"4","login_user_id":"5"}
    
    NSURL *url = [NSURL URLWithString:[[toolshed objectAtIndex:indexPath.row] valueForKey:@"url"]];
    [[UIApplication sharedApplication] openURL:url];
    
}

@end
