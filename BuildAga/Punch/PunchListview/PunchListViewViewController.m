//
//  PunchListViewViewController.m
//  Punch
//
//  Created by Jaimish on 09/07/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import "PunchListViewViewController.h"
#import "PunchListCell.h"
#import "CommentsCell.h"
@interface PunchListViewViewController ()<UIAlertViewDelegate, PunchListCellDelegate>
{
    CGRect commenttableview;
    CGRect chatview;
    CGRect textview;
    NSMutableArray *commnets;
    NSInteger countpunch;
    CGFloat keyboard_height;
    UILabel *temp;
    int i,taskid;
    NSMutableArray *taskdetails;
    NSInteger indexpathrow;
}

@end

@implementation PunchListViewViewController

//-----------------------------------------------------------------------

#pragma mark - Action Method

- (IBAction)deletePunchTapped:(id)sender {

    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Buildaga" message:@"Are you sure you want to delete the punch?" delegate:self cancelButtonTitle:@"YES" otherButtonTitles:@"NO", nil];
    alert.tag = 100;
    [alert show];

}


//-----------------------------------------------------------------------

#pragma mark - Custom Methods

-(void)handleTap:(UITapGestureRecognizer *)tap
{

    [self.puncheditview removeFromSuperview];
    [self.taskedit removeFromSuperview];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==self.commentsview1.commenttable) {
        
        return [commnets count];
    }
    return 1;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView==self.commentsview1.commenttable) {
        
        return 1;
    }
    return [taskdetails count];
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==self.commentsview1.commenttable) {
        
        static NSString *cellIdentifier = @"CommentTableViewCell";
        
        CommentsCell *cell = (CommentsCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier ];
        if (cell == nil)
        {
            cell =[[CommentsCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier ];
            
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.comment.font=[UIFont fontWithName:@"OpenSans" size:12.0];
        //        cell.backgroundColor=[UIColor clearColor];
        //        cell.textLabel.text=[NSString stringWithFormat:@"I have to concede. There is now way using"];
        
        
        
        cell.commet.text=[[commnets valueForKey:@"comment"]objectAtIndex:indexPath.row];
        
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                             NSUserDomainMask, YES);
        //
        NSString *url = [NSString stringWithFormat:@"%@%@",UserProfileThumbs,[[commnets valueForKey:@"image"] objectAtIndex:indexPath.row]];
        NSString *filename =[NSString stringWithFormat:@"Profile_%@",[[commnets valueForKey:@"image"] objectAtIndex:indexPath.row] ];
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:filename];
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        if (image==nil) {
            
            NSString *url = [NSString stringWithFormat:@"%@%@",UserProfileThumbs,[[commnets valueForKey:@"image"] objectAtIndex:indexPath.row]];
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                
                NSString *str=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                NSURL *imagepath = [NSURL URLWithString:str];
                
                NSData *data = [NSData dataWithContentsOfURL:imagepath];
                UIImage *image = [UIImage imageWithData:data];
                [data writeToFile:path atomically:YES];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [cell.userimage setImage:image];
//                    [self.activityindicator stopAnimating];
//                    [self.activityindicator setHidden:YES];
                    
                });
            });
            
        }
        else
        {
             [cell.userimage setImage:image];
//            [self.activityindicator stopAnimating];
//            [self.activityindicator setHidden:YES];
            
        }
        
        
        
        
        
        
        
        
        
        //cell.userimage.image=[UIImage imageNamed:@"1.png"];
        cell.nameofuser.text=[[commnets valueForKey:@"name"] objectAtIndex:indexPath.row];
        
        
        //        cell.textLabel.textColor=[UIColor whiteColor];
        //       UIButton *button=[[UIButton alloc]initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width-70, 0,70 , 36)];
        //       [button setImage:[UIImage imageNamed:@"round_comment"] forState:UIControlStateNormal];
        
        //       cell.accessoryView=button;
        
        return cell;
    }
    else
    {
        static NSString *cellIdentifier = @"TableCell";
        
        PunchListCell *cell = (PunchListCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier ];
        if (cell == nil)
        {
            cell =[[PunchListCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier ];
            
        }
        cell.delegate = self;
        
        [cell.commentbtn addTarget:self action:@selector(commentview:) forControlEvents:UIControlEventTouchUpInside];
        cell.commentbtn.tag=indexPath.section;
        [cell.contentView.layer setBorderColor:[UIColor whiteColor].CGColor];
        [cell.contentView.layer setBorderWidth:1.0f];
        cell.backgroundColor=[UIColor clearColor];
        [cell.tickbtn addTarget:self action:@selector(Checktask:) forControlEvents:UIControlEventTouchUpInside];
       
        cell.tickbtn.tag=indexPath.section;
        //cell.tasknumber.text=[NSString stringWithFormat:@"Task%ld",indexPath.row+1];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if ([[[taskdetails objectAtIndex:indexPath.section]valueForKey:@"counter"] intValue]>0) {
            
            [cell.counterbtn setTitle:[NSString stringWithFormat:@"%@",[[taskdetails objectAtIndex:indexPath.section]valueForKey:@"counter"]] forState:UIControlStateDisabled];
            [cell.counterbtn setTitle:[NSString stringWithFormat:@"%@",[[taskdetails objectAtIndex:indexPath.section]valueForKey:@"counter"]] forState:UIControlStateNormal];
        }
        else
        {
            cell.counterbtn.hidden=TRUE;
        }
        cell.edittask.tag=indexPath.section;
        cell.deletetask.tag = indexPath.section;
        
        [cell.edittask addTarget:self action:@selector(taskedit:) forControlEvents:UIControlEventTouchUpInside];
        if ([[[taskdetails objectAtIndex:indexPath.section]valueForKey:@"is_complete"]isEqualToString:@"1"])
        {
            cell.tickbtn.selected=TRUE;
        }
        else
        {
            cell.tickbtn.selected=FALSE;
            
        }
        if ([[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails]valueForKey:@"role"]isEqualToString:@"CU"]) {
            
            cell.edittask.hidden=TRUE;
            cell.deletetask.hidden = TRUE;
            cell.tickbtn.userInteractionEnabled = FALSE;
            
//            if ([[[taskdetails objectAtIndex:indexPath.section]valueForKey:@"is_complete"]isEqualToString:@"1"])
//            {
//               
//                [cell.tickbtn setImage:nil forState:UIControlStateNormal];
//                   [cell.tickbtn setBackgroundImage:[UIImage imageNamed:@"checked"] forState:UIControlStateDisabled];
//            }
//            else
//            {
//               
//                 [cell.tickbtn setImage:[UIImage imageNamed:@"unchecked"] forState:UIControlStateDisabled];
//                
//            }
        }
        else
        {
            
        }
      
        cell.taskname.text=[[taskdetails objectAtIndex:indexPath.section] valueForKey:@"task_title"];
        cell.taskdetails.text=[[taskdetails objectAtIndex:indexPath.section] valueForKey:@"details"];
        return cell;
    }
    
}

-(void)taskedit:(UIButton *)sender
{
    self.taskedit.frame=CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    self.tasktitle.leftView = paddingView;
    self.tasktitle.leftViewMode = UITextFieldViewModeAlways;
    self.tasktitle.text=[[taskdetails objectAtIndex:[sender tag]] valueForKey:@"task_title"];
    taskid=(int)[sender tag];
    self.taskdetail.text=[[taskdetails objectAtIndex:[sender tag]] valueForKey:@"details"];
     self.edittaskokbtn.tag=1000;
    [self.view addSubview:self.taskedit];
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if (tableView==self.commentsview1.commenttable) {
        
        // NSIndexPath *indexPath = [NSIndexPath indexPathForRow:indexPath inSection:0];
        
        //        CommentsCell *cell=(CommentsCell *)[self.commentsview1.commenttable cellForRowAtIndexPath:indexPath];
        temp=nil;
        temp=[[UILabel alloc]init];
        temp.font=[UIFont fontWithName:@"OpenSans" size:13.0];
        temp.text=[[commnets valueForKey:@"comment"] objectAtIndex:indexPath.row];
        
        if ([self resizeMessageLabel:temp].height<80) {
            return 90;
        }
        else
        {
            return [self resizeMessageLabel:temp].height;
        }
    }
    
    return 210.0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    
    if (tableView==self.commentsview1.commenttable) {
        return 0.0;
    }
    else
    {
        return 10.0;
    }
    
}
- (IBAction)back_btn_click:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
{
    
    if ([alertView tag]==1001) {
        
        if (buttonIndex==0) {
          
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:indexpathrow];
            
            PunchListCell *cell=(PunchListCell *)[self.tableview cellForRowAtIndexPath:indexPath];
            
            if (cell.tickbtn.selected) {
                
                
                // {"method":"task_status","task_id":"2","status":"1","user_id":"2"}
                NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                [dic setObject:@"task_status" forKey:@"method"];
                [dic setObject:@"0" forKey:@"status"];
                [dic setObject:[[taskdetails objectAtIndex:indexpathrow] valueForKey:@"id"] forKey:@"task_id"];
                [dic setObject:[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails] valueForKey:@"id"] forKey:@"user_id"];
                [APPDELEGATE addLoader:nil];
                [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"task_status"];
              
                
            }
            else
            {
                
                //{"method":"punch_status","punch_id":"1","status":"1","user_id":"2"}
                NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                [dic setObject:@"task_status" forKey:@"method"];
                [dic setObject:@"1" forKey:@"status"];
                [dic setObject:[[taskdetails objectAtIndex:indexpathrow] valueForKey:@"id"] forKey:@"task_id"];
                [dic setObject:[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails] valueForKey:@"id"] forKey:@"user_id"];
                [APPDELEGATE addLoader:nil];
                [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"task_status"];
            }
        }
    }
 
    if ([alertView tag]==1002) {
        
        if (buttonIndex==0) {
            if (self.updatepunchbtn.selected) {
                NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                [dic setObject:@"punch_status" forKey:@"method"];
                [dic setObject:@"0" forKey:@"status"];
                [dic setObject:[[[NSUserDefaults standardUserDefaults]valueForKey:@"punch_id"] valueForKey:@"id"] forKey:@"punch_id"];
                //[dic setObject:[[NSUserDefaults standardUserDefaults]valueForKey:@"punch_id"] forKey:@"punch_id"];
                [dic setObject:[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails] valueForKey:@"id"] forKey:@"user_id"];
                [APPDELEGATE addLoader:nil];
                [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"punch_status"];
               
            }
            else
            {
                NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                [dic setObject:@"punch_status" forKey:@"method"];
                [dic setObject:@"1" forKey:@"status"];
                [dic setObject:[[[NSUserDefaults standardUserDefaults]valueForKey:@"punch_id"] valueForKey:@"id"] forKey:@"punch_id"];
                [dic setObject:[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails] valueForKey:@"id"] forKey:@"user_id"];
                [APPDELEGATE addLoader:nil];
                [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"punch_status"];
            }
        }
    }
    
    if (alertView.tag == 100)
    {
        if (buttonIndex == 0)
        {
            NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
            [dic setObject:@"delete_punch" forKey:@"method"];
            [dic setObject:[[[NSUserDefaults standardUserDefaults]valueForKey:@"punch_id"] valueForKey:@"id"] forKey:@"punch_id"];
            [APPDELEGATE addLoader:nil];
            [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"delete_punch"];
        }
    }
    
    if (alertView.tag == 200)
    {
        if (buttonIndex == 0)
        {
            NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
            [dic setObject:@"delete_task" forKey:@"method"];
            
            [dic setObject:[[taskdetails objectAtIndex:taskid] valueForKey:@"id"] forKey:@"task_id"];
            //[dic setObject:[[[NSUserDefaults standardUserDefaults] valueForKey:LoginUserDetails] valueForKey:@"id"] forKey:@"user_id"];
            [APPDELEGATE addLoader:nil];
            [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"delete_task"];
        }
    }
    
}

-(void)Checktask:(UIButton *)btn
{
    
  //  Are you sure you want to complete this task?
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:indexpathrow];
    
    PunchListCell *cell=(PunchListCell *)[self.tableview cellForRowAtIndexPath:indexPath];
    if (cell.tickbtn.selected) {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:@"Are you sure you would like to reopen this task?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
        alert.tag=1001;
        indexpathrow=btn.tag;
        [alert show];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:@"Are you sure you want to complete this task?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
        alert.tag=1001;
        indexpathrow=btn.tag;
        [alert show];
    }
   
    
   
    
    
}
-(void)close_btn_click:(UIButton *)sender
{
    [self.commentsview1 removeFromSuperview];
}
-(void)commentview:(UIButton *)sender
{
    i=0;
    

    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:sender.tag];
    PunchListCell *cell=(PunchListCell *)[self.tableview cellForRowAtIndexPath:indexPath];
    cell.counterbtn.hidden=TRUE;
    self.commentsview1=[[CommentsView alloc]initWithFrame:self.view.frame];
    [self.view addSubview:self.commentsview1];
    self.commentsview1.chattextview.delegate=self;
    [ self.commentsview1.sendbutton addTarget:self action:@selector(send:) forControlEvents:UIControlEventTouchUpInside];
    [ self.commentsview1.closebtn addTarget:self action:@selector(close_btn_click:) forControlEvents:UIControlEventTouchUpInside];
    self.commentsview1.sendbutton.tag=sender.tag;
     self.commentsview1.texview.text=[[taskdetails objectAtIndex:sender.tag] valueForKey:@"details"];
       self.commentsview1.tasktitle.text=[[taskdetails objectAtIndex:sender.tag] valueForKey:@"task_title"];
    self.commentsview1.detailTask.text=[[taskdetails objectAtIndex:sender.tag] valueForKey:@"details"];
    self.commentsview1.detailview.frame=CGRectMake(0, 0, self.commentsview1.detailview.frame.size.width, 130+[self resizeMessageLabel:self.commentsview1.detailTask].height);
    self.commentsview1.commenttable.tableHeaderView=self.commentsview1.detailview;
    self.commentsview1.commenttable.delegate=self;
    self.commentsview1.commenttable.dataSource=self;
    [self performSelector:@selector(maindelay) withObject:self afterDelay:1];
   // {"method":"comment_list","task_id":"1"}
    
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    
    [dic setObject:@"comment_list" forKey:@"method"];
       [dic setObject:[[[NSUserDefaults standardUserDefaults] valueForKey:LoginUserDetails] valueForKey:@"id"] forKey:@"user_id"];
    [dic setObject:[[taskdetails objectAtIndex:sender.tag] valueForKey:@"id"] forKey:@"task_id"];
    [APPDELEGATE addLoader:nil];
    [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"comment_list"];
    
}
-(void)maindelay
{
    commenttableview=self.commentsview1.commenttable.frame;
    chatview=self.commentsview1.chatview.frame;
    textview=self.commentsview1.chattextview.frame;
    
}
- (IBAction)checked_punch:(UIButton *)sender {
    
    //{"method":"punch_status","punch_id":"1","status":"1","user_id":"2"}
    
    
   
    
  
    if (sender.selected) {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:@"Are you sure you want to incomplete all tasks?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
        alert.tag=1002;
        indexpathrow=sender.tag;
        [alert show];
//        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
//        [dic setObject:@"punch_status" forKey:@"method"];
//        [dic setObject:@"0" forKey:@"status"];
//        [dic setObject:[[[NSUserDefaults standardUserDefaults]valueForKey:@"punch_id"] valueForKey:@"id"] forKey:@"punch_id"];
//        //[dic setObject:[[NSUserDefaults standardUserDefaults]valueForKey:@"punch_id"] forKey:@"punch_id"];
//        [dic setObject:[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails] valueForKey:@"id"] forKey:@"user_id"];
//        [APPDELEGATE addLoader:nil];
//        [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"punch_status"];
//       sender.selected=FALSE;
    }
    else
    {
    
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:@"Are you sure you want to complete all tasks?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
        alert.tag=1002;
        indexpathrow=sender.tag;
        [alert show];
//        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
//        [dic setObject:@"punch_status" forKey:@"method"];
//        [dic setObject:@"1" forKey:@"status"];
//         [dic setObject:[[[NSUserDefaults standardUserDefaults]valueForKey:@"punch_id"] valueForKey:@"id"] forKey:@"punch_id"];
//        [dic setObject:[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails] valueForKey:@"id"] forKey:@"user_id"];
//        [APPDELEGATE addLoader:nil];
//        [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"punch_status"];
//        sender.selected=TRUE;
    
    }
    
    
}


#pragma mark labelSize
- (CGSize) resizeMessageLabel:(UILabel *)lblMessage
{
    CGSize maxLabelSize = CGSizeMake([[UIScreen mainScreen] bounds].size.width-20,9999);
    
    CGSize expectedLabelSize = [lblMessage.text sizeWithFont:lblMessage.font
                                           constrainedToSize:maxLabelSize
                                               lineBreakMode:lblMessage.lineBreakMode];
    expectedLabelSize.height=expectedLabelSize.height+20;
    
    //adjust the label the the new height.
    //    CGRect newFrame = lblMessage.frame;
    //    newFrame.size.height = expectedLabelSize.height;
    //    lblMessage.frame = newFrame;
    //    viewMessage.frame = CGRectMake(viewMessage.frame.origin.x, viewMessage.frame.origin.y, viewMessage.frame.size.width, lblMessage.frame.size.height);
    
    return expectedLabelSize;
}
-(void)send:(UIButton *)sendbutton
{
    i=0;
    
    self.commentsview1.chatview.frame=chatview;
    self.commentsview1.chattextview.frame=textview;
    self.commentsview1.commenttable.frame=commenttableview;
    
    //    self.commentsview1.chattextview.frame=CGRectMake(self.commentsview1.chatview.frame.origin.x,self.commentsview1.commenttable.frame.origin.y+self.commentsview1.commenttable.frame.size.height+5,self.commentsview1.chatview.frame.size.width,38);
    //    self.commentsview1.chatview.frame = CGRectMake(self.commentsview1.chatview.frame.origin.x,self.commentsview1.commenttable.frame.origin.y+self.commentsview1.commenttable.frame.size.height,self.commentsview1.chatview.frame.size.width,43);
    
    if ([self.commentsview1.chattextview.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0 || [self.commentsview1.chattextview.text isEqualToString:@"Write here..."] ) {
        
        

        self.commentsview1.chattextview.text=@"Write here...";
        [self.commentsview1.chattextview endEditing:TRUE];
        return;
    }
    else
    {
        
        NSString *comment=self.commentsview1.chattextview.text;
       // {"method":"give_comment","task_id":"1","comment":"please complete task fast","user_id":"5"}
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        
        [dic setObject:@"give_comment" forKey:@"method"];
        
        [dic setObject:[[taskdetails objectAtIndex:sendbutton.tag] valueForKey:@"id"] forKey:@"task_id"];
         [dic setObject:[[[NSUserDefaults standardUserDefaults] valueForKey:LoginUserDetails] valueForKey:@"id"] forKey:@"user_id"];
        [dic setObject:comment forKey:@"comment"];
        [APPDELEGATE addLoader:nil];
        [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"give_comment"];
        // comment = sfsdjfhksjafhaldksjfhlaskdjfhlaskjfhaslkdjfhsaldkfjhsalfjkhsaldkfjhsaldkfjhsalkfjhsaldkfjhsadklfjhasklfjhaskldjfhsakldfjhaskldfjhsakldfjhsakljdfhajksfhalksjfhalskdjfhaskljdfhsdjkfhskdjfhaskjfhklajsdhfjklsahfklsajdhfjklsdhfklasjfhalskjfhaklsjdfhaklsjfhaskldfhajklsfh;
//        id = 102;
//        image = "1438667571.png";
//        insertdate = "2015-08-04 09:47:49";
//        name = john;
//        "task_id" = 9;
//        "user_id" = 8;
        NSMutableDictionary *tempcomment=[[NSMutableDictionary alloc]init];
        [tempcomment  setValue:comment forKey:@"comment"];
        [tempcomment setValue:[[[NSUserDefaults standardUserDefaults] valueForKey:LoginUserDetails] valueForKey:@"image"] forKey:@"image"];
        [tempcomment setValue:[[[NSUserDefaults standardUserDefaults] valueForKey:LoginUserDetails] valueForKey:@"name"] forKey:@"name"];
        [commnets insertObject:tempcomment atIndex:[commnets count]];
        [self.commentsview1.commenttable beginUpdates];
        NSIndexPath *myIP = [NSIndexPath indexPathForRow:([commnets count]-1) inSection:0];
        NSArray *myArray = [[NSArray alloc] initWithObjects:myIP, nil];
        [self.commentsview1.commenttable insertRowsAtIndexPaths:myArray withRowAnimation:UITableViewRowAnimationBottom];
        
        //[self.mytableview deleteRowsAtIndexPaths:path withRowAnimation:UITableViewRowAnimationBottom];
        [self.commentsview1.commenttable endUpdates];
        
        

        
        self.commentsview1.chattextview.text=@"Write here...";
    }
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[commnets count]-1 inSection:0];
    [self.commentsview1.commenttable scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    [self.commentsview1.chattextview endEditing:TRUE];
    self.commentsview1.chattextview.textAlignment=NSTextAlignmentCenter;

    self.commentsview1.chattextview.text=@"Write here...";
    
    
    
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if (textView.tag==8000) {
        if ([commnets count]>0) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[commnets count]-1 inSection:0];
            [self.commentsview1.commenttable scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
            
        }
         textView.text=@"";
    }
    
   

    return YES;
    
}
- (IBAction)back_btn_cllick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)keyboardShow:(NSNotification *)notification
{
    NSLog(@"%f", [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height);
    keyboard_height = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    
    //  self.commentsview1.chatview.frame = CGRectMake(self.commentsview1.chatview.frame.origin.x, [[UIScreen mainScreen] bounds].size.height- self.commentsview1.chatview.frame.size.height,self.commentsview1.chatview.frame.size.width, self.commentsview1.chatview.frame.size.height);
}

- (void)keyboardHide:(NSNotification *)notification
{
    
    
    
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    
    if (textView.tag==8000) {
          [self adjustFrames];
    }
    
  
    
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    
    if (textField.text.length==1) {
        textField.text = [[[textField.text substringToIndex:1] uppercaseString] stringByAppendingString:[textField.text substringFromIndex:1]];
    }
    
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    
    if (textField.text.length==1) {
        textField.text = [[[textField.text substringToIndex:1] uppercaseString] stringByAppendingString:[textField.text substringFromIndex:1]];
    }
    
    return YES;
    
}
-(void) adjustFrames
{
    //    CGRect textFrame = self.commentsview1.chattextview.frame;
    //
    //
    //    NSLog(@"%f",textFrame.size.height);
    //
    //
    //    if (i<10) {
    //        if (textFrame.size.height< self.commentsview1.chattextview.contentSize.height) {
    //
    //
    //            self.commentsview1.chatview.frame = CGRectMake(self.commentsview1.chatview.frame.origin.x,self.commentsview1.chatview.frame.origin.y+self.commentsview1.chatview.frame.size.height-self.commentsview1.chattextview.contentSize.height-5, self.commentsview1.chatview.frame.size.width,  self.commentsview1.chattextview.contentSize.height+5);
    //            textFrame.size.height =  self.commentsview1.chattextview.contentSize.height;
    //            self.commentsview1.chattextview.frame = textFrame;
    //            i++;
    //        }
    //
    //
    //    }
    //
    //
    //    //
    //    NSLog(@"%f",textFrame.size.height);
    [self performSelector:@selector(delay123) withObject:self afterDelay:0.5];
}

-(void)delay123
{
    CGRect textFrame = self.commentsview1.chattextview.frame;
    
    
    NSLog(@"%f",self.commentsview1.chattextview.contentSize.height);
    
    self.commentsview1.commenttable.translatesAutoresizingMaskIntoConstraints=YES;
    
    
    if ((textFrame.size.height-self.commentsview1.chattextview.contentSize.height)>20) {
        self.commentsview1.commenttable.frame=CGRectMake(self.commentsview1.commenttable.frame.origin.x, self.commentsview1.commenttable.frame.origin.y+(textFrame.size.height-self.commentsview1.chattextview.contentSize.height),self.commentsview1.commenttable.frame.size.width ,self.commentsview1.commenttable.frame.size.height);
        
        
        if (self.commentsview1.chattextview.contentSize.height < textFrame.size.height && self.commentsview1.chattextview.contentSize.height<150) {
            
            
            self.commentsview1.chatview.translatesAutoresizingMaskIntoConstraints=YES;
            self.commentsview1.chattextview.translatesAutoresizingMaskIntoConstraints=YES;
            self.commentsview1.chatview.frame = CGRectMake(self.commentsview1.chatview.frame.origin.x,self.commentsview1.chatview.frame.origin.y+self.commentsview1.chatview.frame.size.height-self.commentsview1.chattextview.contentSize.height-5, self.commentsview1.chatview.frame.size.width,  self.commentsview1.chattextview.contentSize.height+5);
            textFrame.size.height =  self.commentsview1.chattextview.contentSize.height;
            self.commentsview1.chattextview.translatesAutoresizingMaskIntoConstraints=YES;
            
            self.commentsview1.chattextview.frame=CGRectMake(self.commentsview1.chattextview.frame.origin.x,(self.commentsview1.chattextview.frame.origin.y+self.commentsview1.chattextview.contentSize.height)-self.commentsview1.chattextview.contentSize.height, self.commentsview1.chattextview.frame.size.width,  self.commentsview1.chattextview.contentSize.height);
            
            
            // self.commentsview1.chattextview.frame = textFrame;
            // [self.commentsview1.chattextview sizeToFit];
            
        }
        
        
        
        
    }
    else
    {
        if ((self.commentsview1.chattextview.contentSize.height-textFrame.size.height)>20 && self.commentsview1.chattextview.contentSize.height<150) {
            //         self.commentsview1.commenttable.frame=CGRectMake(self.commentsview1.commenttable.frame.origin.x, self.commentsview1.commenttable.frame.origin.y, self.commentsview1.commenttable.frame.size.width,self.commentsview1.commenttable.frame.size.height-(self.commentsview1.chattextview.contentSize.height-textFrame.size.height)-6);
            self.commentsview1.commenttable.frame=CGRectMake(self.commentsview1.commenttable.frame.origin.x, self.commentsview1.commenttable.frame.origin.y-(self.commentsview1.chattextview.contentSize.height-textFrame.size.height),self.commentsview1.commenttable.frame.size.width ,self.commentsview1.commenttable.frame.size.height);
        }
        
        
        
        //    if (i<10) {
        if (textFrame.size.height< self.commentsview1.chattextview.contentSize.height && self.commentsview1.chattextview.contentSize.height<150) {
            
            
            self.commentsview1.chatview.translatesAutoresizingMaskIntoConstraints=YES;
            self.commentsview1.chattextview.translatesAutoresizingMaskIntoConstraints=YES;
            self.commentsview1.chatview.frame = CGRectMake(self.commentsview1.chatview.frame.origin.x,self.commentsview1.chatview.frame.origin.y+self.commentsview1.chatview.frame.size.height-self.commentsview1.chattextview.contentSize.height-5, self.commentsview1.chatview.frame.size.width,  self.commentsview1.chattextview.contentSize.height+5);
            textFrame.size.height =  self.commentsview1.chattextview.contentSize.height;
            self.commentsview1.chattextview.translatesAutoresizingMaskIntoConstraints=YES;
            
            self.commentsview1.chattextview.frame=CGRectMake(self.commentsview1.chattextview.frame.origin.x,(self.commentsview1.chattextview.frame.origin.y+self.commentsview1.chattextview.contentSize.height)-self.commentsview1.chattextview.contentSize.height, self.commentsview1.chattextview.frame.size.width,  self.commentsview1.chattextview.contentSize.height);
            
            
            // self.commentsview1.chattextview.frame = textFrame;
            // [self.commentsview1.chattextview sizeToFit];
            
        }
    }
}
- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    if (action == @selector(paste:) ||action == @selector(copy:))
    {
        return [super canPerformAction:action withSender:sender];
    }
    
    
    return NO;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    
    [self.commentsview1 removeFromSuperview];
//    [self.puncheditview removeFromSuperview];
//    [self.taskedit removeFromSuperview];
    
    
}
- (IBAction)punchedit_ok:(id)sender {
    //  {"method":"edit_punch","punch_id": "2","punch_title": "punch 11"}
    
    if ([self.punchnamedit.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0 ){
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message: @"Please enter punch name" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        
        
        return;
        
        
    }
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    [dic setObject:@"edit_punch" forKey:@"method"];
    [dic setObject:[[[NSUserDefaults standardUserDefaults]valueForKey:@"punch_id"] valueForKey:@"id"] forKey:@"punch_id"];
    [dic setObject:self.punchnamedit.text forKey:@"punch_title"];
    [APPDELEGATE addLoader:nil];
    [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"edit_punch"];
}

- (IBAction)edit_punch:(id)sender {
    
    self.puncheditview.frame=CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    self.punchnamedit.leftView = paddingView;
    self.punchnamedit.leftViewMode = UITextFieldViewModeAlways;
    self.punchnamedit.text=self.punchname.text;
    [self.view addSubview:self.puncheditview];
    
    
    
    
}
- (IBAction)edittask_btn_click:(id)sender {
    
    
    if([sender tag]==1000)
    {
    if ([self.tasktitle.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0 ){
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message: @"Please enter task title" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        
        
        return;
        
        
    }
    //  {"method":"edit_task","task_id":"1","task_title":"task1","details":"abcde"}
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    [dic setObject:@"edit_task" forKey:@"method"];
    
    [dic setObject:[[taskdetails objectAtIndex:taskid] valueForKey:@"id"] forKey:@"task_id"];
    [dic setObject:self.tasktitle.text forKey:@"task_title"];
    [dic setObject:self.taskdetail.text forKey:@"details"];
        [dic setObject:[[[NSUserDefaults standardUserDefaults] valueForKey:LoginUserDetails] valueForKey:@"id"] forKey:@"user_id"];
    [APPDELEGATE addLoader:nil];
    [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"edit_task"];
        
    }
    else
    {
        //{"method":"add_task","punch_id": "1","task_title": "task new","details":"abcdefg"}
        
        if ([self.tasktitle.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0 ){
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message: @"Please enter task title" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            
            
            return;
            
            
        }
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        [dic setObject:@"add_task" forKey:@"method"];
        
        
      [dic setObject:[[[NSUserDefaults standardUserDefaults]valueForKey:@"punch_id"] valueForKey:@"id"] forKey:@"punch_id"];    
        [dic setObject:self.tasktitle.text forKey:@"task_title"];
           [dic setObject:[[[NSUserDefaults standardUserDefaults] valueForKey:LoginUserDetails] valueForKey:@"id"] forKey:@"user_id"];
        [dic setObject:self.taskdetail.text forKey:@"details"];
        [APPDELEGATE addLoader:nil];
        [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"add_task"];
    
    }
}


- (IBAction)addtask_btn_click:(id)sender {
    
    self.taskedit.frame=CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
      UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    self.tasktitle.leftView = paddingView;
    self.punchnamedit.leftViewMode = UITextFieldViewModeAlways;
    self.edittaskokbtn.tag=2000;
    self.tasktitle.text=@"";
    
    self.taskdetail.text=@"";
    [self.view addSubview:self.taskedit];
    
}


#pragma mark web service
-(void)webHandlerManager:(Webservice *)webHandlerManager didFinishLoadingWithData:(NSObject *)responseObject
{
    [APPDELEGATE removeLoader];
    if ([webHandlerManager.strMethod isEqualToString:@"comment_list"])
    {
        if([[responseObject valueForKey:@"sucess"]isEqualToString:@"1"])
        {
            
            [commnets removeAllObjects];
            commnets=[responseObject valueForKey:@"comment_data"];
            [self.commentsview1.commenttable reloadData];
           
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                    [dic setObject:@"task_list" forKey:@"method"];
                    [dic setObject:[[[NSUserDefaults standardUserDefaults] valueForKey:LoginUserDetails] valueForKey:@"id"] forKey:@"user_id"];
                    [dic setObject:[[[NSUserDefaults standardUserDefaults]valueForKey:@"punch_id"] valueForKey:@"id"] forKey:@"punch_id"];
                  
                    [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"task_list"];
                    //                    [self.activityindicator stopAnimating];
                    //                    [self.activityindicator setHidden:YES];
                    
                });
            });
          
//             self.commentsview1.detailTask.text=[[taskdetails objectAtIndex:sender.tag] valueForKey:@"details"];
//            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Punch List" message:[responseObject valueForKey:@"msg"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//            [alert show];
//            
//            if ([[[responseObject valueForKey:@"punch_data"] valueForKey:@"is_complete"]isEqualToString:@"1"]) {
//                self.updatepunchbtn.selected=TRUE;
//            }
//            else
//            {
//                self.updatepunchbtn.selected=FALSE;
//            }
            
            
            
        }
        else
        {
            
//            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Punch List" message:[responseObject valueForKey:@"msg"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//            [alert show];
            
            
        }
    }
    if ([webHandlerManager.strMethod isEqualToString:@"give_comment"])
    {
        if([[responseObject valueForKey:@"sucess"]isEqualToString:@"1"])
        {
            [self.commentsview1 endEditing:YES];
        }
        else
        {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:[responseObject valueForKey:@"msg"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            
             [self.commentsview1 endEditing:YES];
        }
    }
    if ([webHandlerManager.strMethod isEqualToString:@"punch_status"])
    {
        if([[responseObject valueForKey:@"sucess"]isEqualToString:@"1"])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:[responseObject valueForKey:@"msg"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            taskdetails = [responseObject valueForKey:@"task_details"];
             [self.tableview reloadData];
            
          
                if (self.updatepunchbtn.selected) {
                    self.updatepunchbtn.selected=FALSE;
                }
                else
                {
                     self.updatepunchbtn.selected=TRUE;
                }
            
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:[responseObject valueForKey:@"msg"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
    }
    
    if ([webHandlerManager.strMethod isEqualToString:@"task_status"])
    {
        if([[responseObject valueForKey:@"sucess"]isEqualToString:@"1"])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:[responseObject valueForKey:@"msg"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
           NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:indexpathrow];
            PunchListCell *cell=(PunchListCell *)[self.tableview cellForRowAtIndexPath:indexPath];
            if (cell.tickbtn.selected)
            {
                cell.tickbtn.selected=FALSE;
            
            }
            else
            {
             cell.tickbtn.selected=TRUE;
            
            }
            
            if ([[[responseObject valueForKey:@"punch_data"] valueForKey:@"is_complete"]isEqualToString:@"1"]) {
                  self.updatepunchbtn.selected=TRUE;
            }
            else
            {
              self.updatepunchbtn.selected=FALSE;
            }
            
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:[responseObject valueForKey:@"msg"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            
            
        }
    }
    if ([webHandlerManager.strMethod isEqualToString:@"task_list"])
    {
        if([[responseObject valueForKey:@"sucess"]isEqualToString:@"1"])
        {
            self.punchname.text=[[responseObject valueForKey:KData] valueForKey:@"punch_title"];
            self.punchheadertitle.text=[[responseObject valueForKey:KData] valueForKey:@"punch_title"];
            
            taskdetails=[responseObject valueForKey:@"task_data"];
            [self.tableview reloadData];
        }
        else
        {
            
        }
    }
    
    if ([webHandlerManager.strMethod isEqualToString:@"add_task"])
    {
        if([[responseObject valueForKey:@"sucess"]isEqualToString:@"1"])
        {
          
            if ([[[responseObject valueForKey:@"punch_data"] valueForKey:@"is_complete"]isEqualToString:@"1"]) {
                self.updatepunchbtn.selected=TRUE;
            }
            else
            {
                self.updatepunchbtn.selected=FALSE;
            }
            [self.taskedit removeFromSuperview];
            
            NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
            [dic setObject:@"task_list" forKey:@"method"];
            [dic setObject:[[[NSUserDefaults standardUserDefaults]valueForKey:@"punch_id"] valueForKey:@"id"] forKey:@"punch_id"];;
            [dic setObject:[[[NSUserDefaults standardUserDefaults] valueForKey:LoginUserDetails] valueForKey:@"id"] forKey:@"user_id"];
            [APPDELEGATE addLoader:nil];
            [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"task_list"];
            
        }
        else
        {
            
        }
    }
    if ([webHandlerManager.strMethod isEqualToString:@"edit_task"])
    {
        if([[responseObject valueForKey:@"sucess"]isEqualToString:@"1"])
        {
            // taskdetails=[responseObject valueForKey:@"task_data"];
            [taskdetails replaceObjectAtIndex:taskid withObject:[responseObject valueForKey:KData]];
            
            NSIndexSet *sections = [[NSIndexSet alloc] initWithIndex:taskid];
            [self.tableview reloadSections:sections withRowAnimation:UITableViewRowAnimationNone];
            
            
            [self.taskedit removeFromSuperview];
            
        }
        else
        {
            [self.taskedit removeFromSuperview];
        }
    }
    if ([webHandlerManager.strMethod isEqualToString:@"edit_punch"])
    {
        if([[responseObject valueForKey:@"sucess"]isEqualToString:@"1"])
        {
            self.punchname.text=[[responseObject valueForKey:KData] valueForKey:@"punch_title"];
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Buildaga" message:[responseObject valueForKey:@"msg"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            
            [self.puncheditview removeFromSuperview];
        }
    }
    
    if ([webHandlerManager.strMethod isEqualToString:@"delete_punch"])
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Buildaga" message:[responseObject valueForKey:@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        if ([[responseObject valueForKey:@"sucess"]isEqualToString:@"1"]) {
          
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    
    if ([webHandlerManager.strMethod isEqualToString:@"delete_task"])
    {
        if ([[responseObject valueForKey:@"sucess"]isEqualToString:@"1"]) {
            
             NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
            [dic setObject:@"task_list" forKey:@"method"];
            [dic setObject:[[[NSUserDefaults standardUserDefaults]valueForKey:@"punch_id"] valueForKey:@"id"] forKey:@"punch_id"];;
            [dic setObject:[[[NSUserDefaults standardUserDefaults] valueForKey:LoginUserDetails] valueForKey:@"id"] forKey:@"user_id"];
            [APPDELEGATE addLoader:nil];
            [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"task_list"];
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Buildaga" message:[responseObject valueForKey:@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            
        } else {
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Buildaga" message:[responseObject valueForKey:@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }
}

-(void)webHandlerManager:(Webservice *)webHandlerManager didFailedLoadingWithError:(NSString *)responseError
{
    
    NSLog(@"%@",responseError);
    NSLog(@"%@",webHandlerManager.strMethod);
    
}

//-----------------------------------------------------------------------

#pragma mark - PunchListCellDelegate 

- (void)deleteTaskButtonTapped:(UIButton *)sender {

    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Buildaga" message:@"Are you sure want to delete this task?" delegate:self cancelButtonTitle:@"YES" otherButtonTitles:@"NO", nil];
    alert.tag = 200;
    taskid=(int)[sender tag];
    [alert show];
}

//-----------------------------------------------------------------------

#pragma mark - UIView Life Cycle Metho

- (void)viewDidLoad {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardShow:) name:UIKeyboardWillChangeFrameNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardHide:) name:UIKeyboardDidHideNotification object:nil];
    self.tableview.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    commnets=[[NSMutableArray alloc]init];
    countpunch=2;
    self.punchheadertitle.adjustsFontSizeToFitWidth=TRUE;
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    if ([[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails]valueForKey:@"role"]isEqualToString:@"CU"]) {
        self.editpunchbtn.hidden=TRUE;
        self.deletepunchbtn.hidden = TRUE;
        self.updatepunchbtn.userInteractionEnabled=FALSE;
        
    }
    else
    {
    }
    taskdetails=[[NSMutableArray alloc]init];
    i=0;
    //{"method":"task_list","punch_id":"1"}
    if ([[[[NSUserDefaults standardUserDefaults]valueForKey:@"punch_id"]valueForKey:@"is_complete"]isEqualToString:@"1"]) {
        
        self.updatepunchbtn.selected=TRUE;
    }
    else
    {
        self.updatepunchbtn.selected=FALSE;
    }
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    [dic setObject:@"task_list" forKey:@"method"];
    [dic setObject:[[[NSUserDefaults standardUserDefaults] valueForKey:LoginUserDetails] valueForKey:@"id"] forKey:@"user_id"];
    [dic setObject:[[[NSUserDefaults standardUserDefaults]valueForKey:@"punch_id"] valueForKey:@"id"] forKey:@"punch_id"];
    
    [APPDELEGATE addLoader:nil];
    [APPDELEGATE callWebservice:dic andconrtoller:self methodName:@"task_list"];
    
    UITapGestureRecognizer *tapping=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTap:)];
    [self.edittaskbg addGestureRecognizer:tapping];
    UITapGestureRecognizer *tapping1=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTap:)];
    [self.editpunchbg addGestureRecognizer:tapping1];
    
    
    NSLog(@"Role value ----> %@",[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails]valueForKey:@"role"]);
    
    
//    if ([[[[NSUserDefaults standardUserDefaults]valueForKey:LoginUserDetails]valueForKey:@"role"]isEqualToString:@"CO"]) {
//        
//        self.addtask.hidden=false;
//
//        //self.addtask.hidden=false;
//        
//        
//    }
//    else
//    {
//        
//         self.addtask.hidden=true;
//        
//       
//        
//        
//        
//    }
//    

    
}

@end
