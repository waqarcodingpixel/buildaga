//
//  PunchListViewViewController.h
//  Punch
//
//  Created by Jaimish on 09/07/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommentsView.h"

@interface PunchListViewViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate,UITextFieldDelegate,UIAlertViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableview;
@property (strong, nonatomic) IBOutlet UILabel *punchname;
@property (strong, nonatomic) IBOutlet CommentsView  *commentsview1;

@property (strong, nonatomic) IBOutlet UIView *puncheditview;
@property (strong, nonatomic) IBOutlet UITextField *punchnamedit;
@property (strong, nonatomic) IBOutlet UIView *taskedit;
@property (strong, nonatomic) IBOutlet UITextField *tasktitle;
@property (strong, nonatomic) IBOutlet UITextView *taskdetail;
@property (strong, nonatomic) IBOutlet UIButton *editpunchbtn,*deletepunchbtn;
@property (strong, nonatomic) IBOutlet UIButton *addtask;

@property (strong, nonatomic) IBOutlet UIButton *edittaskokbtn;

@property (strong, nonatomic) IBOutlet UIImageView *editpunchbg;
@property (strong, nonatomic) IBOutlet UIImageView *edittaskbg;
@property (strong, nonatomic) IBOutlet UILabel *punchheadertitle;
@property (strong, nonatomic) IBOutlet UIButton *updatepunchbtn;

- (IBAction)deletePunchTapped:(id)sender;
- (void)deleteTaskTapped:(id)sender;

@end
