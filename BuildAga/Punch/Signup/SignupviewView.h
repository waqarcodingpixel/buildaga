//
//  SignupviewViewController.h
//  Punch
//
//  Created by Jaimish on 01/07/15.
//  Copyright (c) 2015 Hyperlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignupviewView : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UITextViewDelegate,UIAlertViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource>
{
    UIPickerView *State_List;
     NSMutableArray *State_List_Array;
     NSMutableArray *State_List_iso;
    
    NSString *string_value;
    NSString *string_IOS;
    
    
    
    NSInteger string_value_int;
   
    
}
@property (strong, nonatomic) IBOutlet UIView *headerview;
@property (strong, nonatomic) IBOutlet UIView *footerview;
@property (strong, nonatomic) IBOutlet UIButton *signupbtn;
@property (strong, nonatomic) IBOutlet UIButton *contrator;
@property (strong, nonatomic) IBOutlet UIButton *cusotmer;
@property (strong, nonatomic) IBOutlet UITableView *tableview;
@property (strong, nonatomic) IBOutlet UIImageView *profileimage;

@end
